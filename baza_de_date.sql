-- phpMyAdmin SQL Dump
-- version 4.0.5
-- http://www.phpmyadmin.net
--
-- Gazda: localhost
-- Timp de generare: 11 Sep 2014 la 11:49
-- Versiune server: 5.0.92-community-log
-- Versiune PHP: 5.3.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Bază de date: `colegi33_ngsn`
--

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `anunturi`
--

CREATE TABLE IF NOT EXISTS `anunturi` (
  `id` int(11) NOT NULL auto_increment,
  `anunt` text NOT NULL,
  `id_clasa` int(3) NOT NULL,
  `id_user` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=53 ;

--
-- Salvarea datelor din tabel `anunturi`
--

INSERT INTO `anunturi` (`id`, `anunt`, `id_clasa`, `id_user`) VALUES
(1, '', 12, 0),
(2, '', 13, 0),
(3, '', 14, 0),
(4, '', 15, 0),
(5, '', 16, 0),
(6, '', 17, 0),
(7, '', 18, 0),
(8, '', 19, 0),
(9, '', 20, 0),
(10, '', 21, 0),
(11, '', 22, 0),
(12, '', 23, 0),
(13, '', 24, 0),
(14, '', 25, 0),
(15, '', 26, 0),
(16, '', 27, 0),
(17, '', 28, 0),
(18, '', 29, 0),
(19, '', 30, 0),
(20, '', 31, 0),
(21, 'maine incepem la 9!', 32, 11),
(22, '', 33, 0),
(23, '', 34, 0),
(24, '', 35, 0),
(25, '', 36, 0),
(26, '', 37, 0),
(27, '', 38, 0),
(28, '', 39, 0),
(29, '', 40, 0),
(30, '', 41, 0),
(41, '', 2, 0),
(42, '', 3, 0),
(46, '', 5, 0),
(45, '', 4, 0),
(47, '', 6, 0),
(48, '', 7, 0),
(49, '', 8, 0),
(50, '', 9, 0),
(51, '', 10, 0),
(52, '', 11, 0);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `clase`
--

CREATE TABLE IF NOT EXISTS `clase` (
  `id` int(11) NOT NULL auto_increment,
  `clasa` varchar(3) NOT NULL,
  `nr_elevi` smallint(2) NOT NULL,
  `profil` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=43 ;

--
-- Salvarea datelor din tabel `clase`
--

INSERT INTO `clase` (`id`, `clasa`, `nr_elevi`, `profil`) VALUES
(1, 'ALL', 0, ''),
(2, '9A', 28, ''),
(3, '9B', 31, ''),
(4, '9C', 27, ''),
(5, '9D', 29, ''),
(6, '9E', 30, ''),
(7, '9F', 29, ''),
(8, '9G', 29, ''),
(9, '9H', 30, ''),
(10, '9I', 30, ''),
(11, '9J', 30, ''),
(12, '10A', 23, ''),
(13, '10B', 26, ''),
(14, '10C', 24, ''),
(15, '10D', 29, ''),
(16, '10E', 20, ''),
(17, '10F', 31, ''),
(18, '10G', 28, ''),
(19, '10H', 30, ''),
(20, '10I', 30, ''),
(21, '10J', 30, ''),
(22, '11A', 26, ''),
(23, '11B', 15, ''),
(24, '11C', 32, ''),
(25, '11D', 16, ''),
(26, '11E', 25, ''),
(27, '11F', 30, ''),
(28, '11G', 30, ''),
(29, '11H', 33, ''),
(30, '11I', 30, ''),
(31, '11J', 25, ''),
(32, '12A', 28, ''),
(33, '12B', 18, ''),
(34, '12C', 21, ''),
(35, '12D', 17, ''),
(36, '12E', 31, ''),
(37, '12F', 29, ''),
(38, '12G', 31, ''),
(39, '12H', 30, ''),
(40, '12I', 30, ''),
(41, '12J', 30, ''),
(42, 'ABS', 0, '');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `cnp`
--

CREATE TABLE IF NOT EXISTS `cnp` (
  `id` int(11) NOT NULL auto_increment,
  `cnp` varchar(23) NOT NULL,
  `utilizat` tinyint(1) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `comentarii`
--

CREATE TABLE IF NOT EXISTS `comentarii` (
  `id` int(11) NOT NULL auto_increment,
  `text` text NOT NULL,
  `data` datetime NOT NULL,
  `id_postare` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=61 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `fisiere`
--

CREATE TABLE IF NOT EXISTS `fisiere` (
  `id` int(11) NOT NULL auto_increment,
  `nume` varchar(200) NOT NULL,
  `cale` varchar(400) NOT NULL,
  `marime` float NOT NULL,
  `data` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `cod` varchar(30) NOT NULL,
  `downloaded` text NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_clasa` int(3) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=480 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `fisiere_categorii`
--

CREATE TABLE IF NOT EXISTS `fisiere_categorii` (
  `id` int(11) NOT NULL auto_increment,
  `nume` varchar(200) NOT NULL,
  `cale` varchar(400) NOT NULL,
  `marime` float NOT NULL,
  `data` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `downloaded` text NOT NULL,
  `id_materie` int(3) NOT NULL,
  `id_user` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=49 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `materii`
--

CREATE TABLE IF NOT EXISTS `materii` (
  `id` int(11) NOT NULL auto_increment,
  `nume` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Salvarea datelor din tabel `materii`
--

INSERT INTO `materii` (`id`, `nume`) VALUES
(1, 'Limba si literatura romana'),
(2, 'Limba engleza'),
(3, 'Limba franceza'),
(4, 'Limba latina'),
(5, 'Matematica'),
(6, 'Istorie'),
(7, 'Geografie'),
(8, 'Fizica'),
(9, 'Chimie'),
(10, 'Biologie'),
(11, 'Informatica'),
(12, 'Logica'),
(13, 'Psihologie'),
(14, 'Economie'),
(15, 'Sociologie'),
(16, 'Filosofie');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `mesaje`
--

CREATE TABLE IF NOT EXISTS `mesaje` (
  `id` int(11) NOT NULL auto_increment,
  `mesaj` text NOT NULL,
  `subiect` text NOT NULL,
  `data` datetime NOT NULL,
  `atasament` text NOT NULL,
  `nume_att` varchar(30) NOT NULL,
  `citit` tinyint(4) NOT NULL,
  `de_la` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=660 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `postari`
--

CREATE TABLE IF NOT EXISTS `postari` (
  `id` int(11) NOT NULL auto_increment,
  `titlu` text NOT NULL,
  `text` text NOT NULL,
  `imagine` text NOT NULL,
  `apreciat` text NOT NULL,
  `neapreciat` text NOT NULL,
  `data` datetime NOT NULL,
  `id_clasa` int(3) NOT NULL,
  `id_user` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `setari`
--

CREATE TABLE IF NOT EXISTS `setari` (
  `id` int(11) NOT NULL auto_increment,
  `dim_max` bigint(20) NOT NULL,
  `max_thumb` bigint(20) NOT NULL,
  `extensii` text NOT NULL,
  `blocked_emails` tinyint(1) NOT NULL,
  `blocked_uploads` tinyint(1) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Salvarea datelor din tabel `setari`
--

INSERT INTO `setari` (`id`, `dim_max`, `max_thumb`, `extensii`, `blocked_emails`, `blocked_uploads`) VALUES
(1, 6, 3, '/php/xml/js/jar/vb/vbs/vbe/inc/', 0, 1);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `trimise`
--

CREATE TABLE IF NOT EXISTS `trimise` (
  `id` int(11) NOT NULL auto_increment,
  `subiect` varchar(40) NOT NULL,
  `mesaj` text NOT NULL,
  `atasament` text NOT NULL,
  `nume_att` varchar(30) NOT NULL,
  `data` datetime NOT NULL,
  `destinatar` varchar(40) NOT NULL,
  `id_user` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=322 ;

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `utilizatori`
--

CREATE TABLE IF NOT EXISTS `utilizatori` (
  `id` int(11) NOT NULL auto_increment,
  `nume` text NOT NULL,
  `prenume` text NOT NULL,
  `user` varchar(30) NOT NULL,
  `parola` varchar(100) NOT NULL,
  `id_clasa` int(3) NOT NULL,
  `email` text NOT NULL,
  `rank` varchar(2) NOT NULL,
  `prieteni` text NOT NULL,
  `m_trimise` int(11) NOT NULL,
  `f_incarcate` int(11) NOT NULL,
  `last_login` datetime NOT NULL,
  `thumb` text NOT NULL,
  `spatiu_ocupat` float NOT NULL,
  `activat` bigint(20) NOT NULL,
  `resetat` bigint(20) NOT NULL,
  `blocat` tinyint(1) NOT NULL,
  `id_materie` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=209 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
         