<?
include "config";
//fisierul de configurare care include si functiile folosite

$token=mres($_GET['token']); //token-ul care da acces la pagina dorita

if ($token=="captcha") //PAGINA CARE RETURNEAZA IMAGINEA CU CODUL DE SECURITATE CAPTCHA
{
$cod=md5(mt_rand(10, 99999)); //generam un cod aleator
$cod=substr($cod, 0, 7); //folosim doar primele 7 caractere generate
$imagine=imagecreatefrompng("imagini/cod.png"); //setam imaginea pe care o modificam
$colorit1=imagecolorallocate($imagine, 10, 0, 110); //definim prima culoare
$colorit2=imagecolorallocate($imagine, 200, 200, 210); //definim a doua culoare
imagettftext($imagine, 29, mt_rand(0,4), 15, 35, $colorit1, "fonts/Press Style Serif.ttf", $cod); //scriem textul cu codul generat deasupra imaginii, la un unghi de 35 de grade
for($i=0; $i<12; $i++)
    imagefilledellipse($imagine, mt_rand(0,119), mt_rand(0,37), 2, 2, $colorit2); //generam 12 elipse aleatoare pe suprafata imaginii
header("Content-type: image/jpeg"); //trimitem imaginea ca header
imagejpeg($imagine); //citim imaginea obtinuta
$_SESSION['cod_captcha']=$cod; //deschidem sesiunea cu codul generat mai sus pentru a-l putea verifica la procesarea inregistrarii
exit(); //pentru a nu se incarca restul paginii
}

?>

<!-- ANTETUL -->

<!DOCTYPE html>
<head>

<!-- META-URI SI SCRIPT-URI-->
<meta itemprop="name" content="<?=$site_title_short;?>" />
<meta itemprop="author" content="Crihan Cosmin" />
<meta itemprop="creator" content="Crihan Cosmin" />
<meta itemprop="provider" content="Crihan Cosmin" />
<meta itemprop="copyright" content="Copyright © 2014 Cosmin Crihan. Toate drepturile rezervate." />
<meta name="distributor" content="Local" />
<meta itemprop="contentRating" content="General" />
<meta name="robots" content="All" />
<meta name="revisit-after" content="7 days" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<script type="text/javascript" src="js/jquery-2.0.0.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/functii.js"></script>
<script type="text/javascript" src="lightbox/js/lightbox.min.js"></script>

<!--IN CAZ DE CRACIUN :)) -->
<?if (!$detectare->isMobile() && 1==0){?>
<script type="text/javascript" src="js /snowstorm.js"></script><?}?>

<!--FAVICON SI FISIERE CSS -->
<link rel="shortcut icon" href="favicon.ico" />
<link rel="icon" href="favicon.ico" />
<link rel="icon" type="image/png" href="favicon.ico" />
<link rel="icon" type="image/x-icon" href="favicon.ico" />
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.css" />
<link rel="stylesheet" type="text/css" href="css/forms.css" />
<link rel="stylesheet" type="text/css" href="css/footer.css" />
<link rel="stylesheet" type="text/css" href="lightbox/css/lightbox.css" />
<title><?=$site_title;?></title>

</head>

<!--SFARSIT ANTET -->

<body style="background-color: #eee;">

<header>

<!--LOGO-->

<?if (!logat()) {?>
<center>
<h1><a href="index.php"><img src="imagini/logo.png" width="90" height="65"/></a></h1>
</center>
<?}?>

<!--BARA DE NAVIGARE -->

<nav <?if (!logat()) echo 'style="display: none;"';?>>
<div class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php"><img src="imagini/logo.png" align="top" width="45" height="30"/></a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <?if (rank($_SESSION['user'])=='P') //OPTIUNILE DIN PANOUL PROFESORULUI
            {?>
            <li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown">Panoul profesorului<span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="index.php?token=postare">Postare noua</a></li>
                <li><a href="index.php?token=email">Mesaj nou</a></li>
                <li><a href="index.php?token=clase">Gestiune clase</a></li>
                <li><a href="index.php?token=anunt">Anunt</a></li>
                <li><a href="index.php?token=difuzare">Difuzare mesaj</a></li>
                <li><a href="index.php?token=promovare">Promovare</a></li>
                <li><a href="index.php?token=transfer">Transfer elev</a></li>
                <li><a href="index.php?token=blocare">Blocare/deblocare cont</a></li>
                <?if (!($detectare->isMobile() && !$detectare->isTablet())){?><li><a href="index.php?token=utilizatori">Tabel cu utilizatori</a></li>
                <li><a href="index.php?token=absolventi">Tabel cu absolventi</a></li><?}?>
                <li><a href="index.php?token=calculator">Calculator medii</a></li>
              </ul>
            </li>
            <?}
            if (rank($_SESSION['user'])=='A') //OPTIUNILE DIN PANOUL ADMINISTRATORULUI
            {?>
            <li class="dropdown">
              <a class="dropdown-toggle" data-toggle="dropdown">Panoul administratorului<span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="index.php?token=clase">Gestiune clase</a></li>
                <li><a href="index.php?token=anunt">Anunt</a></li>
                <li><a href="index.php?token=difuzare">Difuzare mesaj</a></li>
                <li><a href="index.php?token=promovare">Promovare</a></li>
                <?if (!($detectare->isMobile() && !$detectare->isTablet())) {?><li><a href="index.php?token=utilizatori">Tabel cu utilizatori</a></li>
                <li><a href="index.php?token=absolventi">Tabel cu absolventi</a></li>
                <?}?>
                <li><a href="index.php?token=calculator">Calculator medii</a></li>
                <li><a href="index.php?token=transfer">Transfer elev</a></li>
                <li><a href="index.php?token=avansare">Avansare clase</a></li>
                <li><a href="index.php?token=stergere">Stergere cont</a></li>
                <li><a href="index.php?token=blocare">Blocare/deblocare cont</a></li>
                <li><a href="index.php?token=ultimii">Ultimii utilizatori inregistrati</a></li>
                <li><a href="index.php?token=setari">Setari</a></li>
              </ul>
            </li>
            <?
            }
            ?>
            <li class="dropdown"> <!-- MANAGERUL DE FISIERE -->
              <a class="dropdown-toggle" data-toggle="dropdown">Manager de fisiere<span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="index.php?token=incarca">Incarcare fisier</a></li>
                <li><a href="index.php?token=fisiere">Fisiere incarcate</a></li>
                <li><a href="index.php?token=incarca_materii">Incarcare fisier pe materii</a></li>
                <li><a href="index.php?token=materii">Fisiere pe materii</a></li>
                <?if (rank($_SESSION['user'])!='E') {?>
                <li><a href="index.php?token=toate">Fisiere fara categorie</a></li>
                <?}?>
              </ul>
            </li>
            <li class="dropdown"> <!--MESAGERIA -->
              <a class="dropdown-toggle" data-toggle="dropdown">Mesagerie
            <?$m=query("SELECT COUNT(*) FROM mesaje WHERE id_user=".get_id_by_user($_SESSION['user'])." AND citit=0");
            $c=$m[0];
            if ($c!=0) echo '<span class="badge">'.$c.'</span>';
            ?>
              <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="index.php?token=mesaje_primite">Mesaje primite
            <?$m=query("SELECT COUNT(*) FROM mesaje WHERE id_user=".get_id_by_user($_SESSION['user'])." AND citit=0");
            if ($m[0]!=0) echo '<span class="badge">'.$m[0].'</span>';?>
                </a></li>
                <li><a href="index.php?token=mesaje_trimise">Mesaje trimise
            <?$m=query("SELECT COUNT(*) FROM trimise WHERE id_user=".get_id_by_user($_SESSION['user']));
            $c=$m[0];
            if ($c!=0) echo '<span class="badge">'.$c.'</span>';?>
                </a></li>
              </ul>
            </li>
          </ul>
          <ul class="nav navbar-nav navbar-right"> <!-- MENIUL UTILIZATORULUI -->
            <li class="active">
                <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown"><?=$_SESSION['user'];?>
                <img style="vertical-align:middle; border-radius:3px;" src="<?=get_thumb($_SESSION['user']);?>" width="25" height="25"/>
                <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="index.php?token=deconectare">Deconectare</a></li>
                        <li><a href="index.php?token=admin">Profilul meu</a></li>
                        <li><a href="index.php?token=postare">Postare noua</a></li>
                        <li><a href="index.php?token=email">Mesaj nou</a></li>
                        <li><a href="index.php?token=profil">Vizualizare profil</a></li>
                        <li><a href="index.php?token=favoriti">Lista mea de favoriti</a></li>
                        <?if (rank($_SESSION['user'])=='E') {?>
                        <li><a href="index.php?token=calculator">Calculator medii</a></li>
                        <?if (!($detectare->isMobile() && !$detectare->isTablet())) {?>
                        <li><a href="index.php?token=utilizatori">Tabel cu utilizatori</a></li>
                        <li><a href="index.php?token=absolventi">Tabel cu absolventi</a></li>
                        <?}}
                        if (!($detectare->isMobile() && !$detectare->isTablet())) {?>
                        <li><a href="index.php?token=radio">Asculta radio</a></li>
                        <?}?>
                    </ul>
                </li>
            </li>
          </ul>
        </div>
      </div>
    </div>
</nav>

<!-- SFARSIT BARA DE NAVIGARE -->

</header>

<!-- CONTINUTUL CURENT: AICI ESTE INCLUS CONTINUTUL FIECAREI PAGINI, FIE EA VIZUALA SAU DE PROCESARE -->
<div class="container" style="margin-top: 20px; <? if (logat()) echo 'margin-bottom: 50px;';
if (!logat()) echo 'margin-bottom: 60px;';
echo '"';
?>>
<?
if ($token=="") //PAGINA PRINCIPALA (LOGARE SAU CRONOLOGIE)
{
if (!logat()){?>
<center>
<form class="form-set" role="form" action="index.php?token=logare" method="POST">
<input class="form-control" type="text" name="user" size="30" placeholder="Nume de utilizator" required autofocus/>
<input class="form-control" type="password" name="pass" size="25" placeholder="Parola" required/>
<?read_error();?>
<div class="checkbox">
          <label>
            <input type="checkbox" name="logat" value="logat"/> Pastreaza-ma logat
          </label>
</div>
<button class="btn btn-lg btn-primary btn-block" style="width: auto;" type="submit">Logare</button>
</form>
<a href="index.php?token=uitatparola">Ati uitat parola?</a>
<br />
<a href="index.php?token=uitatuser">Ati uitat numele de utilizator?</a>
<br />
Nu ati primit link-ul de activare al contului? Click <a href="index.php?token=retrimite">aici</a> pentru a-l retrimite.<br />
Nu aveti cont? <a href="index.php?token=inregistrare">Inregistrati-va!</a></center>
<?}
else if (logat())
{
/*?>
<center>
<?
if (rank($_SESSION['user'])=='E') echo "<h1 class='page-header'>Cronologia clasei ".get_clasa_by_user($_SESSION['user'])."</h1>";
else echo "<h1 class='page-header'>Cronologie</h1>";
?>
</center>
<?*/
if (rank($_SESSION['user'])=='E') //in cazul elevilor preluam anuntul pentru clasa (daca exista) si il afisam intr-un panou rosu, deasupra postarilor, pentru vizibilitate
    {
    $q=query("SELECT * FROM anunturi WHERE id_clasa=".get_id_by_class(get_clasa_by_user($_SESSION['user'])));
    if ($q['anunt']) echo '<center><div class="alert alert-danger">
    <h4>ANUNT - difuzat de <b><i>'.get_user_by_id($q['id_user']).'</i></b></h4>'.$q['anunt'].'</div></center>';
    }
if (exist_posts()) //daca exista, preluam postarile din baza de date si le afisam
{
read_error();
read_succes();
$clasa_c=get_clasa_by_user($_SESSION['user']);
$id_clasa_c=get_id_by_class($clasa_c);
if (rank($_SESSION['user'])=='E') //pentru un elev, selectam doar postarile pentru clasa lui
    $q=@mysql_query("SELECT * FROM postari WHERE id_clasa=$id_clasa_c OR id_clasa=1 ORDER BY data DESC");
else $q=@mysql_query("SELECT * FROM postari ORDER BY data DESC");
$id_p=1;
echo "<div class='postari'>";
while ($r=@mysql_fetch_array($q))
    {echo '<div class="panel panel-primary">
    <div id="tp'.$r['id'].'" class="panel panel-heading clearfix" style="margin-bottom: 0px;">
    <h3 class="panel-title"><div class="pull-left" style="width: 70%;">'.$r["titlu"].'</div>
    <div class="pull-right">';
    if (rank($_SESSION['user'])=='E' && get_user_by_id($r['id_user'])==$_SESSION['user'] || rank($_SESSION['user'])!='E') //postarile nu pot fi sterse sau distribuite de un elev, decat in cazul in care acesta a creat postarea
        echo '<a href="index.php?token=sterge_postare&id='.$r["id"].'"><span style="float: right; margin-left: 7px; color: white;" class="glyphicon glyphicon-trash" onclick="return confirm(\'Sigur stergeti aceasta postare?\');"></span></a>';
    if (rank($_SESSION['user'])!='E') echo '<a href="#dp'.$r['id'].'"><span style="float: right; margin-left: 7px; color: white;" class="glyphicon glyphicon-share-alt" onclick="distribuie(\'d'.$r['id'].'\');"></span></a>';
    if ($r['id_user']==get_id_by_user($_SESSION['user']) || rank($_SESSION['user'])!='E')
        echo '<a href="javascript:void(0);"><span style="float: right; margin-left: 7px; color: white;" class="glyphicon glyphicon-edit" onclick="editeaza(\'edit'.$r['id'].'\', \'tp'.$r['id'].'\', \'cp'.$r['id'].'\');"></span></a>';
    echo '</div>
    </h3>
    </div>
    <div class="panel panel-body">';
    if (exist_image($r['id']))
    {
    if (!($detectare->isMobile() && !$detectare->isTablet()))
        $marime='width: 128px; height: 128px;';
    else $marime='width: 80px; height: 80px;';
    if (!($detectare->isMobile() && !$detectare->isTablet()))
    echo '<div class="media" id="cp'.$r['id'].'">
    <a data-lightbox="imagine'.$r['id'].'" class="pull-left" href="'.$r['imagine'].'">
    <img class="media-object" src="'.$r['imagine'].'" style="'.$marime.'" />
    </a>
    <div class="media-body" style="white-space: pre-wrap;">'.$r['text'].'</div>
    </div>';
    else echo '<div id="cp'.$r['id'].'">
    <a data-lightbox="imagine'.$r['id'].'" href="'.$r['imagine'].'">
    <img src="'.$r['imagine'].'" style="'.$marime.' float: left; padding: 5px;" />
    </a>
    <div style="white-space: pre-wrap;">'.$r['text'].'</div>
    </div>';
    }
    else echo '<div id="cp'.$r['id'].'" style="white-space: pre-wrap;">'.$r["text"].'</div>';
    echo '
    <form id="edit'.$r['id'].'" style="display: none;" action="index.php?token=editare_postare&id='.$r['id'].'&p='.$id_p.'" method="POST">
    <input class="form-control" type="text" name="titlu" size="50" placeholder="Titlu" value="'.$r['titlu'].'" required/>
    <br />
    <textarea class="form-control" style="resize: none;" name="text" rows="10" maxlength="100000" placeholder="Continutul postarii" required>'.$r['text'].'</textarea>
    <button class="btn btn-primary" type="submit" style="width: auto;">Salvare</button>
    <button onclick="anuleaza_editare(\'edit'.$r['id'].'\', \'tp'.$r['id'].'\', \'cp'.$r['id'].'\'); return false;" class="btn btn-primary" style="width: auto;">Anulare</button>
    </form>';
    echo '<span style="float: left;">
    <i>postat de <b><a style="text-decoration: none;" href="index.php?token=profil&id='.$r["id_user"].'">'.get_user_by_id($r["id_user"]).'</a></b> la <strong>'.$r["data"].'</strong>';
    if (rank($_SESSION['user'])!='E'){ echo ' pentru ';
    if ($r['id_clasa']!=1) echo 'clasa <b>'.get_class_by_id($r["id_clasa"]).'</b>';
    else echo '<b>toate clasele</b>';}
    echo '</i></span><br style="clear: both;" />
    <a name="dp'.$r['id'].'"><p style="display: none;">ancora distribuire</p></a>
    <table style="float: left;">
    <tr><td style="padding-right: 10px;';
    //preluam si afisam numarul de like-uri si dislike-uri
    $q1=query("SELECT apreciat FROM postari WHERE id=".$r["id"]);
    $aprecieri=explode("/", $q1[0]);
    $p=-1;
    $a='';
    foreach ($aprecieri as $apreciere)    
        {$p++;
        $a=$a."<a style='text-decoration: none;' href='index.php?token=profil&id=$apreciere'>".get_user_by_id($apreciere)."</a>&nbsp;&nbsp;&nbsp;&nbsp;";
        }
    if ($p==0) echo 'display: none;';
    echo '">
    <a style="text-decoration: none;" href="javascript:void(0);" id="popoverl'.$r['id'].'" data-toggle="popover" title="Urmatorii utilizatori au apreciat postarea:" data-content="'.$a.'">';
    if ($p==1) echo '1 like';
    else if ($p) echo $p.' like-uri';
    echo '</a>
    <script type="text/javascript">
    $(document).ready(function(){
    $("#popoverl'.$r['id'].'").popover({html: true});   
    });
    </script>
    </td><td>';
    $q1=query("SELECT neapreciat FROM postari WHERE id=".$r["id"]);
    $aprecieri=explode("/", $q1[0]);
    $p=-1;
    $a='';
    foreach ($aprecieri as $apreciere)    
        {$p++;
        $a=$a."<a style='text-decoration: none;' href='index.php?token=profil&id=$apreciere'>".get_user_by_id($apreciere)."</a>&nbsp;&nbsp;&nbsp;&nbsp;";
        }
    echo '<a style="text-decoration: none;" href="javascript:void(0);" id="popoverd'.$r['id'].'" data-toggle="popover" title="Urmatorii utilizatori nu au apreciat postarea:" data-content="'.$a.'">';
    if ($p==1) echo '1 dislike';
    else if ($p) echo $p.' dislike-uri';
    echo '</a>
    <script type="text/javascript">
    $(document).ready(function(){
    $("#popoverd'.$r['id'].'").popover({html:true});   
    });
    </script>
    </td></tr></table>
    <br style="clear: both;"/>';
    //afisam butoanele pentru like si dislike
    if (liked($r["id"]))
        echo '<a href="index.php?token=unlike&id='.$r["id"].'&p='.$id_p.'"><img style="float: left;" src="imagini/unlike.png" width="30" height="30" /></a>';
        else echo '<a href="index.php?token=like&id='.$r["id"].'&p='.$id_p.'"><img style="float: left;" src="imagini/like.png" width="30" height="30" /></a>';
        if (disliked($r["id"]))echo '<a href="index.php?token=undislike&id='.$r["id"].'&p='.$id_p.'"><img style="float: left;" src="imagini/undislike.png" width="30" height="30" /></a>';
        else echo '<a href="index.php?token=dislike&id='.$r["id"].'&p='.$id_p.'"><img style="float: left;" src="imagini/dislike.png" width="30" height="30" /></a>';
    ?>
    <div class="alert" id="d<?=$r['id'];?>" style="float: left; display: none; width: auto;">
    <?if (!($detectare->isMobile() && !$detectare->isTablet())) {?>
    <div style="margin-left: 20px; float: right;">
    <button id="buton" type="button" class="close" onclick="close_dist('d<?=$r['id'];?>');">&times;</button>
    </div>
    <?}?>
    <form class="form-set" style="float: left;" role="form" action="index.php?token=distribuie&id=<?=$r['id'];?>" method="POST">
    <label>Alegeti clasa pentru care distribuiti postarea:
    <?if ($detectare->isMobile() && !$detectare->isTablet()) {?>
    <button id="buton" style="float: right;" type="button" class="close" onclick="close_dist('d<?=$r['id'];?>');">&times;</button>
    <?}?>
    </label>
    <select class="form-control" style="width:auto;" name="clasa">
    <?
    $rez=@mysql_query("SELECT * FROM clase ORDER BY clasa ASC");
    while($row=@mysql_fetch_array($rez))
        if ($row['clasa']!='ALL')
            echo "<option>".$row['clasa']."</option>";
    ?>
    </select>
    <button class="btn btn-primary btn-sm btn-block" style="width: auto;" type="submit">Distribuire</button>
    </form>
    </div>
    <br style="clear: both;" />
    <?
    $p_id=$id_p+1;
    if (exist_comm($r["id"])) //daca exista comentarii, le preluam si le afisam
        {   
        echo '
        <div class="panel panel-success">
        <div class="panel panel-heading" style="margin-bottom: 0px;">
        <h3 class="panel-title">COMENTARII</h3>
        </div>
        <div class="panel panel-body">';
        $n=@query("SELECT COUNT(*) FROM comentarii WHERE id_postare=".$r['id']);
        $nr_comm=$n[0];
        if ($nr_comm>5) {
            echo "<a id='nr_comm".$r['id']."' href='javascript:void(0);' onclick=\"afiseaza_comm('c".$r['id']."', 'nr_comm".$r['id']."', 'ascunde_nr_comm".$r['id']."')\">Afiseaza toate comentariile</a>
            <a style='display: none;' id='ascunde_nr_comm".$r['id']."' href='javascript:void(0);' onclick=\"ascunde_comm('c".$r['id']."', 'nr_comm".$r['id']."', 'ascunde_nr_comm".$r['id']."')\">Ascunde comentariile anterioare</a>";
        }
        $c=@mysql_query("SELECT * FROM comentarii WHERE id_postare=".$r['id']." ORDER BY data ASC");
        if ($nr_comm<=5)
            while ($c_r=@mysql_fetch_array($c))   
            {   
            echo "<div style='width: 100%; float: left;'><img src='".get_thumb(get_user_by_id($c_r['id_user']))."' style='border-radius:5px; vertical-align:middle; float: left;' width='40' height='40'/>
            <b><i><a style='text-decoration: none;' href='index.php?token=profil&id=".$c_r['id_user']."'>".get_user_by_id($c_r['id_user'])."</a></b></i> ";
            echo $c_r['text']."</b><br/><strong><i>".$c_r['data']."</i></strong>";   
            if (owner_comm($c_r['id'])) //comentariul poate fi sters doar de proprietarul acestuia sau de un cont care nu este de tip elev
                echo "&nbsp;&nbsp;<a href='index.php?token=sterge_comentariu&id=".$c_r['id']."&p=".$id_p."' onclick='return confirm(\"Stergeti comentariul?\");'><span class='glyphicon glyphicon-remove'></span></a>";
            echo "</div><br />";
            }
        else {
            $k=1;
            echo "<div id='c".$r['id']."' style='display: none;'>";
            while ($c_r=@mysql_fetch_array($c))   
            { 
            echo "<div style='width: 100%; float: left;'><img src='".get_thumb(get_user_by_id($c_r['id_user']))."' style='border-radius:5px; vertical-align:middle; float: left;' width='40' height='40'/>
            <b><i><a style='text-decoration: none;' href='index.php?token=profil&id=".$c_r['id_user']."'>".get_user_by_id($c_r['id_user'])."</a></b></i> ";
            echo $c_r['text']."</b><br/><strong><i>".$c_r['data']."</i></strong>";   
            if (owner_comm($c_r['id'])) //comentariul poate fi sters doar de proprietarul acestuia sau de un cont care nu este de tip elev
                echo "&nbsp;&nbsp;<a href='index.php?token=sterge_comentariu&id=".$c_r['id']."&p=".$id_p."' onclick='return confirm(\"Stergeti comentariul?\");'><span class='glyphicon glyphicon-remove'></span></a>";
            echo "</div><br />";
            $k++;
            if ($k==($nr_comm-5+1)) echo "</div>";
            }
        }
        echo '
        </div>
        </div>'; 
        }
    //afisam forma de trimitere a unui comentariu
    echo '
    <div class="media">
    <form role="form" action="index.php?token=comenteaza&p='.$id_p.'" method="POST" style="float: left;">
    <a class="pull-left">
    <img class="media-object" src="'.get_thumb($_SESSION['user']).'" style="border-radius:5px; vertical-align:middle; float: left;" width="40" height="40" />
    </a>
    <div class="media-body">
    <input class="form-control" style="float: left; margin-top: 3px;" type="text" name="comentariu" size="70" max="400" maxlength="400" placeholder="Adauga un comentariu"/>
    </div>
    <input type="hidden" name="id" value="'.$r["id"].'" />
    </form>
    </div>';
    echo '<a name="p'.$p_id.'" style="color: white; text-decoration: none;"><p style="display: none;">ancora postare</p></a>';
    echo '</div>
    </div>';
    $id_p++;
    }
    echo "</div>";
}
else {//in cazul in care nu exista postari, afisam un mesaj in acest sens, precum si o lista cu facilitatile oferite de platforma
        read_error();
        read_succes();
        echo '<div class="alert alert-danger" role="alert">Nu exista nicio postare adaugata in cronologie! Creati una accesand sectiunea "Postare noua" din meniul utilizatorului.</div>
        <div class="alert alert-info" role="alert">Explorati alte sectiuni ale platformei, precum formele de trimitere de mesaje, listele cu fisiere incarcate de utilizatori, profilurile altor utilizatori, etc.
        <br/><br />
        '.$site_title_short.' va ofera urmatoarele facilitati: <br />
        <ul type="bullet">
        <li>Socializare intre elevii claselor dintr-o institutie de invatamant, fiecare clasa avand o cronologie proprie, ce contine postari create de elevi, profesori sau administratori.</li>
        <li>Socializare intre clase sau/si intre persoane terte prin folosirea sectiunii de trimitere mesaje PM sau e-mail-uri, precum si o "casuta postala proprie", unde fiecare utilizator isi gestioneaza mesajele primite si trimise.</li>
        <li>Gazduire de fisiere pentru fiecare clasa in parte, existand si o categorie de fisiere care au fost incarcate pentru anumite materii de studiu. Spatiul ocupat de fiecare elev este limitat de administrator.</li>
        <li>Profil personal cu cateva date statistice si posibilitatea de a adauga o fotografie de profil. Fiecare utilizator poate vedea profilurile celorlalti si adauga pe oricine in lista de favoriti, pentru a facilita mesageria rapida cu acestia.</li>
        <li>Gestiune completa a platformei folosind panourile specifice pentru profesori si administratori, fiecare cu drepturi corespunzatoare.</li>
        <li>Conturi diferentiate pe ranguri de: elev, profesor, administrator, fiecare avand permisiuni specifice pe site.</li>
        <li>Securi-+tate sporita in ceea ce priveste permisiunile, accesul la fiecare sectiune a site-ului, paginile de procesare si introducere de date in baza de date.</li>
        </ul>
        </div>';
    }
}
}

else if ($token=="logare") //PAGINA DE PROCESARE A LOGARII
{
if ($_SERVER['REQUEST_METHOD']!='POST')
    redirect("index.php?token=eroare&id=nepermis");
$user=mres($_POST['user']);
$pass=mres($_POST['pass']);
$check=mres($_POST['logat']);
if ($user=='' || $pass=='')
    {
    opensession('error', 'Completati toate campurile!');
    redirect("index.php");
    exit();
    }
if (!exist_user($user))
    {
    opensession('error', 'Numele de utilizator nu exista in baza de date!');
    redirect("index.php");
    exit();
    }
if (!user_activ($user))
    {
    opensession('error', 'Acest cont nu este inca activ!');
    go_back();
    exit();
    }
if (is_blocat($user))
    {
    opensession('error', 'Contul dvs. a fost blocat! Contactati administratorul '.$site_title_short.'.');
    go_back();
    exit();
    }
if (!query("SELECT * FROM utilizatori WHERE parola=SHA1('$pass') AND user='$user'"))
    {
    /*if (!$_COOKIE['coJHoad88CPV3ptWASet'])
        {setcookie("coJHoad88CPV3ptWASet", 1, time()+ 600); opensession("coJHoad88CPV3ptWASet", $user); redirect("index.php?token=avertizare_parola");}
    else {setcookie("coJHoad88CPV3ptWASet", $_COOKIE['coJHoad88CPV3ptWASet']+1); redirect("index.php?token=avertizare_parola");}*/
    opensession('error', 'Parola este gresita!');
    go_back();
    exit();
    }
opensession("user", $user);
setcookie("coJHoad88CPV3ptWASet", "", time()-1);
if ($check) 
    {
    $id=get_id_by_user($user);
    setcookie("66pWKLbxb1lWoWkvkd2T", $id, time()+604800);
    $p=get_parola_by_user($_SESSION['user']);
    setcookie("CqRh6hveZ7B0Din0eJhY", "$p", time()+604800);
    }
update_last_login(date("Y-m-d H:i:s"), $_SESSION['user']);
redirect("");
}

else if ($token=="deconectare") //DECONECTARE
{
if (!logat())
    redirect("index.php?token=eroare&id=nepermis");
$u=$_SESSION['user'];
setcookie('66pWKLbxb1lWoWkvkd2T', "$u", time()-1);
$p=get_parola_by_user($_SESSION['user']);
setcookie('CqRh6hveZ7B0Din0eJhY', "$p", time()-1);
$_SESSION['user']='';
$_SESSION['clasa']='';
redirect("");
}

else if ($token=="avertizare_parola") //PAGINA CARE PROCESEAZA NUMARUL DE INCERCARI RAMASE PANA CE CONTUL UTILIZATORULUI CARE A INTRODUS PAROLA GRESIT VA FI BLOCAT
{
$user=$_SESSION['coJHoad88CPV3ptWASet'];
if ($_COOKIE['coJHoad88CPV3ptWASet']==5)
        {@mysql_query("UPDATE utilizatori SET blocat=1 WHERE user='$user'");
        opensession('error', 'Ati gresit parola de 5 ori. Contul dvs. a fost blocat! Va rog contactati administratorul.');
        setcookie("coJHoad88CPV3ptWASet", "", time()-1);
        $_SESSION['coJHoad88CPV3ptWASet']='';
        }
else opensession('error', 'Parola este gresita! Mai aveti '.(5-$_COOKIE['coJHoad88CPV3ptWASet']).' incercari ramase.');
redirect("index.php");
}

else if ($token=="termeni") //TERMENI DE UTILIZARE
{
?>
<h1 class="page-header" style="text-align: center;">Termeni si conditii</h1>
<br />
<div class="panel panel-default">
<div class="panel-body">
<ol type="1" style="text-align: justify;">
<li>Site-ul nu va fi folosit decat in scopurile explicite pentru care a fost creat, si anume socializarea intre clase, trimiterea de e-mail-uri, mesaje PM, incarcarea de fisiere. Este interzisa folosirea lui in scopuri comerciale sau frauduloase.</li>
<li>Mesajele trimise vor avea un limbaj adecvat, nu vor contine injurii la adresa destinatarilor, amenintari, referinte la site-uri pornografice, droguri, alcool, arme, etc. NU se va folosi functia de trimitere e-mail pentru a promova reclame, a trimite mesaje spam, sau a deranja in vreun fel alte persoane.</li>
<li>Fisierele incarcate sau trimise prin e-mail/PM nu vor avea continut de tip pornografic, sau orice are legatura cu drogurile, alcoolul, tutunul, armele, etc.</li>
<li>NU sunt responsabil de nicio postare creata, de niciun e-mail trimis / fisier incarcat de pe acest site. Orice utilizator ce foloseste site-ul este direct responsabil pentru ceea ce face, fapt pentru care a citit si acceptat acesti termeni de utilizare.</li>
</ol>
</div>
</div>
<?
}

else if ($token=="inregistrare") //PAGINA DE INREGISTRARE
{
if (logat())
    redirect("");
?>
<center>
<h1>Inregistrare</h1>
<form class="form-set" role="form" action="index.php?token=procesare_inregistrare" method="POST">
<?
read_succes();
?>
<input class="form-control" type="text" name="nume" size="40" placeholder="Nume" required/>
<input class="form-control" type="text" name="prenume" size="40" placeholder="Prenume" required/>
<input class="form-control" type="email" name="email" size="35" placeholder="E-mail" required/>
<input class="form-control" type="text" name="user" size="30" max="30" maxlength="30" placeholder="Nume de utilizator" required/>
<div class="alert alert-info">
Sugestie: Un nume de utilizator mai scurt este mai prezentabil ;)
</div>
<input class="form-control" type="password" name="pass" size="40" max="40" maxlength="40" placeholder="Parola" required/>
<input class="form-control" type="password" name="pass2" size="40" max="40" maxlength="40" placeholder="Confirmare parola" required/>
<div class="checkbox">
          <label>
            <input name="prof" type="checkbox" id="checkbox" name="check" onclick="ascunde();"/> bifati aici daca sunteti profesor
          </label>
</div>
<div id="cnp" style="display: <?if ($_SESSION['prof']) echo "block;"; else echo "none;";?>">
<input id="select_cnp" class="form-control" type="text" name="cnp" size="13" maxlength="13" max="13" placeholder="CNP"/>
</div>
<div id="materie" style="display: <?if ($_SESSION['prof']) echo "block;"; else echo "none;";?>">
<label>Alegeti materia pe care o predati: </label>
<select id="select_materie" class="form-control" style="width: auto;" name="materie">
<?
$q=@mysql_query("SELECT * FROM materii ORDER BY nume ASC");
while ($r=@mysql_fetch_array($q))
    echo "<option>".$r['nume']."</option>";
?>
</select>
</div>
<div id="clasa" style="display: <?if ($_SESSION['prof']) echo "none;"; else echo "block;"; $_SESSION['prof']='';?>">
<select id="select_clasa" class="form-control" name="clasa" style="width: auto;">
<?
$rez=@mysql_query("SELECT * FROM clase ORDER BY clasa ASC");
while($row=@mysql_fetch_array($rez))
    if ($row['clasa']!='ALL' && $row['clasa']!='ABS')
        echo "<option>".$row['clasa']."</option>";
?>
</select>
</div>
<br />
<img src="index.php?token=captcha" style="border-radius: 5px;"/><br />
<input class="form-control" type="text" name="cod" placeholder="Cod de securitate"/>
<div class="checkbox">
          <label>
            <input name="check" type="checkbox" />Sunt de acord cu <a href="index.php?token=termeni" target="_blank">termenii si conditiile</a> de utilizare ai site-ului.
          </label>
</div>
<?
read_error();
?>
<button class="btn btn-lg btn-primary btn-block" style="width: auto;" type="reset">Resetare campuri</button>
<button class="btn btn-lg btn-primary btn-block" style="width: auto;" type="submit">Inregistrare</button>
</form>
</center>
<?
}

else if ($token=="procesare_inregistrare") //PAGINA DE PROCESARE A INREGISTRARII
{
if ($_SERVER['REQUEST_METHOD']!='POST')
    redirect("index.php?token=eroare&id=nepermis");
$user=mres($_POST['user']);
$pass=mres($_POST['pass']);
$pass2=mres($_POST['pass2']);
$nume=mres($_POST['nume']);
$prenume=mres($_POST['prenume']);
$email=mres($_POST['email']);
$clasa=mres($_POST['clasa']);
$cnp=mres($_POST['cnp']);
$materie=mres($_POST['materie']);
$cod=mres($_POST['cod']);
$check=mres($_POST['check']);
$prof=mres($_POST['prof']);
if ($prof)
    opensession("prof", "1");
$flag=1;
if ($user=='')
    $flag=0;
if ($pass=='')
    $flag=0;
if ($pass2=='')
    $flag=0;
if ($nume=='')
    $flag=0;
if ($prenume=='')
    $flag=0;
if ($email=='')
    $flag=0;
if ($cod=='')
    $flag=0;
if ($prof && (!$cnp || !$materie))
    $flag=0;
if (!$prof && !$clasa)
    $flag=0;
if ($flag==0)
    {
    opensession('error', 'Va rugam completati toate campurile!');
    go_back();
    exit();
    }
if (!email_valid($email))
    {
    opensession('error', 'E-mail invalid!');
    go_back();
    exit(); 
    }
if (query("SELECT * FROM utilizatori WHERE email='$email'"))
    {
    opensession('error', 'Exista deja un utilizator inregistrat cu aceasta adresa de e-mail!');
    go_back();
    exit();
    }
if (!valid_username($user))
    {
    opensession('error', 'Nume de utilizator invalid! Acesta trebuie sa inceapa cu cel putin o litera, sa aiba o lungime cuprinsa intre 6 si 30 de caractere, poate contine doar caractere alfanumerice, respectiv caracterul underscore ("_"), punct (".") sau cratima ("-") si nu poate contine spatii goale!');
    go_back();
    exit();  
    }
if (query("SELECT * FROM utilizatori WHERE user='$user'"))
    {
    opensession('error', 'Numele de utilizator exista deja in baza de date!');
    go_back();
    exit();
    }
if (strlen($pass)>40 || strlen($pass)<6)
    {
    opensession('error', 'Parola trebuie sa aiba intre 6 si 40 de caractere!');
    go_back();
    exit();  
    }
if ($pass!=$pass2)
    {
    opensession('error', 'Parolele nu coincid!');
    go_back();
    exit();
    }
if ($prof)
    {if (strlen($cnp)!=13 || !is_numeric($cnp))
        {
        opensession('error', 'CNP-ul trebuie sa fie un numar de 23 de cifre!');
        go_back();
        exit();  
        }
    if (!exist_cnp($cnp))
        {
        opensession('error', 'CNP-ul introdus nu este valid!');
        go_back();
        exit();  
        }
    if (used_cnp($cnp))
        {
        opensession('error', 'CNP-ul introdus este deja utilizat!');
        go_back();
        exit(); 
        }
    $rank="P";
    }
if (!$prof)
    $rank="E";
$id_clasa=get_id_by_class($clasa);
if ($prof) $id_clasa=0;
$res=@query("SELECT COUNT(*) FROM utilizatori WHERE id_clasa=$id_clasa");
$res2=@query("SELECT nr_elevi FROM clase WHERE id=$id_clasa");
if ($res[0]>=$res2[0] && !$prof)
    {
    opensession('error', 'Numarul de elevi/conturi de utilizator pentru aceasta clasa a fost depasit! Contactati administratorul '.$site_title_short.'!');
    go_back();
    exit();
    }
if ($_SESSION['cod_captcha']!=$cod)
    {
    opensession('error', 'Cod de securitate incorect!');
    go_back();
    exit();  
    }
if (!$check)
    {
    opensession('error', 'Va rugam bifati ca sunteti de acord cu termenii si conditiile de utilizare!');
    go_back();
    exit(); 
    }
$id_unic=rand(11111111111, 99999999999);
$id_materie=get_id_by_materie($materie);
if ($prof) @mysql_query("UPDATE cnp SET utilizat=1 WHERE cnp=$cnp");
@mysql_query("INSERT INTO utilizatori (nume, prenume, user, parola, email, rank, activat, id_clasa, id_materie) VALUES ('$nume', '$prenume', '$user', SHA1('$pass'), '$email', '$rank', $id_unic, $id_clasa, $id_materie)");
email($email, "Activare cont $site_title_short", "Ati creat contul cu numele de utilizator ".$user." pe ".$site_title_short.". Pentru a-l activa, accesati urmatorul link: \n ".$site_link."index.php?token=activare&id_unic=".$id_unic."\n\nCu respect,\nCosmin Crihan (admin)", "contact@".$site_title."");
opensession('succes', 'Contul a fost creat cu succes! Pentru a-l activa, accesati linkul trimis prin e-mail!');
redirect("index.php?token=inregistrare");
}

else if ($token=="activare") //PAGINA DE ACTIVARE A CONTULUI
{
$id_unic=mres($_GET['id_unic']);
if (!is_numeric($_GET['id_unic']) || !isset($_GET['id_unic']) || $_SERVER['REQUEST_METHOD']!='GET')
    redirect("index.php?token=eroare&id=nepermis");
if ($id_unic==1) //daca id-ul este 1, atunci contul este activat
{
    opensession('error', 'Acest cont este deja activ!');
    redirect("index.php?token=eroare");
}
if (!query("SELECT * FROM utilizatori WHERE activat=$id_unic")) //daca nu exista codul de activare primit
{
    opensession('error', 'Aceasta cerere de activare a contului nu a fost facuta de dvs.! Va rugam creati-va un cont!');
    redirect("index.php?token=eroare");
}
$u=@query("SELECT * FROM utilizatori WHERE activat=$id_unic");
$id_user=$u['id'];
@mysql_query("INSERT INTO mesaje (id_user, mesaj, de_la, subiect, data, citit) VALUES ($id_user, '".$site_title_short." va ureaza bun venit pe site! <br/><br/>Cu respect, <br/>Cosmin Crihan (admin).', 0, 'Bun venit pe ".$site_title_short."!', NOW(), 0)"); //trimitem un mesaj de bun venit
@mysql_query("UPDATE utilizatori SET activat=1 WHERE activat=$id_unic"); //activam contul
opensession('succes', 'Contul dvs. a fost activat cu succes! Ati fost logat automat.');
opensession('user', $u['user']); //logam user-ul automat
update_last_login(date("Y-m-d H:i:s"), $u['user']);
redirect("index.php?token=succes");
}

else if ($token=="uitatparola") //PAGINA "AM UITAT PAROLA"
{
if (logat())
    redirect("index.php?token=eroare&id=nepermis");
?>
<center>
<h1>Ati uitat parola?</h1><br />
<form class="form-set" role="form" action="index.php?token=procesare_uitatparola" method="POST">
<input class="form-control" type="email" name="email" size="40" placeholder="Introduceti e-mail-ul dvs." required autofocus/>
<?
read_error();
read_succes();
?>
<button class="btn btn-lg btn-primary btn-block" style="width: auto;" type="submit">Resetare parola</button>
</form>
</center>
<?  
}

else if ($token=="procesare_uitatparola") //PAGINA DE PROCESARE PENTRU RESETAREA PAROLEI
{
if (logat())
    redirect("index.php?token=eroare&id=nepermis");
if (isset($_GET['id_unic']))
{
$id=mres($_GET['id_unic']);
if (!query("SELECT * FROM utilizatori WHERE resetat=$id") || !is_numeric($id))
{
opensession('error', 'Cererea de resetare a parolei nu a fost facuta de pe contul dvs.!');
redirect("index.php?token=uitatparola");
}
?>
<center>
<h1>Modificare parola</h1>
<form class="form-set" role="form" action="index.php?token=resetare&id_unic=<?=$id;?>" method="POST">
<input class="form-control" type="password" name="parola1" size="40" max="40" maxlength="40" placeholder="Parola noua" required autofocus/>
<input class="form-control" type="password" name="parola2" size="40" max="40" maxlength="40" placeholder="Confirmare parola noua" required/>
<?
read_error();
read_succes();
?>
<button class="btn btn-lg btn-primary btn-block" style="width: auto;" type="submit">Modificare parola</button>
</form>
</center>
<?
}
else if (isset($_POST['email']))
{
$email=mres($_POST['email']);
if ($email=='')
    {
        opensession('error', 'Completati e-mail-ul!');
        redirect("index.php?token=uitatparola");
    }
if (!email_valid($email))
    {
        opensession("error", "E-mail invalid!");
        redirect("index.php?token=uitatparola");
    }
if (!query("SELECT * FROM utilizatori WHERE email='$email'"))
    {
        opensession('error', 'E-mail-ul introdus nu exista in baza de date!');
        redirect("index.php?token=uitatparola");
    }
$id=rand(1000000000, 99999999999);
@mysql_query("UPDATE utilizatori SET resetat=$id WHERE email='$email'");
email($email, "Resetare parola ".$site_title_short, "Ati optat pentru resetarea parolei contului dvs. de pe ".$site_title_short.". Pentru a o reseta, accesati urmatorul link:\n\n ".$site_link."index.php?token=procesare_uitatparola&id_unic=$id\n\nCu respect, \nCosmin Crihan (admin)", "contact@".$site_link_short);
opensession('succes', 'Veti primi un e-mail de confirmare pentru resetarea parolei.');
redirect("index.php?token=uitatparola");
}
else redirect("index.php?token=eroare&id=nepermis");
}

else if ($token=="resetare") //ACTUALIZAREA PAROLEI DUPA PROCESAREA CERERII DE RESETARE
{
if (logat() || !isset($_GET['id_unic']) || !is_numeric($_GET['id_unic']))
    redirect("index.php?token=eroare&id=nepermis");
$id=mres($_GET['id_unic']);
$pass=mres($_POST['parola1']);
$pass2=mres($_POST['parola2']);
if (!query("SELECT * FROM utilizatori WHERE resetat=$id"))
    {
    opensession('error', 'Cererea de resetare a parolei nu a fost facuta de pe contul dvs.!');
    redirect("index.php?token=procesare_uitatparola&id_unic=$id");
    }
if ($pass=='' || $pass2=='')
    {
    opensession('error', 'Completati toate campurile!');
    redirect("index.php?token=procesare_uitatparola&id_unic=$id");
    }
if ($pass!=$pass2)
    {
    opensession('error', 'Parolele nu coincid!');
    redirect("index.php?token=procesare_uitatparola&id_unic=$id");
    }
@mysql_query("UPDATE utilizatori SET parola=SHA1('$pass') WHERE resetat=$id");
@mysql_query("UPDATE utilizatori SET resetat=1 WHERE resetat=$id");
opensession('succes', 'Parola dvs. a fost actualizata!');
redirect("index.php?token=succes");
}

else if ($token=="uitatuser") //PAGINA "AM UITAT NUMELE DE UTILIZATOR"
{
if (logat())
    redirect("index.php?token=eroare&id=nepermis");
$email=mres($_POST['email']);
if (!isset($_POST['email']))
{
?>
<center>
<h1>Ati uitat numele de utilizator?</h1>
<form class="form-set" role="form" action="index.php?token=uitatuser" method="POST">
<input class="form-control" type="email" name="email" size="40" placeholder="Introduceti e-mail-ul dvs." required autofocus/>
<?
read_error();
read_succes();
?>
<button class="btn btn-lg btn-primary btn-block" style="width: auto;" type="submit">Trimitere</button>
</form>
</center>
<?
}
else
{
if ($email=='')
    {
    opensession("error", "Introduceti e-mail-ul!");
    redirect("index.php?token=uitatuser");
    }
if (!email_valid($email))
    {
    opensession("error", "E-mail invalid!");
    redirect("index.php?token=uitatuser");
    }
$rez=query("SELECT * FROM utilizatori WHERE email='$email'");
if (!$rez)
    {
    opensession("error", "E-mail-ul introdus nu exista in baza de date!");
    redirect("index.php?token=uitatuser");
    }
$user=$rez['user'];
email("$email", "Nume de utilizator ".$site_title_short, "Ati facut o cerere pentru a va reaminti numele dvs. de utilizator pe site-ul ".$site_title_short.". \n\n Numele dvs. de cont este: >>> $user <<<\n\nIn cazul in care nu dvs. ati facut aceasta cerere, ignorati acest e-mail.\n\nCu respect,\nCosmin Crihan (admin)", "contact@".$site_link_short);
opensession("succes", "Numele dvs. de utilizator v-a fost trimis prin e-mail.");
redirect("index.php?token=uitatuser");
}
}

else if ($token=="retrimite") //PAGINA DE RETRIMITERE A E-MAIL-ULUI CE CONTINE LINK-UL DE ACTIVARE A CONTULUI
{
if (logat())
    redirect("");
if (!isset($_POST['email']))
{?>
<center>
<h1>Retrimitere link de activare</h1><br />
<form class="form-set" role="form" action="index.php?token=retrimite" method="POST">
<input class="form-control" type="user" name="email" size="40" placeholder="Introduceti e-mail-ul dvs." required autofocus/>
<?
read_error();
read_succes();
?>
<button class="btn btn-lg btn-primary btn-block" style="width: auto;" type="submit">Trimitere</button>
</form>
</center>
<?
}
else if (isset($_POST['email']))
{
$email=mres($_POST['email']);
if ($email=='')
    {
    opensession("error", "Introduceti e-mail-ul!");
    redirect("index.php?token=retrimite");
    }
if (!email_valid($email))
    {
    opensession("error", "E-mail invalid!");
    redirect("index.php?token=retrimite");
    }
$rez=query("SELECT * FROM utilizatori WHERE email='$email'");
if (!$rez)
    {
    opensession("error", "E-mail-ul introdus nu exista in baza de date!");
    redirect("index.php?token=retrimite");
    }
if ($rez['activat']==1)
    {
    opensession("error", "Contul utilizatorului cu acest e-mail este deja activ!");
    redirect("index.php?token=retrimite");
    }
$user=$rez['user'];
$r=query("SELECT activat FROM utilizatori WHERE email='$email'");
$id_unic=$r[0];
email($email, "Activare cont ".$site_title_short, "Ati creat contul cu numele de utilizator ".$user." pe ".$site_title_short.". Pentru a-l activa, accesati urmatorul link: \n ".$site_link."index.php?token=activare&id_unic=".$id_unic."\n\nCu respect,\nCosmin Crihan (admin)", "contact@".$site_link_short);
opensession("succes", "V-a fost trimis un e-mail nou cu link-ul de activare al contului dvs. <br/>Verificati e-mail-ul cu care v-ati inregistrat!");
redirect("index.php?token=retrimite");
}
else redirect("index.php?token=eroare&id=nepermis");
}

else if ($token=="postare") //PAGINA DE CREARE A UNEI POSTARI
{
if (!logat())
    redirect("index.php?token=eroare&id=nepermis");
if (!isset($_POST['text']) && !isset($_POST['clasa']) && !isset($_POST['titlu']))
{
?>
<center>
<h1 class="page-header">Postare noua</h1>
<form class="form-set" role="form" action="index.php?token=postare" method="POST" enctype="multipart/form-data">
<?
if (rank($_SESSION['user'])=='E') {
$clasa=get_clasa_by_user($_SESSION['user']);
?>
<label>Clasa pentru care postati: <b><?if ($clasa!='ABS') echo $clasa; else echo "ABSOLVENTI";?></b></label>
<input type="hidden" name="clasa" value="<?=$clasa;?>" />
<?
}
else {
?>
<label>Alegeti clasa pentru care postati: </label>
<select class="form-control" style="width:auto;" name="clasa">
<?
$rez=@mysql_query("SELECT * FROM clase ORDER BY clasa ASC");
while($row=@mysql_fetch_array($rez))
        echo "<option>".$row['clasa']."</option>";
?>
</select>
<?}?>
<input class="form-control" type="text" name="titlu" size="50" placeholder="Titlu" required/>
<textarea class="form-control" style="resize: none;" name="text" rows="10" cols="70" maxlength="100000" placeholder="Continutul postarii" required></textarea>
<label>Imagine (optional)</label>
<input class="form-control" type="file" name="fisier" />
<?read_error();?>
<button class="btn btn-primary btn-block" style="width: auto;" type="submit">Postare</button>
</form>
</center>
<?
}
else if (isset($_POST['text']) && isset($_POST['clasa']) && isset($_POST['titlu']))
{
    $text=mres($_POST['text']);
    $titlu=mres($_POST['titlu']);
    $clasa=mres($_POST['clasa']);
    $user=$_SESSION['user'];
    $id_user=get_id_by_user("$user");
    $id_clasa=get_id_by_class($clasa);
    if ($titlu=='' || $text=='') {
        opensession("error", "Completati toate campurile!");
        go_back();
        exit();
    }
    $p=query("SELECT clasa FROM clase WHERE clasa='$clasa'");
    if (!$p[0]) {
        opensession("error", "Clasa introdusa nu exista in baza de date!");
        go_back();
        exit();
    }
    $extensii=array("jpg", "jpeg", "png", "gif");
    if ($_FILES['fisier']['name'])
    {
        if ($_FILES['fisier']['error'])
            {
            opensession('error', 'Incarcarea a returnat eroarea '.$_FILES['fisier']['error']);
            go_back();
            exit();
            }
        $nume=$_FILES['fisier']['name'];
        $info_fisier=@pathinfo($nume);
        if (!in_array(strtolower($info_fisier['extension']), $extensii))
            {
            opensession('error', 'Extensie neacceptata! Se accepta doar imagini de tip JPG, PNG si GIF!');
            go_back();
            exit();
            }
        $img='uploads/'.$nume;
        if (query("SELECT imagine FROM postari WHERE imagine='$img'"))
            {
            $v=explode(".", $nume);
            $nume=$v[0].time().".".$info_fisier['extension'];
            }
        @move_uploaded_file($_FILES['fisier']['tmp_name'], 'uploads/'.$nume);
        $cale='uploads/'.$nume;
    }
    @mysql_query("INSERT INTO postari (titlu, text, id_clasa, id_user, data, imagine) VALUES ('$titlu', '$text', $id_clasa, $id_user, NOW(), '$cale')");
    opensession("succes", "Postarea a fost adaugata in cronologie.");
    redirect("");
}
else redirect("index.php?token=eroare&id=nepermis");
}

else if ($token=="editare_postare") //EDITAREA UNEI POSTARI
{
if (!logat() || !isset($_GET['id']) || !is_numeric($_GET['id']))
    redirect("index.php?token=eroare&id=nepermis");
$id=mres($_GET['id']);
$id_p=mres($_GET['p']);
$titlu=mres($_POST['titlu']);
$text=mres($_POST['text']);
if (!exist_post($id))
    {
    opensession("error", "Postarea nu exista in baza de date!");
    redirect("index.php?token=eroare");
    }
$r=query("SELECT * FROM postari WHERE id=$id");
if (rank($_SESSION['user'])=='E' && $r['id_user']!=get_id_by_user($_SESSION['user']))
    {
    opensession("error", "Nu aveti permisiunea de a edita aceasta postare!");
    redirect("index.php?token=eroare");
    }
if ($titlu=='' || $text=='')
    {
    opensession("error", "Completati toate campurile!");
    go_back();
    exit();
    }
@mysql_query("UPDATE postari SET text='$text', titlu='$titlu' WHERE id=$id");
redirect("index.php#p".$id_p);
}

else if ($token=="distribuie") //DISTRIBUIREA UNEI POSTARI
{
if (!logat() || !isset($_GET['id']) || !is_numeric($_GET['id']))
    redirect("index.php?token=eroare&id=nepermis");
$id=mres($_GET['id']);
$clasa=mres($_POST['clasa']);
if (!exist_post($id))
    {
    opensession("error", "Postarea nu exista in baza de date!");
    redirect("index.php?token=eroare");
    }
$r=query("SELECT * FROM postari WHERE id=$id");
if (rank($_SESSION['user'])=='E' && $r['id_user']!=get_id_by_user($_SESSION['user']))
    {
    opensession("error", "Nu aveti permisiunea de a distribui aceasta postare!");
    redirect("index.php?token=eroare");
    }
if (exist_image($id))
    {
    $imagine=$r['imagine'];
    $info_fisier=pathinfo($imagine);
    $extensie=$info_fisier['extension'];
    $nume=$info_fisier['filename'];
    $imagine_noua="imagini/".$nume.time().".".$extensie;
    copy($imagine, $imagine_noua);
    }
$titlu=$r['titlu'];
$text=$r['text'];
$id_clasa=get_id_by_class($clasa);
$id_user=get_id_by_user($_SESSION['user']);
$cale=$imagine_noua;
@mysql_query("INSERT INTO postari (titlu, text, id_clasa, id_user, data, imagine) VALUES ('$titlu', '$text', $id_clasa, $id_user, NOW(), '$cale')");
opensession("succes", "Postarea a fost distribuita in cronologia clasei ".$clasa.".");
redirect("");
}

else if ($token=="sterge_postare") //STERGEREA UNEI POSTARI
{
if (!logat() || $_SERVER['REQUEST_METHOD']!='GET' || !isset($_GET['id']) || !is_numeric($_GET['id']))
    redirect("index.php?token=eroare&id=nepermis");
$id=mres($_GET['id']);
if (!exist_post($id))
    {
    opensession("error", "Postarea nu exista in baza de date!");
    redirect("index.php?token=eroare");
    }
$r=query("SELECT * FROM postari WHERE id=$id");
if (rank($_SESSION['user'])=='E' && $r['id_user']!=get_id_by_user($_SESSION['user']))
    {
    opensession("error", "Nu aveti permisiunea de a sterge aceasta postare!");
    redirect("index.php?token=eroare");
    }
if (exist_image($id))
    {
    $q=query("SELECT imagine FROM postari WHERE id=$id");
    unlink($q[0]);
    }
@mysql_query("DELETE FROM comentarii WHERE id_postare=$id");
@mysql_query("DELETE FROM postari WHERE id=$id");
opensession("succes", "Postarea a fost stearsa din cronologie.");
redirect("");
}

else if ($token=="comenteaza") //COMENTAREA UNEI POSTARI
{
if (!logat() || $_SERVER['REQUEST_METHOD']!='POST' || !isset($_POST['comentariu']))
    redirect("index.php?token=eroare&id=nepermis");
$id=mres($_POST['id']);
$comentariu=mres($_POST['comentariu']);
$q=query("SELECT id_clasa FROM postari WHERE id=$id");
if (rank($_SESSION['user'])=='E' && ($q[0]!=get_id_by_class(get_clasa_by_user($_SESSION['user']))) && $q[0]!=1)
    {
    opensession("error", "Nu aveti permisiunea de a comenta aceasta postare!");
    redirect("index.php?token=eroare");
    }
if ($comentariu=='') {
    go_back();
    exit();
    }
@mysql_query("INSERT INTO comentarii (text, id_user, data, id_postare) VALUES ('$comentariu', '".get_id_by_user($_SESSION['user'])."', NOW(), $id)");
$id_p=$_GET['p'];
if ($id_p==0) $id_p="";
else $id_p="#p".$id_p;
redirect("index.php".$id_p);
}

else if ($token=="sterge_comentariu") //STERGEREA UNUI COMENTARIU
{
if (!logat() || $_SERVER['REQUEST_METHOD']!='GET' || !isset($_GET['id']))
    redirect("index.php?token=eroare&id=nepermis");
$id=mres($_GET['id']);
$q=query("SELECT * FROM comentarii WHERE id=$id");
if (rank($_SESSION['user'])=='E' && $q['id_user']!=get_id_by_user($_SESSION['user']))
    {
    opensession("error", "Nu aveti permisiuni asupra acestui comentariu!");
    redirect("index.php?token=eroare");
    }
@mysql_query("DELETE FROM comentarii WHERE id=$id");
$id_p=$_GET['p'];
if ($id_p==0) $id_p="";
else $id_p="#p".$id_p;
redirect("index.php".$id_p);
}

else if ($token=="like") //APRECIEREA UNEI POSTARI
{
if (!logat() || $_SERVER['REQUEST_METHOD']!='GET' || !isset($_GET['id']))
    redirect("index.php?token=eroare&id=nepermis");
$id=mres($_GET['id']);
$r=query("SELECT * FROM postari WHERE id=$id");
if (rank($_SESSION['user'])=='E' && $r['id_clasa']!=get_id_by_class(get_clasa_by_user($_SESSION['user'])) && $r['id_clasa']!=1)
    {
    opensession("error", "Nu aveti permisiuni asupra acestei postari!");
    redirect("index.php?token=eroare");
    }
if (!exist_post($id))
    {
    opensession("error", "Postarea nu exista in baza de date!");
    redirect("index.php?token=eroare");
    }
if (disliked($id))
    {
    $q=query("SELECT neapreciat FROM postari WHERE id=$id");
    $p=str_replace(get_id_by_user($_SESSION['user'])."/", "", $q[0]);
    @mysql_query("UPDATE postari SET neapreciat='$p' WHERE id=$id");
    }
$q=query("SELECT apreciat FROM postari WHERE id=$id");
if (strstr($q[0], get_id_by_user($_SESSION['user']))) redirect("");
$p=$q[0].get_id_by_user($_SESSION['user'])."/";
@mysql_query("UPDATE postari SET apreciat='$p' WHERE id=$id");
$id_p=$_GET['p'];
if ($id_p==0) $id_p="";
else $id_p="#p".$id_p;
redirect("index.php".$id_p);
}

else if ($token=="dislike") //"NEAPRECIEREA" UNEI POSTARI
{
if (!logat() || $_SERVER['REQUEST_METHOD']!='GET' || !isset($_GET['id']))
    redirect("index.php?token=eroare&id=nepermis");
$id=mres($_GET['id']);
$r=query("SELECT * FROM postari WHERE id=$id");
if (rank($_SESSION['user'])=='E' && $r['id_clasa']!=get_id_by_class(get_clasa_by_user($_SESSION['user'])) && $r['id_clasa']!=1)
    {
    opensession("error", "Nu aveti permisiuni asupra acestei postari!");
    redirect("index.php?token=eroare");
    }
if (!exist_post($id))
    {
    opensession("error", "Postarea nu exista in baza de date!");
    redirect("index.php?token=eroare");
    }
if (liked($id))
    {
    $q=query("SELECT apreciat FROM postari WHERE id=$id");
    $p=str_replace(get_id_by_user($_SESSION['user'])."/", "", $q[0]);
    @mysql_query("UPDATE postari SET apreciat='$p' WHERE id=$id");
    }
$q=query("SELECT neapreciat FROM postari WHERE id=$id");
if (strstr($q[0], get_id_by_user($_SESSION['user']))) redirect("");
$p=$q[0].get_id_by_user($_SESSION['user'])."/";
@mysql_query("UPDATE postari SET neapreciat='$p' WHERE id=$id");
$id_p=$_GET['p'];
if ($id_p==0) $id_p="";
else $id_p="#p".$id_p;
redirect("index.php".$id_p);
}

else if ($token=="unlike") //ANULAREA APRECIERII UNEI POSTARI
{
if (!logat() || $_SERVER['REQUEST_METHOD']!='GET' || !isset($_GET['id']) || !is_numeric($_GET['id']))
    redirect("index.php?token=eroare&id=nepermis");
$id=mres($_GET['id']);
$q=query("SELECT apreciat FROM postari WHERE id=$id");
$p=str_replace(get_id_by_user($_SESSION['user'])."/", "", $q[0]);
@mysql_query("UPDATE postari SET apreciat='$p' WHERE id=$id");
$id_p=$_GET['p'];
if ($id_p==0) $id_p="";
else $id_p="#p".$id_p;
redirect("index.php".$id_p);
}

else if ($token=="undislike") //ANULAREA "NEAPRECIERII" UNEI POSTARI
{
if (!logat() || $_SERVER['REQUEST_METHOD']!='GET' || !isset($_GET['id']) || !is_numeric($_GET['id']))
    redirect("index.php?token=eroare&id=nepermis");
$id=mres($_GET['id']);
$q=query("SELECT neapreciat FROM postari WHERE id=$id");
$p=str_replace(get_id_by_user($_SESSION['user'])."/", "", $q[0]);
@mysql_query("UPDATE postari SET neapreciat='$p' WHERE id=$id");
$id_p=$_GET['p'];
if ($id_p==0) $id_p="";
else $id_p="#p".$id_p;
redirect("index.php".$id_p);
}

else if ($token=="admin") //PAGINA PROFILULUI PERSONAL
{
if (!logat())
    redirect("index.php?token=eroare&id=nepermis");
if (!($detectare->isMobile() && !$detectare->isTablet())) {
?>
<div style="float: right; width: 30%; padding-top: 40px;">
<?
echo "<img style='border-radius:10px; float: right;' src='".get_thumb($_SESSION['user'])."' width='200' height='200'/>";
?>
<br style="clear: both;" />
<h3>
<?if (thumb_exist($_SESSION['user'])) {?><a href="index.php?token=sterge_fotografie" onclick="return confirm('Sigur stergeti fotografia de profil?');"><span style="padding-left: 5px; float: right;" class="glyphicon glyphicon-remove"></span></a><?}?>
<a href="index.php?token=adauga_fotografie"><span style="float: right;" class="glyphicon glyphicon-<? if (!thumb_exist($_SESSION['user'])) echo "plus"; else echo "edit";?>"></span></a> 
</h3>
</div>
<h1 class="page-header">Profilul meu</h1>
<div style="float:  left; width: 60%; height: auto;">
<?read_succes();?>
<h3>Nume si prenume: 
<?
echo get_first_name_by_user($_SESSION['user'])." ".get_surname_by_user($_SESSION['user'])."  ";
?>
<a href="index.php?token=modifica&id=name"><span class="glyphicon glyphicon-edit"></span></a></h3>
<h3>Nume de utilizator: 
<?echo $_SESSION['user']."  ";?>
<a href="index.php?token=modifica&id=user"><span class="glyphicon glyphicon-edit"></span></a></h3>
<h3>Parola <a href="index.php?token=modifica&id=pass"><span class="glyphicon glyphicon-edit"></span></a></h3>
<h3>E-mail: <?=get_email_by_user($_SESSION['user']);?></h3>
<?if (rank($_SESSION['user'])=='E') {?><h3>Clasa: <?=get_clasa_by_user($_SESSION['user']);?></h3><?}?>
<h3>Numar de mesaje trimise: <?=get_nr_trimise($_SESSION['user']);?></h3>
<h3>Numar de fisiere incarcate: <?=get_nr_fisiere($_SESSION['user']);?></h3>
<?if (rank($_SESSION['user'])!='E') {?>
<h3>Spatiu de stocare folosit: <?=get_spatiu_user($_SESSION['user']);?> MB</h3>
<?}
else {
    $dim=get_spatiu_user($_SESSION['user']);
    $max=get_dim_max();
    $proc=round(100*$dim/$max, 2);
    $dim=round($dim, 2);
?>
<h3>Spatiu de stocare folosit: <?=$dim." MB";?></h3>
<div class="progress" style="height: 40px;">
  <div class="progress-bar" role="progressbar" aria-valuenow="<?=$proc;?>" aria-valuemin="0" aria-valuemax="100" style="width: <?=$proc;?>%;">
    <p style="font-size: 1.4em; padding-top: 10px;"><?=$proc;?>%</p>
  </div>
</div>
<?
}
?>
</div>
<?}
else {
?>
<center>
<h1 class="page-header">Profilul meu</h1>
<?
echo "<img style='border-radius:10px;' src='".get_thumb($_SESSION['user'])."' width='200' height='200'/>";
?>
<br style="clear: both;" />
<h1>
<a href="index.php?token=adauga_fotografie"><span class="glyphicon glyphicon-<? if (!thumb_exist($_SESSION['user'])) echo "plus"; else echo "edit";?>"></span></a> 
<?if (thumb_exist($_SESSION['user'])) {?><a href="index.php?token=sterge_fotografie"><span style="padding-left: 5px;" class="glyphicon glyphicon-remove"></span></a><?}?>
</h1>
</center>
<?read_succes();?>
<h4>Nume si prenume: 
<?
echo get_first_name_by_user($_SESSION['user'])." ".get_surname_by_user($_SESSION['user'])."  ";
?>
<a href="index.php?token=modifica&id=name"><span class="glyphicon glyphicon-edit"></span></a></h4>
<h4>Nume de utilizator: 
<?echo $_SESSION['user']."  ";?>
<a href="index.php?token=modifica&id=user"><span class="glyphicon glyphicon-edit"></span></a></h3>
<h4>Parola <a href="index.php?token=modifica&id=pass"><span class="glyphicon glyphicon-edit"></span></a></h4>
<h4>E-mail: <?=get_email_by_user($_SESSION['user']);?></h4>
<?if (rank($_SESSION['user'])=='E') {?><h4>Clasa: <?=get_clasa_by_user($_SESSION['user']);?></h4><?}?>
<h4>Numar de mesaje trimise: <?=get_nr_trimise($_SESSION['user']);?></h4>
<h4>Numar de fisiere incarcate: <?=get_nr_fisiere($_SESSION['user']);?></h4>
<?if (rank($_SESSION['user'])!='E') {?>
<h4>Spatiu de stocare folosit: <?=get_spatiu_user($_SESSION['user']);?> MB</h4>
<?}
else {
    $dim=get_spatiu_user($_SESSION['user']);
    $max=get_dim_max();
    $proc=round(100*$dim/$max, 2);
    $dim=round($dim, 2);
?>
<h4>Spatiu de stocare folosit: <?=$dim." MB";?></h4>
<div class="progress" style="height: 40px;">
  <div class="progress-bar" role="progressbar" aria-valuenow="<?=$proc;?>" aria-valuemin="0" aria-valuemax="100" style="width: <?=$proc;?>%;">
    <p style="font-size: 2em; padding-top: 10px;"><?=$proc;?>%</p>
  </div>
</div>
<?
}
}
}

else if ($token=="adauga_fotografie") //PAGINA DE ADAUGARE/MODIFICARE A FOTOGRAFIEI DE PROFIL
{
if (!logat())
    redirect("index.php?token=eroare&id=nepermis");
if ($_SERVER['REQUEST_METHOD']=='POST')
{
$extensii=array("jpg", "png", "gif", "jpeg");
if (!$_FILES['fisier']['name']) //daca nu exista nicio imagine incarcata
{
    opensession('error', 'Nu ati incarcat nicio imagine!');
    redirect("index.php?token=adauga_fotografie");
}
if ($_FILES['fisier']['error']) //daca exista vreo eroare o afisam
{
    opensession('error', 'Incarcarea a returnat eroarea '.$_FILES['fisier']['error']);
    redirect("index.php?token=adauga_fotografie");
}
$nume=$_FILES['fisier']['name']; //preluam numele
$info_fisier=@pathinfo($nume); //preluam informatiile fisierului in vector
$marime=$_FILES['fisier']['size']/1024; //calculam dimensiunea in KB
if (!in_array(strtolower($info_fisier['extension']), $extensii)) //daca extensia nu se afla printre cele acceptate
{
    opensession('error', 'Extensie neacceptata! Se accepta doar imagini de tip JPG, PNG si GIF!');
    redirect("index.php?token=adauga_fotografie");
}
if ($_FILES['fisier']['size']>get_dim_max_thumb()*1024000 && rank($_SESSION['user'])=='E') //daca user-ul este de tip elev si dimensiunea o depaseste pe cea predefinita
{
    opensession('error', 'Fisierul este prea mare! Acesta nu trebuie sa depaseasca '.get_dim_max_thumb().' MB.');
    redirect("index.php?token=adauga_fotografie");
}
$rand=strtotime(date('Y-m-d H:i:s')); //construim un timestamp pentru randomizarea numelui
move_uploaded_file($_FILES['fisier']['tmp_name'], 'uploads/'.$nume); //mutam fisierul, temporar, in folderul cu incarcari
$cale="upload_pic/thumbnail_".$rand.$info_fisier['extension']; //construim calea noua pentru imaginea scalata
scaleImage("uploads/".$nume, 300, 300, $cale); //scalam imaginea la 300px x 300px
unlink("uploads/".$nume); //stergem imaginea originala, folosita
if (thumb_exist($_SESSION['user'])) //daca user-ul are deja o imagine de profil
    {$poza=get_thumb($_SESSION['user']); //o preluam si o stergem
    unlink($poza);}
@mysql_query("UPDATE utilizatori SET thumb='$cale' WHERE user='".$_SESSION['user']."'"); //setam noua imagine
opensession("succes", "Fotografia a fost setata cu succes!");
redirect("index.php?token=admin");
}
else
{
?>
<center>
<h1 class="page-header"><?if (!thumb_exist($_SESSION['user'])) echo "Incarcare"; else echo "Modificare";?> fotografie de profil</h1>
<div class="alert alert-info" style="width: 60%;" role="alert">Incarcati, pe cat posibil, o fotografie ce are raportul lungime/latime cat mai aproape de 1:1, deoarece va fi scalata automat la dimensiunea de 300x300 pixeli!</div>
<?read_error();?>
<form class="form-set" role="form" enctype="multipart/form-data" action="index.php?token=adauga_fotografie" method="post">
<input class="form-control" type="file" name="fisier" size="30" required/> 
<button class="btn btn-primary btn-block" style="width: auto;" type="submit">Incarcare</button>
</form>
</center>
<?
}
}

else if ($token=="sterge_fotografie") //STERGEREA FOTOGRAFIEI DE PROFIL
{
if (!logat())
    redirect("index.php?token=eroare&id=nepermis");
$poza=get_thumb($_SESSION['user']);
unlink($poza);
@mysql_query("UPDATE utilizatori SET thumb='' WHERE user='".$_SESSION['user']."'");
opensession("succes", "Fotografia a fost stearsa cu succes.");
redirect("index.php?token=admin");
}

else if ($token=="modifica") //PAGINA DE MODIFICARE A DATELOR PERSONALE + PAGINILE DE PROCESARE PENTRU MODIFICARI
{
if (!logat())
    redirect("index.php?token=eroare&id=nepermis");
$id=mres($_GET['id']);
if ($id=='')
    redirect("index.php?token=eroare&id=nepermis");
?>
<h1 class="page-header" style="text-align: center;">Modificare
    <?if ($id=='name') echo " nume";
    else if ($id=='surname') echo " prenume";
    else if ($id=='pass') echo " parola";
    else if ($id=='user') echo " nume de utilizator";
    else if ($id=='dim') echo " dimensiune maxima pentru utilizatori";
    else if ($id=='dimthumb') echo "dimensiune maxima pentru fotografia de profil";
    else redirect("index.php?token=eroare&id=nepermis");?>
</h1>
<br />
<?
if ($id=='user') //PAGINA DE MODIFICARE USERNAME
{
if (!isset($_POST['user']) || !isset($_POST['user2']))
{
?>
<center>
<form class="form-set" role="form" action="index.php?token=modifica&id=user" method="POST">
<input class="form-control" type="text" maxlength="25" max="25" name="user" placeholder="Nume de utilizator nou" required/>
<input class="form-control" type="text" maxlength="25" max="25" name="user2" placeholder="Confirmati numele de utilizator" required/>
<?
read_error();
?>
<button class="btn btn-primary btn-block" style="width: auto;" type="submit">Modificare</button>
</form>
</center>
<?
}
else { //PAGINA DE PROCESARE - MODIFICARE USERNAME
if (!logat())
    redirect("index.php?token=eroare&id=nepermis");
$user=mres($_POST['user']);
$user2=mres($_POST['user2']);
if ($user=='' || $user2=='')
    {
    opensession("error", "Completati toate campurile!");
    redirect("index.php?token=modifica&id=user");
    }
if ($user!=$user2)
    {
    opensession("error", "Numele de utilizator nu coincid!");
    redirect("index.php?token=modifica&id=user");
    }
if (!valid_username($user))
    {
    opensession("error", 'Nume de utilizator invalid! Acesta trebuie sa inceapa cu cel putin o litera, sa aiba o lungime cuprinsa intre 6 si 30 de caractere, poate contine doar caractere alfanumerice, respectiv caracterul underscore ("_"), punct (".") sau cratima ("-") si nu poate contine spatii goale!');
    redirect("index.php?token=modifica&id=user");  
    }
if ($user==$_SESSION['user'])
    {
    opensession("error", "Va rugam introduceti un nume de utilizator diferit de cel curent!");
    redirect("index.php?token=modifica&id=user");
    }
if (exist_user($user))
    {
    opensession("error", "Numele de utilizator exista deja in baza de date!");
    redirect("index.php?token=modifica&id=user"); 
    }
@mysql_query("UPDATE utilizatori SET user='$user' WHERE user='".$_SESSION['user']."'");
opensession("succes", "Numele de utilizator a fost modificat!");
if ($_COOKIE['66pWKLbxb1lWoWkvkd2T'])
    $_COOKIE['66pWKLbxb1lWoWkvkd2T']=$user;
opensession("user", "$user");
redirect("index.php?token=admin");
}
}
else if ($id=='pass') //PAGINA DE MODIFICARE A PAROLEI
{
if (!isset($_POST['pass']) || !isset($_POST['passnew']) || !isset($_POST['passnew2']))
{
?>
<center>
<form class="form-set" role="form" action="index.php?token=modifica&id=pass" method="POST">
<input class="form-control" type="password" maxlength="40" max="40" name="pass" placeholder="Parola curenta" required/>
<input class="form-control" type="password" maxlength="40" max="40" name="passnew" placeholder="Parola noua" required/>
<input class="form-control" type="password" maxlength="40" max="40" name="passnew2" placeholder="Confirmare parola noua" required/>
<?
read_error();
?>
<button class="btn btn-primary btn-block" style="width: auto;" type="submit">Modificare</button>
</form>
</center>
<?
}
else //PAGINA DE PROCESARE - MODIFICARE PAROLA
{
if (!logat())
    redirect("index.php?token=eroare&id=nepermis");
$pass=mres($_POST['pass']);
$passnew=mres($_POST['passnew']);
$passnew2=mres($_POST['passnew2']);
if ($pass=='' || $passnew=='' || $passnew2=='')
    {
    opensession("error", "Completati toate campurile!");
    redirect("index.php?token=modifica&id=pass");
    }
if (SHA1($pass)!=get_parola_by_user($_SESSION['user']))
    {
    opensession("error", "Parola curenta incorecta!");
    redirect("index.php?token=modifica&id=pass");
    }
if (strlen($passnew)>40 || strlen($passnew)<6)
    {
    opensession('error', 'Parola noua trebuie sa aiba intre 6 si 40 de caractere!');
    redirect("index.php?token=modifica&id=pass"); 
    }
if ($passnew!=$passnew2)
    {
    opensession("error", "Parolele nu coincid!");
    redirect("index.php?token=modifica&id=pass");
    }
if (SHA1($passnew)==get_parola_by_user($_SESSION['user']))
    {
    opensession("error", "Introduceti o parola noua diferita de cea curenta!");
    redirect("index.php?token=modifica&id=pass");
    }
@mysql_query("UPDATE utilizatori SET parola='".SHA1($passnew)."' WHERE user='".$_SESSION['user']."'");
opensession("succes", "Parola a fost modificata!");
if ($_COOKIE['66pWKLbxb1lWoWkvkd2T'])
    $_COOKIE['CqRh6hveZ7B0Din0eJhY']=SHA1($passnew);
redirect("index.php?token=admin");
}
}
else if ($id=='name') //PAGINA DE MODIFICARE A NUMELUI SI PRENUMELUI
{
if (!isset($_POST['name']) || !isset($_POST['surname']))
{
?>
<center>
<form class="form-set" role="form" action="index.php?token=modifica&id=name" method="POST">
<input class="form-control" type="text" maxlength="25" max="25" name="name" placeholder="Noul nume" required/>
<input class="form-control" type="text" maxlength="25" max="25" name="surname" placeholder="Noul prenume" required/>
<?
read_error();
?>
<button class="btn btn-primary btn-block" style="width: auto;" type="submit">Modificare</button>
</form>
</center>
<?
}
else { //PAGINA DE PROCESARE - MODIFICARE NUME SI PRENUME
if (!logat())
    redirect("index.php?token=eroare&id=nepermis");
$nume=mres($_POST['name']);
$prenume=mres($_POST['surname']);
if ($nume=='' || $prenume=='')
    {
    opensession("error", "Introduceti un nume si un prenume nou!");
    redirect("index.php?token=modifica&id=name");
    }
if ($nume==get_first_name_by_user($_SESSION['user']) && $prenume==get_surname_by_user($_SESSION['user']))
    {
    opensession("error", "Introduceti un nume diferit de cel curent!");
    redirect("index.php?token=modifica&id=name");
    }
@mysql_query("UPDATE utilizatori SET nume='$nume', prenume='$prenume' WHERE user='".$_SESSION['user']."'");
opensession("succes", "Numele si prenumele au fost modificate!");
redirect("index.php?token=admin");
}
}
else if ($id=='dim') //PAGINA DE MODIFICARE A DIMENSIUNII MAXIME PENTRU UTILIZATORII ELEVI
{
if (!isset($_POST['dim']))
{
?>
<center>
<form class="form-set" role="form" action="index.php?token=modifica&id=dim" method="POST">
<h4>Dimensiune maxima actuala: <label><?=get_dim_max();?> MB</label></h4>
<input class="form-control" type="text" maxlength="4" max="4" name="dim" placeholder="Noua dimensiune maxima (in MB)" required/>
<?
read_error();
?>
<button class="btn btn-primary btn-block" style="width: auto;" type="submit">Modificare</button>
</form>
</center>
<?
}
else { //PAGINA DE PROCESARE - MODIFICARE DIMENSIUNE MAXIMA PENTRU ELEVI
if (!logat())
    redirect("index.php?token=eroare&id=nepermis");
$dim=mres($_POST['dim']);
if (!is_numeric($dim))
    {
    opensession("error", "Introduceti o valoare numerica pentru noua dimensiune!");
    redirect("index.php?token=modifica&id=dim");
    }
if ($dim=='' || $dim<=0 || $dim>1024)
    {
    opensession("error", "Introduceti o valoare mai mare ca 0 si mai mica ca 1024!");
    redirect("index.php?token=modifica&id=dim"); 
    }
if (get_spatiu_max_ocupat()>$dim)
    {
    opensession("error", "Dimensiunea noua trebuie sa fie mai mare ca ".get_spatiu_max_ocupat()." MB!");
    redirect("index.php?token=modifica&id=dim"); 
    }
@mysql_query("UPDATE setari SET dim_max=$dim");
opensession("succes", "Dimensiunea maxima pentru utilizatori a fost actualizata!");
redirect("index.php?token=setari");
}
}
else if ($id=='dimthumb') //PAGINA DE MODIFICARE A DIMENSIUNII MAXIME PENTRU IMAGINEA DE PROFIL
{
if (!isset($_POST['dimthumb']))
{
?>
<center>
<form class="form-set" role="form" action="index.php?token=modifica&id=dimthumb" method="POST">
<h4>Dimensiune maxima actuala: <label><?=get_dim_max_thumb();?> MB</label></h4>
<input class="form-control" type="text" maxlength="2" max="2" name="dimthumb" placeholder="Noua dimensiune maxima (in MB)" required/>
<?
read_error();
?>
<button class="btn btn-primary btn-block" style="width: auto;" type="submit">Modificare</button>
</form>
</center>
<?
}
else { //PAGINA DE PROCESARE - MODIFICARE DIMENSIUNE MAXIMA PENTRU IMAGINEA DE PROFIL
if (!logat() || $_SERVER['REQUEST_METHOD']!='POST')
    redirect("index.php?token=eroare&id=nepermis");
$dim=mres($_POST['dimthumb']);
if (!is_numeric($dim))
    {
    opensession("error", "Introduceti o valoare numerica pentru noua dimensiune!");
    redirect("index.php?token=modifica&id=dimthumb");
    }
if ($dim=='' || $dim<=0 || $dim>30)
    {
    opensession("error", "Introduceti o valoare mai mare ca 0 si mai mica ca 30!");
    redirect("index.php?token=modifica&id=dimthumb");
    }
@mysql_query("UPDATE setari SET max_thumb=$dim");
opensession("succes", "Dimensiunea maxima pentru fotografia de profil a fost actualizata!");
redirect("index.php?token=setari");
}
}
}

else if ($token=="setari") //PAGINA CU SETARILE PLATFORMEI
{
if (!logat() || rank($_SESSION['user'])!='A')
    redirect("index.php?token=eroare&id=nepermis");
?>
<div class="panel panel-default">
<div class="panel-heading"><h1>Setari si informatii</h1></div>
<div class="panel-body">
<?
read_succes();
?>
<h4>E-mail-uri/mesaje blocate: <?if (bea()) echo 'DA'; else echo 'NU';?>&nbsp; <a href="index.php?token=block_emails"><span class="glyphicon <? if (bea()) echo "glyphicon-lock"; else echo "glyphicon-ok"; echo '">';?></span></a></h4>
<h4>Incarcari blocate: <?if (bua()) echo 'DA'; else echo 'NU';?>&nbsp; <a href="index.php?token=block_uploads"><span class="glyphicon <? if (bua()) echo "glyphicon-lock"; else echo "glyphicon-ok"; echo '">';?></span></a></h4>
<h4>Dimensiune maxima pentru utilizatori: <?=get_dim_max();?> MB&nbsp; <a href="index.php?token=modifica&id=dim"><span class="glyphicon glyphicon-pencil"></span></a></h4>
<h4>Dimensiune maxima pentru fotografia de profil: <?=get_dim_max_thumb();?> MB&nbsp; <a href="index.php?token=modifica&id=dimthumb"><span class="glyphicon glyphicon-pencil"></span></a></h4>
<h4>Tipuri de extensii respinse pentru fisierele incarcate: <?=show_extensii();?></h4>
<h4>Total spatiu ocupat de fisierele incarcate: <?=spatiu_ocupat();?> MB</h4>
<?
$e=query("SELECT COUNT(*) FROM utilizatori WHERE rank='E'");
$p=query("SELECT COUNT(*) FROM utilizatori WHERE rank='P'");
$a=query("SELECT COUNT(*) FROM utilizatori WHERE rank='A'");
echo "<h4>Numarul utilizatorilor:</h4>
<ul type='bullet'>
<li>elevi: ".$e[0]."</li>
<li>profesori: ".$p[0]."</li>
<li>administratori: ".$a[0]."</li>
</ul>";
?>
</div>
<?
}

else if ($token=="block_emails") //BLOCARE/DEBLOCARE E-MAIL-URI SI MESAJE PRIVATE
{
if (!logat() || rank($_SESSION['user'])!='A')
    redirect("index.php?token=eroare&id=nepermis");
if (bea()) @mysql_query("UPDATE setari SET blocked_emails=0");
else @mysql_query("UPDATE setari SET blocked_emails=1");
redirect("index.php?token=setari");
}

else if ($token=="block_uploads") //BLOCARE/DEBLOCARE INCARCARI DE FISIERE
{
if (!logat() || rank($_SESSION['user'])!='A')
    redirect("index.php?token=eroare&id=nepermis");
if (bua()) @mysql_query("UPDATE setari SET blocked_uploads=0");
else @mysql_query("UPDATE setari SET blocked_uploads=1");
redirect("index.php?token=setari");
}

else if ($token=="blocare") //PAGINA DE BLOCARE/DEBLOCARE CONT
{
if (!logat() || rank($_SESSION['user'])=='E')
    redirect("index.php?token=eroare&id=nepermis");
if (!isset($_POST['user_b']) && !isset($_POST['user_d']))
{?>
<center>
<h1 class="page-header">Blocare/deblocare cont</h1>
<form class="form-set" role="form" action="index.php?token=blocare" method="POST">
<label>Introduceti numele de utilizator al carui cont doriti sa fie blocat:</label>
<input class="form-control" type="text" max="30" maxlength="30" name="user_b" required/>
<?
if ($_SESSION['errorb'])
    {
    echo '<div class="alert alert-danger" role="alert">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <h4>Eroare!</h4>
    '.$_SESSION['errorb'].'</div>';
    $_SESSION['errorb']='';
    }
?>
<button class="btn btn-primary btn-block" style="width: auto;" type="submit">Blocare</button>
</form>
<br />
<form class="form-set" role="form" action="index.php?token=blocare" method="POST">
<label>Introduceti numele de utilizator al carui cont doriti sa fie deblocat:</label>
<input class="form-control" type="text" max="30" maxlength="30" name="user_d" required/>
<?
if ($_SESSION['errord'])
    {
    echo '<div class="alert alert-danger" role="alert">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <h4>Eroare!</h4>
    '.$_SESSION['errord'].'</div>';
    $_SESSION['errord']='';
    }
?>
<button class="btn btn-primary btn-block" style="width: auto;" type="submit">Deblocare</button>
</form>
</center>
<?
}
if (isset($_POST['user_b']))
{
    $user=mres($_POST['user_b']);
    if ($user=='')
        {
        opensession("errorb", "Introduceti un nume de utilizator!");
        redirect("index.php?token=blocare");
        }
    if (!exist_user($user))
        {
        opensession("errorb", "Numele de utilizator nu exista in baza de date!");
        redirect("index.php?token=blocare");
        }
    if ($user==$_SESSION['user'])
        {
        opensession("errorb", "Nu va puteti bloca propriul cont!");
        redirect("index.php?token=blocare");
        }
    if (rank($user)!='E')
        {
        opensession("errorb", "Nu puteti bloca contul unui profesor sau al unui administrator!");
        redirect("index.php?token=blocare");   
        }
    if (is_blocat($user))
        {
        opensession("errorb", "Utilizatorul este deja blocat!");
        redirect("index.php?token=blocare");   
        }
    @mysql_query("UPDATE utilizatori SET blocat=1 WHERE user='$user'");
    opensession("succes", "Contul utilizatorului ".$user." a fost blocat!");
    redirect("index.php?token=succes");
}
if (isset($_POST['user_d']))
{
    $user=mres($_POST['user_d']);
    if ($user=='')
        {
        opensession("errord", "Introduceti un nume de utilizator!");
        redirect("index.php?token=blocare");
        }
    if (!exist_user($user))
        {
        opensession("errorb", "Numele de utilizator nu exista in baza de date!");
        redirect("index.php?token=blocare");
        }
    if ($user==$_SESSION['user'])
        {
        opensession("errorb", "Nu va puteti debloca propriul cont!");
        redirect("index.php?token=blocare");
        }
    if (rank($user)!='E')
        {
        opensession("errord", "Nu puteti debloca contul unui profesor sau al unui administrator!");
        redirect("index.php?token=blocare");   
        }
    if (!is_blocat($user))
        {
        opensession("errord", "Utilizatorul nu este blocat!");
        redirect("index.php?token=blocare");   
        }
    @mysql_query("UPDATE utilizatori SET blocat=0 WHERE user='$user'");
    opensession("succes", "Contul utilizatorului ".$user." a fost deblocat!");
    redirect("index.php?token=succes");
}
}

else if ($token=="difuzare") //PAGINA DE DIFUZARE A UNUI MESAJ
{
if (!logat() || (rank($_SESSION['user'])!='P' && rank($_SESSION['user'])!='A'))
    redirect("index.php?token=eroare&id=nepermis");
if (!isset($_POST['clasa']) && !isset($_POST['mesaj']) && !isset($_POST['subiect']))
{
?>
<center>
<h1 class="page-header">Difuzare mesaj</h1>
<form class="form-set" role="form" action="index.php?token=difuzare" method="POST">
<label>Alegeti clasa: </label>
<select class="form-control" style="width:auto;" name="clasa" required>
<?
$q=@mysql_query("SELECT * FROM clase ORDER BY clasa ASC");
while ($row=@mysql_fetch_array($q))
    if ($row['clasa']!='ALL') echo "<option>".$row['clasa']."</option>";
?>
</select>
<input class="form-control" type="text" name="subject" size="50" placeholder="Subiect"/>
<textarea class="form-control" style="resize: none;" name="mesaj" cols="60" rows="10" placeholder="Mesaj" required></textarea><br />
<?
read_error();
read_succes();
?>
<button class="btn btn-primary btn-block" style="width: auto;" type="submit">Transmitere</button>
</form>
</center>
<?
}
else
{
    $clasa=mres($_POST['clasa']);
    $id_clasa=get_id_by_class($clasa);
    $message=mres($_POST['mesaj']);
    $subject=mres($_POST['subject']);
    $from=$_SESSION['user'];
    $id_from=get_id_by_user($from);
    if ($message=='' && $subject=='' || $clasa=='')
        {
        opensession("error", "Completati toate campurile!");
        redirect("index.php?token=difuzare");
        }
    $c=query("SELECT COUNT(*) FROM utilizatori WHERE id_clasa=$id_clasa");
    if ($c[0]==0)
        {
        opensession("error", "Nu exista niciun utilizator in aceasta clasa!");
        redirect("index.php?token=difuzare");
        }
    if ($subject=='')
        $subject="&lt;fara subiect&gt;";
    if ($message=='')
        {
        opensession("error", "Scrieti un mesaj!");
        redirect("index.php?token=difuzare");
        }
    $q=@mysql_query("SELECT * FROM utilizatori WHERE id_clasa=".get_id_by_class($clasa));
    while ($u=@mysql_fetch_array($q))
        {
        $touser=$u['user'];
        $id_touser=get_id_by_user($touser);
        @mysql_query("INSERT INTO mesaje (id_user, mesaj, de_la, subiect, data, citit) VALUES ($id_touser, '$message', $id_from, '$subject', NOW(), 0)");
        $to=get_email_by_user($touser);
        email($to, "Ati primit un PM pe ".$site_title_short, "Aceasta este o instiintare cum ca ati primit un mesaj privat pe ".$site_title_short.", cu subiectul <<".$subject.">>.\n\nPentru a-l citi, vizitati ".$site_link.".\n\nCu respect, \nCosmin Crihan (admin).\n\nAcesta este un mesaj trimis automat. Nu raspundeti acestui e-mail.", "contact@".$site_link_short);
        }
    opensession("succes", "Mesajul a fost transmis utilizatorilor din clasa ".$clasa."!");
    redirect("index.php?token=difuzare");
}
}

else if ($token=="utilizatori") //PAGINA CU LISTA UTILIZATORILOR
{
if (!logat())
    redirect("index.php?token=eroare&id=nepermis");
?>
<center>
<h1 class="page-header">Lista utilizatorilor <?=$site_title_short;?></h1>
<div class="table-responsive">
<table class="table">
<thead>
<th style="text-align: center;">Nume si prenume</th>
<th style="text-align: center;">Nume de utilizator</th>
<th style="text-align: center;">Clasa / Materia predata</th>
<th style="text-align: center;">Numar de mesaje trimise</th>
<th style="text-align: center;">Numar de fisiere incarcate</th>
<th style="text-align: center;">E-mail</th>
<th style="text-align: center;">Stare</th>
</thead>
<tbody>
<?
if (rank($_SESSION['user'])=='E')
    $q=@mysql_query("SELECT * FROM utilizatori WHERE rank='E' ORDER BY id_clasa, user ASC");
else $q=@mysql_query("SELECT * FROM utilizatori ORDER BY id_clasa, user ASC");
while ($r=@mysql_fetch_array($q))
    {
    $clasa=get_class_by_id($r['id_clasa']);
    if ($clasa=='ABS') continue;
    echo "<tr><td style='text-align:center;'>".$r['nume']." ".$r['prenume']."</td><td style='text-align:center;'><img src='".get_thumb($r['user'])."' style='border-radius:5px; vertical-align:middle;' width='30' height='30'/><b><a href='index.php?token=profil&id=".get_id_by_user($r['user'])."'>".$r['user']." ";
    if (rank($r['user'])=='A') echo "<font color='red'>(ADMIN)</font>";
    else if (rank($r['user'])=='P') echo "<font color='magenta'>(PROFESOR)</font>";
    if (rank($r['user'])=='P') $clasa=get_materie_by_id($r['id_materie']);
    if (rank($r['user'])=='A') $clasa='administrator';
    echo"</a></b></td><td style='text-align:center;'>".$clasa."</td><td style='text-align:center;'>".$r['m_trimise']."</td><td style='text-align:center;'>".$r['f_incarcate']."</td><td style='text-align:center;'>".$r['email']."</td>";
    echo "</td><td style='text-align:center;'>";
    if (is_blocat($r['user'])) echo "<font color='red'>blocat</font>"; else if (!user_activ($r['user'])) echo "inactiv"; else echo "activ";
    echo "</td></tr>";
    }
?>
</tbody>
</table>
</div>
</center>
<?
}

else if ($token=="absolventi") //PAGINA CU LISTA UTILIZATORILOR
{
if (!logat())
    redirect("index.php?token=eroare&id=nepermis");
?>
<center>
<h1 class="page-header">Lista cu elevii absolventi</h1>
<?
$clasa_abs=query("SELECT id FROM clase WHERE clasa='ABS'");
if (query("SELECT * FROM utilizatori WHERE id_clasa=".$clasa_abs[0]))
{
?>
<div class="table-responsive">
<table class="table">
<thead>
<th style="text-align: center;">Nume si prenume</th>
<th style="text-align: center;">Nume de utilizator</th>
<th style="text-align: center;">Numar de mesaje trimise</th>
<th style="text-align: center;">Numar de fisiere incarcate</th>
<th style="text-align: center;">E-mail</th>
<th style="text-align: center;">Stare</th>
</thead>
<tbody>
<?
$q=@mysql_query("SELECT * FROM utilizatori WHERE id_clasa=".$clasa_abs[0]." ORDER BY user ASC");
while ($r=@mysql_fetch_array($q))
    {
    echo "<tr><td style='text-align:center;'>".$r['nume']." ".$r['prenume']."</td><td style='text-align:center;'><img src='".get_thumb($r['user'])."' style='border-radius:5px; vertical-align:middle;' width='30' height='30'/><b><a href='index.php?token=profil&id=".get_id_by_user($r['user'])."'>".$r['user']." ";
    if (rank($r['user'])=='A') echo "<font color='red'>(ADMIN)</font>";
    else if (rank($r['user'])=='P') echo "<font color='magenta'>(PROFESOR)</font>";
    echo"</a></b></td><td style='text-align:center;'>".$r['m_trimise']."</td><td style='text-align:center;'>".$r['f_incarcate']."</td><td style='text-align:center;'>".$r['email']."</td>";
    echo "</td><td style='text-align:center;'>";
    if (is_blocat($r['user'])) echo "<font color='red'>blocat</font>"; else if (!user_activ($r['user'])) echo "inactiv"; else echo "activ";
    echo "</td></tr>";
    }
?>
</tbody>
</table>
</div>
</center>
<?
}
else echo '<div class="alert alert-danger" role="alert">Nu exista niciun elev absolvent in baza de date.</div>';
}

else if ($token=="anunt") //PAGINA DE CREARE A UNUI ANUNT
{
if (!logat() || (rank($_SESSION['user'])!='P' && rank($_SESSION['user'])!='A'))
    redirect("index.php?token=eroare&id=nepermis");
if (!isset($_POST['anunt']) && !isset($_POST['clasa']))
{?>
<center>
<h1 class="page-header">Difuzare anunt</h1>
<form class="form-set" role="form" action="index.php?token=anunt" method="POST">
<label>Alegeti clasa: </label>
<select class="form-control" style="width:auto;" name="clasa">
<?
$rez=@mysql_query("SELECT * FROM clase ORDER BY clasa ASC");
while($row=@mysql_fetch_array($rez))
    if ($row['clasa']!='ALL')
        echo "<option>".$row['clasa']."</option>";
?>
</select>
<textarea class="form-control" style="resize: none;" name="anunt" rows="10" cols="70" maxlength="100000" placeholder="Introduceti anuntul (lasati gol pentru stergere)"></textarea>
<?read_error();
read_succes();?>
<button class="btn btn-primary btn-block" style="width: auto;" type="submit">Transmite</button>
</form>
<?
if (query("SELECT anunt FROM anunturi WHERE anunt<>''"))
{
echo '<h3>Anunturi curente</h3>';
if (!$detectare->isMobile() && !$detectare->isTablet()) $s='style="width: 70%;"';
$q=@mysql_query("SELECT * FROM anunturi");
while ($r=@mysql_fetch_array($q))
    {
    if ($r['anunt'])
    echo '<div class="panel panel-warning" '.$s.'>
    <div class="panel-heading">Pentru clasa <b>'.get_class_by_id($r['id_clasa']).'</b>
    </div>
    <div class="panel-body">'.$r['anunt'].'<br /><label>difuzat de <b><i>'.get_user_by_id($r['id_user']).'</i></b></label>
    </div>
    </div>
    ';
    }
}
?>
</center>
<?
}
else if (isset($_POST['anunt']) && isset($_POST['clasa']))
{
    $anunt=mres($_POST['anunt']);
    $clasa=mres($_POST['clasa']);
    $id_clasa=get_id_by_class($clasa);
    $user=$_SESSION['user'];
    $id_user=get_id_by_user($user);
    if ($anunt=='')
        {@mysql_query("UPDATE anunturi SET anunt='', id_user='' WHERE id_clasa=$id_clasa");
        opensession("succes", "Anuntul pentru utilizatorii din clasa ".$clasa." a fost sters!");
        }
    else 
        {
        @mysql_query("UPDATE anunturi SET anunt='$anunt', id_user=$id_user WHERE id_clasa=$id_clasa");
        opensession("succes", "Anuntul a fost transmis utilizatorilor din clasa ".$clasa.".");
        }
    redirect("index.php?token=anunt");
}
}

else if ($token=="ultimii") //PAGINA CU LISTA ULTIMILOR 20 DE UTILIZATORI INREGISTRATI
{
if (!logat())
    redirect("index.php?token=eroare&id=nepermis");
?>
<center>
<h4 class="page-header">Ultimii utilizatori inregistrati:</h4>
<div class="panel panel-default" <?if (!$detectare->isMobile() && !$detectare->isTablet()) {?>style="width: 40%;"<?}?>>
  <div class="panel-body">
          <?$users=@mysql_query("SELECT * FROM utilizatori WHERE activat=1 ORDER BY id DESC LIMIT 20");
          while($r=@mysql_fetch_array($users))
            {if (rank($r['user'])=='E') echo '<a style="text-decoration: none;" href="index.php?token=profil&id='.get_id_by_user($r['user']).'"><b>'.$r['user'].'</b></a> - '.get_class_by_id($r['id_clasa']);
            else echo '<a style="text-decoration: none;" href="index.php?token=profil&id='.get_id_by_user($r['user']).'"><b>'.$r['user']."</b></a>";
            echo "<br />";
            }
        ?>
  </div>
</div>
</center>
<?
}

else if ($token=="profil") //PAGINA DE ACCESARE A PROFILULUI ALTUI UTILIZATOR
{
if (!logat())
    redirect("index.php?token=eroare&id=nepermis");
if (!isset($_POST['user']) && !isset($_GET['id']))
{
?>
<center>
<h1 class="page-header">Vizualizare profil</h1>
<form class="form-set" role="form" action="index.php?token=profil" method="POST">
<label>Introduceti numele de utilizator al carui profil doriti sa il vizualizati:</label>
<input class="form-control" type="text" name="user" max="30" maxlength="30" required/>
<?
read_error();
?>
<button class="btn btn-primary btn-block" style="width: auto;" type="submit">Vizualizare</button>
</form>
</center>
<?
}
else if (isset($_POST['user']))
{
$user=mres($_POST['user']);
if ($user==$_SESSION['user'])
    redirect("index.php?token=admin");
if ($user=='')
    {
    opensession("error", "Introduceti un nume de utilizator!");
    redirect("index.php?token=profil");
    }
if (!get_id_by_user($user))
    {
    opensession("error", "Utilizatorul introdus nu exista in baza de date!");
    redirect("index.php?token=profil");    
    }
redirect("index.php?token=profil&id=".get_id_by_user($user));
}
if (isset($_GET['id']))
{
$id=mres($_GET['id']);
if (rank(get_user_by_id($id))!='E' && rank($_SESSION['user'])=='E')
    {
    opensession("error", "Nu aveti acces la acest profil!");
    redirect("index.php?token=profil");    
    }
if (!is_numeric($id))
    redirect("index.php?token=eroare&id=nepermis");
$user=get_user_by_id($id);
if ($user=='')
    {
    opensession("error", "Introduceti un nume de utilizator!");
    redirect("index.php?token=profil");
    }
if ($user==$_SESSION['user'])
    redirect("index.php?token=admin");
if (!($detectare->isMobile() && !$detectare->isTablet())) {
?>
<h1 class="page-header">Profilul utilizatorului <b><i><?=$user;?></i></b> <?if (is_blocat($user)) echo '&nbsp;- CONT BLOCAT'?>
<a href="index.php?token=mesaj_rapid&id=<?=$id;?>"><span class="glyphicon glyphicon-envelope" style="float: right;"></span></a>
<?
if (!prieten(get_id_by_user($user))) echo '<a href="index.php?token=adauga_favorit&id='.$id.'"><span class="glyphicon glyphicon-star-empty" style="float: right;"></span></a>';
else echo '<a onclick="return confirm(\'Vrei sa stergi acest utilizator din lista de favoriti?\');" href="index.php?token=sterge_favorit&id='.$id.'"><span class="glyphicon glyphicon-star" style="float: right;"></span></a>';
?>
</h1>
<div style="float: right; width: 40%; height: auto;">
<?if (thumb_exist($user))
{?>
<img src="<?=get_thumb($user);?>" style="border-radius: 10px; float: right;" width='200' height='200'/>
<?}
else {
?><img src="imagini/profil.jpg" style="border-radius: 10px; float: right;" width='200' height='200'/>
<?}?>
<br style="clear: both;" />
</div>
<div style="float: left; width: 60%; height: auto;">
<h3>Nume: <?=get_first_name_by_user($user);?></h3>
<h3>Prenume: <?=get_surname_by_user($user);?></h3>
<h3>E-mail: <?=get_email_by_user($user);?></h3>
<?if (rank($user)=='E') {
      if (get_clasa_by_user($user)!='ABS') {?><h3>Clasa: <?=get_clasa_by_user($user);
      }
      else echo "<h3>ELEV ABSOLVENT</h3>";?></h3><?}?>
<h3>Rang: <?if (rank($user)=='E') echo "ELEV"; else if (rank($user)=='P') echo "PROFESOR"; else echo "ADMINISTRATOR";?></h3>
<h3>Numar de mesaje trimise: <?=get_nr_trimise($user);?>
</h3>
<h3>Numar de fisiere incarcate: <?=get_nr_fisiere($user);?></h3>
<!--<h3>Cont blocat: <?if (is_blocat($user)) echo "DA"; else echo "NU";?></h3>-->
<h3>Ultima logare: <?
$l=query("SELECT last_login FROM utilizatori WHERE user='".get_user_by_id($id)."'");
if ($l[0]!='0000-00-00 00:00:00') echo $l[0];
else echo "niciodata";?></h3>
</div>
<?
}
else {
?>
<center>
<h1 class="page-header">Profilul utilizatorului <b><i><?=$user;?></i></b> <?if (is_blocat($user)) echo '&nbsp;- CONT BLOCAT'?>
</h1>
<?if (thumb_exist($user))
{?>
<img src="<?=get_thumb($user);?>" style="border-radius: 10px;" width='200' height='200'/>
<?}
else {
?><img src="imagini/profil.jpg" style="border-radius: 10px;" width='200' height='200'/>
<?}?>
<br />
<h1>
<a href="index.php?token=mesaj_rapid&id=<?=$id;?>"><span class="glyphicon glyphicon-envelope"></span></a>
<?
if (!prieten(get_id_by_user($user))) echo '<a href="index.php?token=adauga_favorit&id='.$id.'"><span class="glyphicon glyphicon-star-empty"></span></a>';
else echo '<a onclick="return confirm(\'Vrei sa stergi acest utilizator din lista de favoriti?\');" href="index.php?token=sterge_favorit&id='.$id.'"><span class="glyphicon glyphicon-star"></span></a>';
?>
</h1>
</center>
<h4>Nume: <?=get_first_name_by_user($user);?></h4>
<h4>Prenume: <?=get_surname_by_user($user);?></h4>
<h4>E-mail: <?=get_email_by_user($user);?></h4>
<?if (rank($user)=='E') {?><h4>Clasa: <?=get_clasa_by_user($user);?></h4><?}?>
<h4>Rang: <?if (rank($user)=='E') echo "ELEV"; else if (rank($user)=='P') echo "PROFESOR"; else echo "ADMINISTRATOR";?></h4>
<h4>Numar de mesaje trimise: <?=get_nr_trimise($user);?>
</h4>
<h4>Numar de fisiere incarcate: <?=get_nr_fisiere($user);?></h4>
<!--<h3>Cont blocat: <?if (is_blocat($user)) echo "DA"; else echo "NU";?></h3>-->
<h4>Ultima logare: <?
$l=query("SELECT last_login FROM utilizatori WHERE user='".get_user_by_id($id)."'");
if ($l[0]!='0000-00-00 00:00:00') echo $l[0];
else echo "niciodata";?></h4>
<?
}
}
}

else if ($token=="mesaj_rapid") //PAGINA DE PROCESARE A UNEI CERERI DE MESAJ RAPID CATRE UN ANUMIT USER (FACE REDIRECT CATRE FORMA DE E-MAIL, COMPLETAND AUTOMAT CAMPUL DESTINATAR)
{
if (!logat() || $_SERVER['REQUEST_METHOD']!='GET' || (!isset($_GET['id'])))
    redirect("index.php?token=eroare&id=nepermis");
$id=mres($_GET['id']);
if (!exist_user(get_user_by_id($id)))
    {
    opensession("error", "Utilizatorul nu exista in baza de date!");
    redirect("index.php?token=eroare");
    }
opensession("dest", $id);
redirect("index.php?token=email");
}

else if ($token=="clase") //PAGINA DE GESTIUNE A CLASELOR
{
if (!logat() || rank($_SESSION['user'])=='E')
    redirect("index.php?token=eroare&id=nepermis");
$c=query("SELECT COUNT(*) FROM clase");
?>
<center>
<h1 class="page-header">Gestiune clase</h1>
</center>
<?
read_succes();
?>
<div style="float: right; width: 40%; height: 100%;">
<center>
<form class="form-set" role="form" action="index.php?token=adauga_clasa" method="POST">
<h3>Adaugare clasa noua</h3>
<input class="form-control" style="width: auto; min-width: 120px;" type="text" name="clasa" size="3" maxlength="3" max="3" placeholder="Nume clasa" required/>
<input class="form-control" style="width: auto; min-width: 120px;" type="text" name="nr_elevi" size="2" maxlength="2" max="2" placeholder="Numar elevi" required/>
<select class="form-control" style="width: auto;" name="profil">
      <option>Matematica-Informatica</option>
      <option>Chimie-Biologie</option>
      <option>Filologie</option>
      <option>Stiinte sociale</option>
</select>
<?
if ($_SESSION['errora'])
    {
    echo '<div class="alert alert-danger" role="alert">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <h4>Eroare!</h4>
    '.$_SESSION['errora'].'</div>';
    $_SESSION['errora']='';
    }
?>
<button class="btn btn-primary btn-block" style="width: auto;" type="submit">Adaugare</button>
</form>
</center>
<?
if ($c[0]!=0)
{
?>
<br />
<center>
<form class="form-set" role="form" action="index.php?token=editeaza_clasa" method="POST">
<h3>Modificare clasa existenta</h3>
<label>Alegeti clasa: </label>
<select class="form-control" style="width: auto;" name="clasa">
<?
$rez=@mysql_query("SELECT * FROM clase ORDER BY clasa ASC");
while($row=@mysql_fetch_array($rez))
    if ($row['clasa']!='ALL')
        echo "<option>".$row['clasa']."</option>";
?>
</select>
<input class="form-control" style="width: auto; min-width: 120px;" type="text" name="nr_elevi" size="2" maxlength="2" max="2" placeholder="Numar elevi" required/>
<select class="form-control" style="width: auto;" name="profil">
      <option>Matematica-Informatica</option>
      <option>Chimie-Biologie</option>
      <option>Filologie</option>
      <option>Stiinte sociale</option>
</select>
<?
if ($_SESSION['errorm'])
    {
    echo '<div class="alert alert-danger" role="alert">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <h4>Eroare!</h4>
    '.$_SESSION['errorm'].'</div>';
    $_SESSION['errorm']='';
    }
?>
<button class="btn btn-primary btn-block" style="width: auto;" type="submit">Modificare</button>
</form>
</center>
</div>
<?
}
if ($c[0]==0)
    echo '<div class="alert alert-danger" role="alert">Nu aveti nicio clasa adaugata.</div>';
else 
{
?>
<table class="table" style="width: 30%;">
<thead>
<th style="text-align: center;">Clasa</th>
<th style="text-align: center;">Numar de elevi</th>
<?if (!($detectare->isMobile() && !$detectare->isTablet())) {?><th style="text-align: center;">Profil</th><?}?>
</thead>
<tbody>
<?
$query="SELECT * FROM clase ORDER BY clasa ASC";
$rez=@mysql_query($query);
while ($row=@mysql_fetch_array($rez))
    if ($row['clasa']!='ALL' && $row['clasa']!='ABS')
        {echo "<tr><td style='text-align:center;'>".$row['clasa']."</td><td style='text-align:center;'>".$row['nr_elevi']."</td>";
        if (!($detectare->isMobile() && !$detectare->isTablet())) echo "<td style='text-align: center;'>".$row['profil']."</td>";
        echo "<td><a href='index.php?token=sterge_clasa&id=".$row['id']."' onclick=\"return confirm('Sigur doriti sa stergeti aceasta clasa? ATENTIE: Operatia este ireversibila: toate conturile, respectiv fisierele, postarile asociate acestei clase sunt sterse definitiv!');\">";
        echo "<span class='glyphicon glyphicon-remove'></span></a></td></tr>";
        }
?>
</tbody>
</table>
<?
}
}

else if ($token=="adauga_clasa") //PAGINA DE PROCESARE A CERERII DE ADAUGARE A UNEI NOI CLASE
{
if ($_SERVER['REQUEST_METHOD']!='POST' || !logat() || (rank($_SESSION['user'])!='P' && rank($_SESSION['user'])!='A'))
    redirect("index.php?token=eroare&id=nepermis");
$clasa=mres($_POST['clasa']);
$nr_elevi=mres($_POST['nr_elevi']);
$profil=mres($_POST['profil']);
if ($clasa=='' || $nr_elevi=='' || $profil=='')
    {
    opensession("errora", "Completati toate campurile!");
    redirect("index.php?token=clase");
    }
if (!preg_match("/^([0-9]){1,2}([A-J])$/", $clasa)) //verificam corectitudinea numelui clasei; tip: 1-2 cifre + o majuscula
    {
    opensession("errora", "Numele clasei este invalid!");
    redirect("index.php?token=clase");  
    }
if (!is_numeric($nr_elevi)) //verificam daca numarul de elevi este de tip numeric
    {
    opensession("errora", "Completati numarul de elevi cu o valoare numerica!");
    redirect("index.php?token=clase");
    }
if ($nr_elevi<1 || $nr_elevi>50) //plauzibil ar fi ca o clasa sa aiba intre 1 (absurd) si 50 de elevi
    {
    opensession("errora", "Numarul de elevi trebuie sa fie cuprins intre 1 si 50!");
    redirect("index.php?token=clase");  
    }
if (query("SELECT clasa FROM clase WHERE clasa='$clasa'")) //verificam existenta clasei citite
    {
    opensession("errora", "Clasa respectiva exista deja!");
    redirect("index.php?token=clase");
    }
@mysql_query("INSERT INTO clase (clasa, nr_elevi, profil) VALUES ('$clasa', $nr_elevi, '$profil')"); //inseram clasa in baza de date
@mysql_query("INSERT INTO anunturi (id_clasa, anunt) VALUES (".get_id_by_class($clasa).", '')"); //inseram clasa in tabela cu anunturi
opensession("succes", "Clasa a fost adaugata cu succes!");
redirect("index.php?token=clase");
}

else if ($token=="editeaza_clasa") //PAGINA DE PROCESARE A CERERII DE EDITARE A UNEI CLASE
{
if ($_SERVER['REQUEST_METHOD']!='POST' || !logat() || (rank($_SESSION['user'])!='P' && rank($_SESSION['user'])!='A'))
    redirect("index.php?token=eroare&id=nepermis");
$clasa=mres($_POST['clasa']);
$nr_elevi=mres($_POST['nr_elevi']);
$profil=mres($_POST['profil']);
if ($clasa=='' || $nr_elevi=='')
    {
    opensession("errorm", "Completati toate campurile!");
    redirect("index.php?token=clase");
    }
if (!is_numeric($nr_elevi))
    {
    opensession("errorm", "Completati numarul de elevi cu o valoare numerica!");
    redirect("index.php?token=clase");
    }
if ($nr_elevi<1 || $nr_elevi>50)
    {
    opensession("errorm", "Numarul de elevi trebuie sa fie cuprins intre 1 si 50!");
    redirect("index.php?token=clase");
    }
$q=query("SELECT COUNT(*) FROM utilizatori WHERE id_clasa=".get_id_by_class($clasa));
if ($nr_elevi<$q[0])
    {
    opensession("errorm", "Introduceti un numar de elevi mai mare sau egal decat ".$q[0]."!");
    redirect("index.php?token=clase");
    }
@mysql_query("UPDATE clase SET nr_elevi=$nr_elevi, profil='$profil' WHERE clasa='$clasa'");
opensession("succes", "Clasa a fost modficata cu succes!");
redirect("index.php?token=clase");
}

else if ($token=="sterge_clasa") //PAGINA DE PROCESARE A CERERII DE STERGERE A UNEI CLASE
{
if (!logat() || $_SERVER['REQUEST_METHOD']!='GET' || rank($_SESSION['user'])=='E' || !is_numeric($_GET['id']) || !isset($_GET['id']))
    redirect("index.php?token=eroare&id=nepermis");
$id=mres($_GET['id']);
if (!query("SELECT * FROM clase WHERE id=$id"))
    redirect("index.php?token=eroare&id=nepermis");
$row=@query("SELECT * FROM clase WHERE id=$id");
      @mysql_query("UPDATE anunturi SET anunt='', id_user=0 WHERE id_clasa=".$row['id']); //stergem anunturile
      $f=mysql_query("SELECT * FROM fisiere WHERE id_clasa=".$row['id']);
      while ($fisier=mysql_fetch_array($f)) //stergem fisierele
            {
            unlink($fisier['cale']);
            @mysql_query("DELETE FROM fisiere WHERE id=".$fisier['id']);
            }
      $postari=mysql_query("SELECT * FROM postari WHERE id_clasa=".$row['id']);
      while ($postare=mysql_fetch_array($postari)) //stergem postarile si imaginile (daca exista) asociate
            unlink($postare['imagine']);
      @mysql_query("DELETE FROM postari WHERE id_clasa=".$row['id']);
      $u=@mysql_query("SELECT id FROM utilizatori WHERE id_clasa=".$row['id']);
      while ($user=@mysql_fetch_array($u)) //stergem tot ce le corespunde utilizatorilor din aceasta clasa
            {
            $id_user=$user[0];
            $mp=@mysql_query("SELECT nume_att FROM mesaje WHERE id_user=$id_user"); //stergem atasamentele de la mesaje
            while ($mesaj_primit=@mysql_fetch_array($mp))
                  unlink("uploads/".$mesaj_primit[0]);
            $mt=@mysql_query("SELECT nume_att FROM trimise WHERE id_user=$id_user"); //stergem atasamentele de la mesaje
            while ($mesaj_trimis=@mysql_fetch_array($mp))
                  unlink("uploads/".$mesaj_trimis[0]);
            @mysql_query("DELETE FROM mesaje WHERE id_user=$id_user"); //stergem mesajele primite
            @mysql_query("DELETE FROM trimise WHERE id_user=$id_user"); //stergem mesajele trimise
            $f2=@mysql_query("SELECT cale FROM fisiere WHERE id_user=$id_user"); //stergem fisierele incarcate
            while ($fisier2=@mysql_fetch_array($f2))
                  unlink($r[0]);
            @mysql_query("DELETE FROM fisiere WHERE id_user=$id_user");
            $f3=@mysql_query("SELECT cale FROM fisiere_categorii WHERE id_user=$id_user"); //stergem fisierele incarcate pe materii
            while ($fisier3=@mysql_fetch_array($f3))
                  unlink($r[0]);
            @mysql_query("DELETE FROM fisiere_categorii WHERE id_user=$id_user");
            @mysql_query("DELETE FROM comentarii WHERE id_user=$id_user"); //stergem comentariile
            }
      @mysql_query("DELETE FROM clase WHERE id=".$row['id']); //stergem clasa respectiva din lista
opensession('succes', 'Clasa a fost stearsa din baza de date!');
redirect("index.php?token=clase");
}

else if ($token=="calculator") //CALCULATORUL DE MEDII
{
if (!logat())
    redirect("index.php?token=eroare&id=nepermis");
echo "<center>";
if ($_SESSION['media'])
{
?>
<h1 class="page-header">Calculator medii</h1>
<h2>REZULTAT: <?echo $_SESSION['media'];
$_SESSION['media']='';
?></h2>
<a href="index.php?token=calculator"><button class="btn btn-primary btn-block" style="width: auto;">Calculati alta medie</button></a>
<?
}
else if (!$_POST && !$_SESSION['media'])
{
?>
<h1 class="page-header">Calculator medii</h1>
<form class="form-set" action="index.php?token=calculator" method="POST">
<input class="form-control" style="width: auto;" type="text" name="nr_note" max="2" maxlength="2" placeholder="Numarul de note" required/>
<select class="form-control" style="width: auto;" name="teza">
<option>Cu teza</option>
<option>Fara teza</option>
</select>
<?
read_error();
?>
<button class="btn btn-primary btn-block" style="width: auto;" type="submit">Continuare</button>
</form>
<?
}
else if (isset($_POST['nr_note']) && isset($_POST['teza']))
{
    if ($_POST['nr_note']=='' || !is_numeric($_POST['nr_note']))
        {
        opensession("error", "Introduceti un numar valid.");
        redirect("index.php?token=calculator");
        }
$nr=mres($_POST['nr_note']);
if ($nr>20 || $nr<2)
{
 opensession("error", "Introduceti un numar de note cuprins intre 2 si 20.");
 redirect("index.php?token=calculator");  
}
$teza=$_POST['teza'];
?>
<h1 class="page-header">Calculator medii</h1>
<form class="form-set" action="index.php?token=calculator" method="POST">
<label>Introduceti notele</label>
<?
for ($i=1; $i<=$nr; $i++)
    echo "<input class='form-control' type='text' name='nota".$i."' style='width:auto;' max='2' maxlength='2' required placeholder='Nota ".$i."'/>";
?>
<?if ($teza=='Cu teza')
{
?>
<input class="form-control" style="width: auto;" type="text" name="nota_teza" max="2" maxlength="2" placeholder="Nota obtinuta la teza" required/>
<?
}
?>
<input type="hidden" name="nr" value="<?=$nr;?>"/>
<button class="btn btn-primary btn-block" style="width: auto;" type="submit">Calculare</button>
</form>
<?
}
else if (isset($_POST['nota1']))
{
$nr=mres($_POST['nr']);
if (isset($_POST['nota_teza']) && ($_POST['nota_teza']=='' || !is_numeric($_POST['nota_teza']) || $_POST['nota_teza']<1 || $_POST['nota_teza']>10))
    {
    opensession("error", "Reluati procedeul de calcul si introduceti note de tip numeric, cuprinse intre 1 si 10.");
    redirect("index.php?token=calculator");
    }
for ($i=1; $i<=$nr; $i++)
    if ($_POST['nota'.$i]=='' || !is_numeric($_POST['nota'.$i]) || $_POST['nota'.$i]<1 || $_POST['nota'.$i]>10)
        {
        opensession("error", "Reluati procedeul de calcul si introduceti note de tip numeric, cuprinse intre 1 si 10.");
        redirect("index.php?token=calculator");
        }
$teza=mres($_POST['nota_teza']);
$suma=0;
for ($i=1; $i<=$nr; $i++)
    $suma+=mres($_POST['nota'.$i]);
if ($teza)
    $media=(($suma/$nr)*3+$teza)/4;
else $media=$suma/$nr;
opensession("media", round($media, 2));
redirect("index.php?token=calculator");
}
echo "</center>";
}

else if ($token=="promovare") //PAGINA DE PROMOVARE A RANGULUI UNUI UTILIZATOR
{
if (!logat() || rank($_SESSION['user'])=='E')
    redirect("index.php?token=eroare&id=nepermis");
if (!isset($_POST['user']))
{
?>
<center>
<h1 class="page-header">Promovare</h1>
<form class="form-set" role="form" action="index.php?token=promovare" method="POST">
<label>Introduceti numele de utilizator <?=$site_title_short;?> pe care doriti sa-l promovati la rangul de PROFESOR:</label>
<input class="form-control" type="text" name="user" max="30" maxlength="30" required/>
<input type="hidden" name="rank" value="prof"/>
<?
if ($_SESSION['errorp'])
    {
    echo '<div class="alert alert-danger" role="alert">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <h4>Eroare!</h4>
    '.$_SESSION['errorp'].'</div>';
    $_SESSION['errorp']='';
    }
?>
<button class="btn btn-primary btn-block" style="width: auto;" type="submit">Promovare</button>
</form>
</center>
<?
if (rank($_SESSION['user'])=='A')
{
?>
<br />
<center>
<form class="form-set" role="form" action="index.php?token=promovare" method="POST">
<label>Introduceti numele de utilizator <?=$site_title_short;?> pe care doriti sa-l promovati la rangul de ADMINISTRATOR:</label>
<input class="form-control" type="text" name="user" max="30" maxlength="30" required/>
<input type="hidden" name="rank" value="admin"/>
<?
if ($_SESSION['errora'])
    {
    echo '<div class="alert alert-danger" role="alert">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <h4>Eroare!</h4>
    '.$_SESSION['errora'].'</div>';
    $_SESSION['errora']='';
    }
?>
<button class="btn btn-primary btn-block" style="width: auto;" type="submit">Promovare</button>
</form>
</center>
<?
}
}
else
{
$user=mres($_POST['user']);
$r=mres($_POST['rank']);
if ($r=='prof') $c='p'; else $c='a';
if ($user=='')
    {
    opensession("error".$c, "Introduceti un nume de utilizator!");
    redirect("index.php?token=promovare");  
    }
$rez=@query("SELECT rank FROM utilizatori WHERE user='$user'");
if (!$rez)
    {
    opensession("error".$c, "Numele de utilizator introdus nu exista in baza de date!");
    redirect("index.php?token=promovare");  
    }
$rank=$rez[0];
if ($rank=='P' && $r=='prof')
    {
    opensession("error".$c, "Utilizatorul introdus are deja rangul PROFESOR!");
    redirect("index.php?token=promovare");
    }
if ($rank=='E' && $r=='admin')
    {
    opensession("error".$c, "Un elev nu poate fi promovat ca ADMINISTRATOR!");
    redirect("index.php?token=promovare"); 
    }
if ($rank=='A')
    {
    opensession("error".$c, "Utilizatorul introdus are deja rangul ADMINISTRATOR!");
    redirect("index.php?token=promovare");
    }
if ($r=='prof') @mysql_query("UPDATE utilizatori SET rank='P', id_clasa='' WHERE user='$user'");
else if ($r=='admin') @mysql_query("UPDATE utilizatori SET rank='A' WHERE user='$user'");
opensession("succes", "Utilizatorul <b><i>".$user."</i></b> a fost promovat!<br/>Daca ati promovat pe cineva din greseala, contactati administratorul la adresa astronos2007@yahoo.com!");
redirect("index.php?token=succes");
}
}

else if ($token=="transfer") //PAGINA PENTRU TRANSFERAREA UNUI ELEV
{
if (rank($_SESSION['user'])=='E' || !logat())
    redirect("index.php?token=eroare&id=nepermis");
if (!isset($_POST['user']) && !isset($_POST['clasa']))
{
?>
<center>
<h1 class="page-header">Transfer elev</h1>
<form class="form-set" role="form" action="index.php?token=transfer" method="POST">
<label>Introduceti numele de utilizator al elevului ce va fi transferat:</label>
<input class="form-control" type="text" name="user" max="30" maxlength="30" required/>
<?
read_error();
?>
<button class="btn btn-primary btn-block" style="width: auto;" type="submit">Mai departe</button>
</form>
<?
}
else if (isset($_POST['user']))
{
$user=mres($_POST['user']);
if (!$user)
    {
    opensession("error", "Introduceti un nume de utilizator!");
    redirect("index.php?token=transfer");
    }
if (!exist_user($user))
    {
    opensession("error", "Numele de utilizator introdus nu exista in baza de date!");
    redirect("index.php?token=transfer");
    }
if (rank($user)!='E')
    {
    opensession("error", "Doar elevii pot fi transferati!");
    redirect("index.php?token=transfer");
    }
if (get_clasa_by_user($user)=='ABS')
    {
    opensession("error", "Nu puteti transfera elevi absolventi!");
    redirect("index.php?token=transfer");
    }
opensession("transf", $user);
?>
<center>
<h1 class="page-header">Transfer elev</h1>
<form class="form-set" role="form" action="index.php?token=transfer" method="POST">
<h3>Transferati elevul <b><i><?=$user;?></i></b>, din clasa <?=get_clasa_by_user($user);?></h3>
<label>Alegeti clasa in care sa fie transferat:</label>
<select class="form-control" style="width: auto;" name="clasa">
<?
$rez=@mysql_query("SELECT * FROM clase ORDER BY clasa ASC");
while($row=@mysql_fetch_array($rez))
    if ($row['clasa']!='ALL')
        echo "<option>".$row['clasa']."</option>";
?>
</select>
<?
read_error();
?>
<button class="btn btn-primary btn-block" style="width: auto;" type="submit">Transferare</button>
</form>
<?
}
else if (isset($_POST['clasa']))
{
$clasa=mres($_POST['clasa']);
$id_clasa=get_id_by_class($clasa);
$res=@query("SELECT COUNT(*) FROM utilizatori WHERE id_clasa=$id_clasa");
$res2=@query("SELECT nr_elevi FROM clase WHERE id=$id_clasa");
if ($clasa==get_clasa_by_user($_SESSION['transf']))
    {
    opensession("error", "Utilizatorul este deja in aceasta clasa!");
    redirect("index.php?token=transfer");
    }
if ($res[0]>=$res2[0])
    {
    opensession('error', 'Numarul de elevi pentru aceasta clasa a fost depasit! Contactati administratorul '.$site_title_short.'!');
    redirect("index.php?token=transfer");
    }
@mysql_query("UPDATE clase SET nr_elevi=nr_elevi-1 WHERE clasa='".get_clasa_by_user($_SESSION['transf'])."'");
@mysql_query("UPDATE clase SET nr_elevi=nr_elevi+1 WHERE clasa='".$clasa."'");
@mysql_query("UPDATE utilizatori SET id_clasa=$id_clasa WHERE user='".$_SESSION['transf']."'");
opensession("succes", "Utilizatorul <b><i>".$_SESSION['transf']."</i></b> a fost transferat in clasa ".$clasa."!");
$_SESSION['transf']='';
redirect("index.php?token=succes");
}
else redirect("index.php?token=eroare&id=nepermis");
}

else if ($token=="avansare") //PAGINA CARE AVANSEAZA CLASELE LA SFARSITUL ANULUI SCOLAR
{
if (!logat() || rank($_SESSION['user'])!='A')
    redirect("index.php?token=eroare&id=nepermis");
if (!isset($_POST['check']) && $_SERVER['REQUEST_METHOD']!='POST')
{
?>
<center>
<h1 class="page-header">Avansare clase</h1>
<div class="alert alert-info" role="alert">
Aceasta functie va fi folosita la sfarsitul anului scolar, cand elevii vor trece intr-o clasa superioara, iar datele vor trebui actualizate in conformitate cu aceasta.
</div>
<div class="alert alert-info" role="alert">In cazul repetentilor, acestia vor trebui transferati manual ulterior.</div>
<div class="alert alert-danger" role="alert">ATENTIE: OPERATIE IREVERSIBILA! Odata cu apasarea butonului, clasele a 9-a vor deveni a 10-a, clasele a 10-a vor deveni a 11-a, clasele a 11-a vor deveni a 12-a, iar elevii din clasele a 12-a vor fi introdusi la categoria "Absolventi" si isi vor pastra drepturile actuale, putand efectua aceleasi operatiuni pe site, dar tuturor claselor de a 12-a li se vor STERGE postarile, mesajele, anunturile, fisierele incarcate! Procesul este ireversibil: in cazul in care executati operatia din greseala, datele NU vor putea fi recuperate!</div>
<form role="form" action="index.php?token=avansare" method="POST">
<div class="checkbox">
          <label>
            <input type="checkbox" name="check"/> Am citit avertismentul de mai sus si inteleg consecintele functiei de avansare a claselor.
          </label>
        </div>
<?read_error();?>
<button class="btn btn-primary btn-block" style="width: auto;" type="submit" onclick="return confirm('Sigur doriti sa avansati clasele? ATENTIE: OPERATIE IREVERSIBILA!!!');">Avansare</button>
</form>
</center>
<?
}
else
{
$check=mres($_POST['check']);
if (!$check)
    {
    opensession("error", "Bifati optiunea de securitate!");
    redirect("index.php?token=avansare");
    }
$q=@mysql_query("SELECT * FROM clase WHERE clasa LIKE '12%'");
while ($row=@mysql_fetch_array($q)) //pentru fiecare clasa de a 12-a in parte
    {
      @mysql_query("UPDATE anunturi SET anunt='', id_user=0 WHERE id_clasa=".$row['id']); //stergem anunturile
      $f=mysql_query("SELECT * FROM fisiere WHERE id_clasa=".$row['id']);
      while ($fisier=mysql_fetch_array($f)) //stergem fisierele
            {
            unlink($fisier['cale']);
            @mysql_query("DELETE FROM fisiere WHERE id=".$fisier['id']);
            }
      $postari=mysql_query("SELECT * FROM postari WHERE id_clasa=".$row['id']);
      while ($postare=mysql_fetch_array($postari)) //stergem postarile si imaginile (daca exista) asociate
            unlink($postare['imagine']);
      @mysql_query("DELETE FROM postari WHERE id_clasa=".$row['id']);
      $u=@mysql_query("SELECT id FROM utilizatori WHERE id_clasa=".$row['id']);
      while ($user=@mysql_fetch_array($u)) //stergem tot ce le corespunde utilizatorilor din clasele a 12-a si ii categorisim la tipul "absolventi"
            {
            $id_user=$user[0];
            $mp=@mysql_query("SELECT nume_att FROM mesaje WHERE id_user=$id_user"); //stergem atasamentele de la mesaje
            while ($mesaj_primit=@mysql_fetch_array($mp))
                  unlink("uploads/".$mesaj_primit[0]);
            $mt=@mysql_query("SELECT nume_att FROM trimise WHERE id_user=$id_user"); //stergem atasamentele de la mesaje
            while ($mesaj_trimis=@mysql_fetch_array($mp))
                  unlink("uploads/".$mesaj_trimis[0]);
            @mysql_query("DELETE FROM mesaje WHERE id_user=$id_user"); //stergem mesajele primite
            @mysql_query("DELETE FROM trimise WHERE id_user=$id_user"); //stergem mesajele trimise
            $f2=@mysql_query("SELECT cale FROM fisiere WHERE id_user=$id_user"); //stergem fisierele incarcate
            while ($fisier2=@mysql_fetch_array($f2))
                  unlink($r[0]);
            @mysql_query("DELETE FROM fisiere WHERE id_user=$id_user");
            $f3=@mysql_query("SELECT cale FROM fisiere_categorii WHERE id_user=$id_user"); //stergem fisierele incarcate pe materii
            while ($fisier3=@mysql_fetch_array($f3))
                  unlink($r[0]);
            @mysql_query("DELETE FROM fisiere_categorii WHERE id_user=$id_user");
            @mysql_query("DELETE FROM comentarii WHERE id_user=$id_user"); //stergem comentariile
            $abs=query("SELECT id FROM clase WHERE clasa='ABS'");
            @mysql_query("UPDATE utilizatori SET id_clasa=".$abs[0]." WHERE id=$id_user"); //actualizam clasa elevilor de a 12-a, ca si "absolventi"
            }
      @mysql_query("DELETE FROM clase WHERE id=".$row['id']); //stergem clasa respectiva de a 12-a din lista
    }
$q=@mysql_query("SELECT * FROM clase WHERE clasa<>'ALL' AND clasa<>'ABS'");
while ($row=@mysql_fetch_array($q)) //actualizam numele claselor existente, conform trecerii la anul superior scolar
      {
      $clasa_i=substr($row['clasa'], 0, -1); //extragem numarul clasei
      $litera_clasa=substr($row['clasa'], -1); //extragem litera clasei
      $clasa_f=$clasa_i+1; //avansam clasa si ii atribuim si litera
      $clasa_f=$clasa_f.$litera_clasa;
      @mysql_query("UPDATE clase SET clasa='$clasa_f' WHERE id=".$row['id']);
      }
@mysql_query("INSERT INTO clase (clasa, nr_elevi) VALUES ('9A', 30)");
@mysql_query("INSERT INTO clase (clasa, nr_elevi) VALUES ('9B', 30)");
@mysql_query("INSERT INTO clase (clasa, nr_elevi) VALUES ('9C', 30)");
@mysql_query("INSERT INTO clase (clasa, nr_elevi) VALUES ('9D', 30)");
@mysql_query("INSERT INTO clase (clasa, nr_elevi) VALUES ('9E', 30)");
@mysql_query("INSERT INTO clase (clasa, nr_elevi) VALUES ('9F', 30)");
@mysql_query("INSERT INTO clase (clasa, nr_elevi) VALUES ('9G', 30)");
@mysql_query("INSERT INTO clase (clasa, nr_elevi) VALUES ('9H', 30)");
@mysql_query("INSERT INTO clase (clasa, nr_elevi) VALUES ('9I', 30)");
@mysql_query("INSERT INTO clase (clasa, nr_elevi) VALUES ('9J', 30)");
opensession("succes", "Clasele au fost avansate cu succes! Au fost create noi clase de a 9-a, fiecare avand 30 de elevi in mod implicit. Actualizati aceste date in functie de generatia actuala. Elevii de a 12-a au fost introdusi la clasa \"ABSOLVENTI\". Se pot efectua aceleasi operatiuni pentru aceasta clasa ca si pentru celelalte.");
redirect("index.php?token=succes");
}
}

else if ($token=="radio") //PAGINA CU POSTURI RADIO
{
if (($detectare->isMobile() && !$detectare->isTablet()))
    {echo "<script type='text/javascript'>alert('Pagina cu posturile de radio nu functioneaza pe mobil!');</script>";
    //redirect("");
    }
if(!logat())
    redirect("index.php?token=eroare&id=nepermis");
?>
<center>
<div style="width: auto; height: 30%; display: block;">
<h2>Radio ZU: </h2><script>var zu_autoplay=false;</script><script src="http://live.radiozu.ro/embed.php"></script>
</div>
<br />
<div style="width: auto; height: 30%; display: block;">
<h2>Kiss FM: </h2>
<script type="text/javascript" src="http://www.kissfm.ro/js/swfobject.js"></script><script type="text/javascript" src="http://www.kissfm.ro/js/kissfm_widget_embed_live_small.js"></script><script type="text/javascript">show_player();</script>
</div>
<br />
<div style="width:  auto; height: 30%; display: block;">
<h2>Radio 21:</h2>
<object width="114" height="60" id="shoutcast" data="http://www.radio21.ro/wp-content/players/live/player4.swf" type="application/x-shockwave-flash">
                        <param value="sameDomain" name="allowScriptAccess">
                        <param value="bufferSeconds=5&amp;autoStart=0&amp;Server1=http://193.33.154.31:8000" name="FlashVars">
                        <param value="http://www.radio21.ro/site/flash/player3.swf" name="movie">
                        <param value="high" name="quality">
                        <param value="transparent" name="wmode">
                        <param name="autoplay" value="false">
                        <embed autoplay="false" wmode="transparent" width="114" height="60" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" allowscriptaccess="sameDomain" quality="high" src="http://www.radio21.ro/wp-content/players/live/player4.swf" flashvars="bufferSeconds=5&amp;autoStart=1&amp;Server1=http://193.33.154.31:8000">
</object>
</div>
</center>
<?
}

else if ($token=="favoriti") //PAGINA CU LISTA DE FAVORITI
{
if (!logat())
    redirect("index.php?token=eroare&id=nepermis");
?>
<center>
<h1 class="page-header">Lista mea de favoriti</h1><br />
<?
read_error();
read_succes();
if (friends_exist())
{
?>
<div class="table-responsive">
<table class="table" style="width: auto;">
<thead>
<th style="text-align: center;">Nume de utilizator</th>
<th style="text-align: center;">Nume si prenume</th>
<th style="text-align: center;">Clasa / Materia predata</th>
</thead>
<tbody>
<?
$q=query("SELECT prieteni FROM utilizatori WHERE user='".$_SESSION['user']."'");
$prieteni=explode("/", $q[0]);
foreach ($prieteni as $prieten)
    if (exist_user(get_user_by_id($prieten)))
    {echo "<tr></tr><td style='text-align:center;'>
    <img src='".get_thumb(get_user_by_id($prieten))."' style='border-radius:5px; vertical-align:middle;' width='30' height='30'/>
    <b>
    <span class='logo_colour'>
    <a href='index.php?token=profil&id=".$prieten."'>".get_user_by_id($prieten)."</a>
    </span>
    </b>
    </td>
    <td style='text-align:center;'>".get_full_name_by_user(get_user_by_id($prieten))."</td>";
    if (rank(get_user_by_id($prieten))=='E')
    echo "<td style='text-align:center;'>".get_clasa_by_user(get_user_by_id($prieten))."</td>";
    else if (rank(get_user_by_id($prieten))=='P')
      {
      $p=query("SELECT id_materie FROM utilizatori WHERE user='$prieten'");
      echo "<td style='text-align:center;'>".get_materie_by_id($p[0])."</td>";
      }
    else echo "<td style='text-align:center;'>administrator</td>";
    echo '<td>
    <a href="index.php?token=mesaj_rapid&id='.$prieten.'">
    <span class="glyphicon glyphicon-envelope"></span>
    </a>
    </td>
    <td>
    <a href="index.php?token=sterge_favorit&id='.$prieten.'" onclick="return confirm(\'Stergeti acest utilizator din lista de favoriti?\');">
    <span class="glyphicon glyphicon-remove"></span>
    </a>
    </td>
    </tr>';
    }
?>
</tbody>
</table>
</div>
</center>
<?
}
else echo '<div class="alert alert-danger" style="width: 60%;" role="alert">Nu aveti niciun favorit adaugat in lista!</div>';
}

else if ($token=="adauga_favorit") //PAGINA DE PROCESARE A CERERII DE ADAUGARE A UNUI USER IN LISTA DE FAVORITI
{
if (!logat() || $_SERVER['REQUEST_METHOD']!='GET' || !isset($_GET['id']))
    redirect("index.php?token=eroare&id=nepermis");
$id=mres($_GET['id']);
if (!exist_user(get_user_by_id($id))) //verificam daca utilizatorul cu acest id chiar exista
    {
    opensession("error", "Utilizatorul nu exista in baza de date!");
    redirect("index.php?token=eroare");
    }
$q=query("SELECT prieteni FROM utilizatori WHERE user='".$_SESSION['user']."'"); //extragem lista de favoriti a utilizatorului curent
$p=$q[0].$id."/"; //completam lista cu favoritul cerut
@mysql_query("UPDATE utilizatori SET prieteni='$p' WHERE user='".$_SESSION['user']."'"); //actualizam lista
opensession("succes", "Utilizatorul <b><i>".get_user_by_id($id)."</i></b> a fost adaugat in lista de favoriti.");
redirect("index.php?token=favoriti");
}

else if ($token=="sterge_favorit") //PAGINA DE PROCESARE A CERERII DE STERGERE A UNUI USER DIN LISTA DE FAVORITI
{
if (!logat() || $_SERVER['REQUEST_METHOD']!='GET' || !isset($_GET['id']) || !is_numeric($_GET['id']))
    redirect("index.php?token=eroare&id=nepermis");
$id=mres($_GET['id']);
if (!exist_user(get_user_by_id($id)))
    {
    opensession("error", "Utilizatorul nu exista in baza de date!");
    redirect("index.php?token=eroare");
    }
if (!prieten($id))
    {
    opensession("error", "Nu aveti acest utilizator in lista de favoriti!");
    redirect("index.php?token=eroare");
    }
$q=query("SELECT prieteni FROM utilizatori WHERE user='".$_SESSION['user']."'");
$p=str_replace($id."/", "", $q[0]);
@mysql_query("UPDATE utilizatori SET prieteni='$p' WHERE user='".$_SESSION['user']."'");
opensession("succes", "Utilizatorul <b><i>".get_user_by_id($id)."</i></b> a fost sters din lista de favoriti.");
redirect("index.php?token=favoriti");
}

else if ($token=="stergere") //PAGINA DE STERGERE A UNUI CONT
{
if (!logat() || rank($_SESSION['user'])!='A')
    redirect("index.php?token=eroare&id=nepermis");
if (isset($_POST['user']))
{
    $user=mres($_POST['user']);
    if ($user=='')
        {
        opensession("error", "Introduceti un nume de utilizator!");
        redirect("index.php?token=stergere");
        }
    if (!exist_user($user))
        {
        opensession("error", "Utilizatorul introdus nu exista in baza de date!");
        redirect("index.php?token=stergere");
        }
    if ($user==$_SESSION['user'])
        {
        opensession("error", "Nu va puteti sterge propriul cont!");
        redirect("index.php?token=stergere");
        }
    if (rank($user)=='P' && rank($_SESSION['user'])!='A')
        {
        opensession("error", "Doar un administrator poate sterge contul unui profesor!");
        redirect("index.php?token=stergere");
        }
    if (rank($user)=='A')
        {
        opensession("error", "Nu puteti sterge contul unui administrator!");
        redirect("index.php?token=stergere");
        }
    $id_user=get_id_by_user($user);
    unlink(get_thumb($user));
    $mp=@mysql_query("SELECT nume_att FROM mesaje WHERE id_user=$id_user"); //stergem atasamentele de la mesaje
            while ($mesaj_primit=@mysql_fetch_array($mp))
                  unlink("uploads/".$mesaj_primit[0]);
            $mt=@mysql_query("SELECT nume_att FROM trimise WHERE id_user=$id_user"); //stergem atasamentele de la mesaje
            while ($mesaj_trimis=@mysql_fetch_array($mp))
                  unlink("uploads/".$mesaj_trimis[0]);
            @mysql_query("DELETE FROM mesaje WHERE id_user=$id_user"); //stergem mesajele primite
            @mysql_query("DELETE FROM trimise WHERE id_user=$id_user"); //stergem mesajele trimise
    $q=@mysql_query("SELECT nume FROM fisiere WHERE id_user=$id_user"); //stergem fisierele incarcate
    while ($r=@mysql_fetch_array($q))
        unlink("uploads/".$r[0]);
    @mysql_query("DELETE FROM fisiere WHERE id_user=$id_user"); 
    $q=@mysql_query("SELECT nume FROM fisiere_categorii WHERE id_user=$id_user"); //stergem fisierele incarcate pe materii
    while ($r=@mysql_fetch_array($q))
        unlink("fisiere_categorii/".$r[0]);
    @mysql_query("DELETE FROM fisiere_categorii WHERE id_user=$id_user");
    $q=query("SELECT imagine FROM postari WHERE id_user=$id_user"); //stergem postarile
    if ($q[0]) unlink($q[0]);
    @mysql_query("DELETE FROM postari WHERE id_user=$id_user");
    @mysql_query("DELETE FROM comentarii WHERE id_user=$id_user"); //stergem comentariile
    @mysql_query("UPDATE anunturi SET anunt='', id_user='' WHERE id_user=$id_user"); //stergem anunturile date de acest utilizator
    $q=@mysql_query("SELECT prieteni FROM utilizatori WHERE prieteni LIKE '%/$id_user%/'"); //stergem acest utilizator din toate listele de prieteni
    while($pri=@mysql_fetch_array($q))
        {
        $p=explode("/", $pri[0]);
        for($i=1; $i<=count($p); $i++)
            if ($p[$i]==$id_user)
                $p[$i]="";
        $pr=implode("/", $p);
        @mysql_query("UPDATE utilizatori SET prieteni='$pr' WHERE prieteni='".$pri[0]."'");
        }
    @mysql_query("DELETE FROM utilizatori WHERE user='$user'"); //stergem utilizatorul
    opensession("succes", "Contul utilizatorului ".$user." a fost sters!");
    redirect("index.php?token=succes");
}
?>
<center>
<h1 class="page-header">Stergere cont</h1><br />
<form class="form-set" role="form" action="index.php?token=stergere" method="POST">
<label>Introduceti numele de utilizator <?=$site_title_short;?> al carui cont doriti sa fie sters:</label>
<input class="form-control" type="text" name="user" max="30" maxlength="30" required/>
<?
read_error();
read_succes();
?>
<button class="btn btn-primary btn-block" style="width: auto;" type="submit" onclick="return confirm('Sigur stergeti contul acestui utilizator? ATENTIE: toate postarile, fisierele incarcate, toate mesajele primite, respectiv trimise de acesta vor fi sterse definitiv!');">Stergere</button>
</form>
</center>
<?
}

else if ($token=="email") //PAGINA DE TRIMITERE A MESAJELOR E-MAIL SI PRIVATE
{
if (!logat())
    redirect("error.php?id=nepermis");
?>
<!--<h1>Bun venit<?if (logat()) echo ', '.get_full_name_by_user($_SESSION['user']);?>!</h1>
<h2><?if (logat()) {
$u=$_SESSION['user'];
$m=query("SELECT COUNT(*) FROM mesaje WHERE user='$u' AND citit=0");
if ($m[0]==0) echo "Nu aveti mesaje necitite.";
else {echo "Aveti <font color='red'>".$m[0]."</font>";
if ($m[0]==1) echo " mesaj necitit."; 
else if ($m[0]>=20) echo " de mesaje necitite.";
else echo " mesaje necitite.";}?><br /><?}?>
</h2>-->
<center><h1 class="page-header">Mesaj nou</h1></center>
<form style="<?if (!($detectare->isMobile() && !$detectare->isTablet())) {?>width: 70%; <?}?>margin: 0 auto;" role="form" action="index.php?token=procesare_email" method="POST" enctype="multipart/form-data">
<label>Catre:<?if ($_SESSION['dest']) {?>
<input type="hidden" name="touser" value="<?=get_user_by_id($_SESSION['dest']);?>"/>
<?echo "<b>".get_user_by_id($_SESSION['dest'])."</b></label>";
$_SESSION['dest']='';}
else{?>
</label>
<input class="form-control" type="text" name="toemail" size="35" placeholder='E-mail'/><?if (logat()) {?>
<label>sau</label>
<input class="form-control" type="text" name="touser" size="25" placeholder="utilizator <?=$site_title_short;?>"/><?}?>
<?}?>
<br />
<input class="form-control" type="text" name="from" size="40" placeholder="De la (e-mail sau nume)"/>
<label>NU completati campul "De la" daca trimiteti PM</label>
<input class="form-control" type="text" name="subject" size="50" placeholder="Subiect"/>
<?if (!isset($_GET['id']))
{?><label>Atasament (optional<?if (rank($_SESSION['user'])=='E') echo", MAX. ".round(get_dim_max()-get_spatiu_user($_SESSION['user']), 2)."MB";?>)
</label>
<input class="form-control" type="file" name="fisier" <?if (blocked_uploads()) echo 'disabled="disabled"';?>/><?}?>
<?
if (isset($_GET['id']))
{
?>
<br />
<div class="alert alert-warning">
      Introduceti mesajul dvs. la inceputul casetei text de mai jos. NU stergeti link-ul de pe ultima linie, generat automat. Acela va fi folosit pentru ca destinatarul sa poata accesa fisierul redirectionat de dvs prin acest mesaj.
</div>
<?
}
?>
<textarea class="form-control" style="resize: none;" name="message" rows="5" cols="100" maxlength="100000" placeholder="Mesaj" required>
<?if (isset($_GET['id']))
$q=@query("SELECT id FROM fisiere WHERE id=".$_GET['id']);
if ($q[0])
{
$cod=rand(11111111111111, 99999999999999);
@mysql_query("UPDATE fisiere SET cod=$cod WHERE id=".$q[0]);
echo "

".$site_link."index.php?token=acces_fisier&id=".$cod;
}
?></textarea>
<?read_error();
read_succes();
?>
<button class="btn btn-primary btn-block" style="width: auto;" <?if (blocked_emails() && logat()) echo 'disabled="disabled"';?>>Trimitere</button>
</form>
<?
}

else if ($token=="procesare_email") //PAGINA DE PROCESARE A MESAJELOR E-MAIL/PM
{
if ($_SERVER['REQUEST_METHOD']!='POST' || !logat())
    redirect("index.php?token=eroare&id=nepermis");
$from=mres($_POST['from']);
$toemail=mres($_POST['toemail']);
$touser=mres($_POST['touser']);
$subject=mres($_POST['subject']);
$message=mres($_POST['message']);
$flag=1;
if ($touser=='' && $toemail=='')
    $flag=0;
if ($subject=='')
    $subject="&lt;fara subiect&gt;";
if ($message=='')
    $flag=0;
if ($flag==0)
    {
    opensession('error', 'Completati toate campurile!');
    go_back();
    exit();
    }
if(($toemail!="") && ($touser!=""))
    {
    opensession('error', 'Nu aveti voie sa completati ambele campuri destinatar!');
    go_back();
    exit();
    }
if($toemail!="" && !email_valid($toemail))
    {
    opensession('error', 'E-mail invalid!');
    go_back();
    exit();
    }
else if($touser!="" && !exist_user($touser))
    {
    opensession('error', 'Utilizatorul introdus nu exista in baza de date!');
    go_back();
    exit();
    }
if($from=="" && $touser=="")
    {
    opensession('error', 'Daca trimiteti e-mail, va rugam completati campul "De la"!');
    go_back();
    exit();
    }
else if($from!="" && $touser!="") 
    {
    opensession('error', 'Nu completati campul "De la" daca trimiteti PM!');
    go_back();
    exit();
    }
if($touser && $from=="")
    $from=$_SESSION['user'];
$extensii=get_extensii();
if ($_FILES['fisier']['name'])
    {
    if (blocked_uploads())
    {
        opensession('error', 'Incarcarile de fisiere sunt blocate!');
        go_back();
        exit();
    }
    if ($_FILES['fisier']['error'])
    {
    opensession('error', 'Incarcarea a returnat eroarea '.$_FILES['fisier']['error']);
    go_back();
    exit();
    }
    $nume=$_FILES['fisier']['name'];
    $info_fisier=@pathinfo($nume);
    $marime=$_FILES['fisier']['size']/1024;
    if (in_array(strtolower($info_fisier['extension']), $extensii))
    {
    opensession('error', 'Extensie neacceptata!');
    go_back();
    exit();
    }
    if (spatiu_depasit($_SESSION['user'], $marime) && rank($_SESSION['user'])=='E')
    {
    opensession('error', 'Fisierul este prea mare!');
    go_back();
    exit();
    }
    if (query("SELECT * FROM fisiere WHERE nume='$nume'"))
    {
    $v=explode(".", $nume);
    $nume=$v[0].time().".".$info_fisier['extension'];
    $redenumire=TRUE;
    }
    @move_uploaded_file($_FILES['fisier']['tmp_name'], 'uploads/'.$nume);
    }
    $cod=rand(11111111111111, 99999999999999);
    $cale='uploads/'.$nume;
    $attach=$cale;
    @mysql_query("INSERT INTO fisiere (nume, cale, marime, id_user, id_clasa, cod, data) VALUES ('$nume', '$cale', $marime, '".get_id_by_user($_SESSION['user'])."', 1, '$cod', NOW())");
    increment_uploads($_SESSION['user'], $marime);
    $cale='';
    if($nume)
        $cale=$site_link.'index.php?token=acces_fisier&id='.$cod;
    if ($touser) 
        {
        if ($nume)
            {@mysql_query("INSERT INTO mesaje (id_user, mesaj, de_la, subiect, data, atasament, nume_att, citit) VALUES (".get_id_by_user($touser).", '$message', ".get_id_by_user($from).", '$subject', NOW(), '$cale', '$nume', 0)");
            @mysql_query("INSERT INTO trimise (id_user, mesaj, destinatar, subiect, data, atasament, nume_att) VALUES ('".get_id_by_user($_SESSION['user'])."', '$message', ".get_id_by_user($touser).", '$subject', NOW(), '$cale', '$nume')");
            }
        else 
            {@mysql_query("INSERT INTO mesaje (id_user, mesaj, de_la, subiect, data, citit) VALUES (".get_id_by_user($touser).", '$message', ".get_id_by_user($from).", '$subject', NOW(), 0)");
            @mysql_query("INSERT INTO trimise (id_user, mesaj, destinatar, subiect, data) VALUES ('".get_id_by_user($_SESSION['user'])."', '$message', ".get_id_by_user($touser).", '$subject', NOW())");
            }
        $to=get_email_by_user($touser);
        email($to, "Ati primit un PM pe ".$site_title_short, "Aceasta este o instiintare cum ca ati primit un mesaj privat pe ".$site_title_short.", cu subiectul <<".$subject.">>.\n\nPentru a-l citi, vizitati ".$site_link.".\n\nCu respect, \nCosmin Crihan (admin).\n\nAcesta este un mesaj trimis automat. Nu raspundeti acestui e-mail.", "contact@".$site_link_short);
        }
    else 
        {
	$msj=$_POST['message'];
        if ($nume) email($toemail, $_POST['subject'], $msj, $from, $attach);
        else email($toemail, $_POST['subject'], $msj, $from);
        if (logat()) 
            if ($nume)
                @mysql_query("INSERT INTO trimise (id_user, mesaj, destinatar, subiect, data, atasament, nume_att) VALUES ('".get_id_by_user($_SESSION['user'])."', '$message', '$toemail', '$subject', NOW(), '$cale', '$nume')");
            else @mysql_query("INSERT INTO trimise (id_user, mesaj, destinatar, subiect, data) VALUES ('".get_id_by_user($_SESSION['user'])."', '$message', '$toemail', '$subject', NOW())");
        }
if (logat()) increment_messages($_SESSION['user']);
opensession('succes', 'Mesajul a fost trimis!');
if ($redenumire)
    $_SESSION['succes']=$_SESSION['succes']."<br/>Fisierul atasat a fost redenumit automat ca ".$nume."!";
setcookie("53XvwsZeY8KtlxmB5lcF", "", time()-1);
redirect("index.php?token=email");
}

else if ($token=="acces_fisier") //PAGINA CARE CONFERA ACCES LA DESCARCAREA FISIERELOR REDIRECTIONATE
{
$cod=mres($_GET['id']);
if (is_numeric($cod) && $_SERVER['REQUEST_METHOD']=='GET' && isset($_GET['id']) && is_fisier($cod))
{
    $id=get_fileid($cod); //preluam codul de acces si, pe baza acestuia, id-ul fisierului pentru descarcare
    //@mysql_query("UPDATE fisiere SET cod=1 WHERE cod='$cod'");
    opensession("acc", 1); //dam acces la descarcarea fisierului
    redirect("index.php?token=descarca&id=$id");
}
else if (!is_fisier($cod))
{
    opensession("error", "Fisierul nu exista!");
    redirect("index.php?token=eroare");
}
else redirect("index.php?token=eroare&id=nepermis");
}

else if ($token=="mesaje_primite") //PAGINA CU LISTA MESAJELOR PRIMITE
{
if (!logat())
    redirect("index.php?token=eroare&id=nepermis");
?>
<center>
<h1 class="page-header">Mesaje primite
<?if (query("SELECT * FROM mesaje WHERE id_user='".get_id_by_user($_SESSION['user'])."'"))
echo '
<a href="index.php?token=sterge_tot&id=inbox" onclick="return confirm(\'Sigur stergeti toate mesajele primite?\');"><span class="glyphicon glyphicon-trash"></span></a>';
?></h1>
<?
read_succes();
$u=$_SESSION['user'];
$id_user=get_id_by_user($u);
$m=query("SELECT COUNT(*) FROM mesaje WHERE id_user=$id_user");
if ($m[0]==0)
    if (($detectare->isMobile() && !$detectare->isTablet())) echo '<div class="alert alert-danger" role="alert">Nu aveti niciun mesaj primit.</div>';
    else echo '<div class="alert alert-danger" style="width: 70%;" role="alert">Nu aveti niciun mesaj primit.</div>';
else {
?>
<div class="table-responsive">
<table class="table" style="width: auto;">
<thead>
<th style="text-align: center;">Expeditor</th>
<th style="text-align: center;">Subiect</th>
<?if (!($detectare->isMobile() && !$detectare->isTablet())) {?>
<th style="text-align: center;">Data primirii</th>
<?}?>
</thead>
<tbody>
<?
$query="SELECT * FROM mesaje WHERE id_user=$id_user ORDER BY data DESC";
$query2=query("SELECT COUNT(*) FROM mesaje WHERE id_user=$id_user ORDER BY data DESC");
$rez=@mysql_query($query);
$m_id=1;
while ($row=@mysql_fetch_array($rez))
        {
        if (!exist_user(get_user_by_id($row['de_la'])))
            $user=$row['de_la'];
        else $user=get_user_by_id($row['de_la']);
        if (!exist_user(get_user_by_id($row['de_la']))) $user="nGSN";
        ?>
        <tr class='panel panel-default'
        <?
        if ($row['citit']==1) {echo " style='background-color: #eee;'>
        <td style='text-align:center;'><a name=m".$m_id."><p style='display: none;'>ancora mesaj</p></a>";
        if (!($detectare->isMobile() && !$detectare->isTablet())) echo "
        <img src='".get_thumb($user)."' style='border-radius:5px; vertical-align:middle;' width='30' height='30'/>";
        if (($detectare->isMobile() && !$detectare->isTablet()))
            {if (strlen($user)>10)
                $user=substr($user, 0, 9)."...";}
        if (($detectare->isMobile() && !$detectare->isTablet()))
            {if (strlen($row['subiect'])>10)
                $row['subiect']=substr($row['subiect'], 0, 9)."...";}
        if ($user!="nGSN")
        echo "<a href='index.php?token=profil&id=".$row['de_la']."'>".$user."</a>
        </td>";
        else echo $user;
        echo "<td style='text-align:center;'>".$row['subiect'];
        if (attachment_link($row['id']))
        echo "<a href=".attachment_link($row['id'])."> <span class='glyphicon glyphicon-paperclip'></span></a>";
        echo "</td>";
        if (!($detectare->isMobile() && !$detectare->isTablet())) echo "
        <td style='text-align:center;'>".$row['data']."</td>";
        echo "
        <td>
        <a href='index.php?token=mesaje_primite&id=".$row['id']."&m=".$m_id."'>
        <span class='glyphicon glyphicon-comment'></span>
        </a>
        </td>
        <td>
        <a href='index.php?token=raspuns&id=".$row['id']."'>
        <span class='glyphicon glyphicon-share-alt'></span>
        </td>
        <td>
        <a onclick='return confirm(\"Sigur stergeti acest mesaj?\");' href='index.php?token=sterge_inbox&id=".$row['id']."'>
        <span class='glyphicon glyphicon-trash'></span>
        </a>
        </td>
        </tr>";
        }
        else {echo ">
        <td style='text-align:center; font-weight:bold;'>
        <img src='".get_thumb($user)."' style='border-radius:5px; vertical-align:middle;' width='30' height='30'/>";
        if ($user!="nGSN")
        echo "<a href='index.php?token=profil&id=".$row['de_la']."'>".$user."</a>
        </td>";
        else echo $user;
        echo "</td>
        <td style='text-align:center; font-weight:bold;'>".$row['subiect'];
        if (attachment_link($row['id']))
        echo "<a href=".attachment_link($row['id'])."> <span class='glyphicon glyphicon-paperclip'></span></a>";
        echo "
        </td>
        <td style='text-align:center; font-weight:bold;'>".$row['data']."</td>
        <td>
        <a href='index.php?token=mesaje_primite&id=".$row['id']."&m=".$m_id."'>
        <span class='glyphicon glyphicon-comment'></span>
        </a>
        </td>
        <td>
        <a href='index.php?token=raspuns&id=".$row['id']."'>
        <span class='glyphicon glyphicon-share-alt'></span>
        </td>
        <td>
        <a onclick='return confirm(\"Sigur stergeti acest mesaj?\");' href='index.php?token=sterge_inbox&id=".$row['id']."'>
        <span class='glyphicon glyphicon-trash'></span>
        </a>
        </td>
        </tr>";
        }
        if (isset($_GET['id']))
        {
        $id=mres($_GET['id']);
        $res=query("SELECT * FROM mesaje WHERE id=$id");
        if (!$res || $res['id_user']!=get_id_by_user($_SESSION['user']))
            redirect("index.php?token=eroare&id=nepermis");
        if (isset($_GET['m']))
            {
            $id_m=mres($_GET['m']);
            if ($id_m==1 || $id_m==2) $id_m="";
            else $id_m="#m".($id_m-2);
            redirect("index.php?token=mesaje_primite&id=".$id.$id_m);
            }
        if ($id==$row['id'])
        {
        $q=query("SELECT citit FROM mesaje WHERE id=$id");
        if ($q[0]==0)
        {@mysql_query("UPDATE mesaje SET citit=1 WHERE id=$id");
        redirect("index.php?token=mesaje_primite&id=$id");}
        ?>
        </tbody>
        </table>
        </div>
        </center>
        <div id="d<?=$res['id'];?>" class="panel panel-info">
        <div class="panel-heading">Data primirii: <?echo $res['data'];
        if (blocked_emails()) $b='disabled="disabled"';
            echo "<a href='#'><span style='float: right;' class='glyphicon glyphicon-remove' id='buton_msj'  onclick=\"close_msg('d".$res['id']."');\"></span></a>
        <a style='float:right;' onclick='return confirm(\"Sigur stergeti acest mesaj?\");' href='index.php?token=sterge_inbox&id=".$id."'><span class='glyphicon glyphicon-trash'></span></a>";
        if ($res['de_la']!=0) echo "<a style='float:right;' href='index.php?token=raspuns&id=".$id."'><span class='glyphicon glyphicon-share-alt'></span></a>";
        if (exist_user(get_user_by_id($res['de_la'])))
            $user=get_user_by_id($res['de_la']);
        else $user=$res['de_la'];
        ?>
        <br />
        De la: <?if ($res['de_la']==0) echo $site_title_short; else echo $user;?>
        <br />
        Subiect: <?echo $res['subiect'];
        if (attachment_link($id))
                echo "<br />Atasament: <a href=".attachment_link($id).">".attachment_name($id)."<span class='glyphicon glyphicon-paperclip' style='padding-left: 2px;'></span></a>";
        ?>
        </div>
        <?echo "<div class='panel-body' style='white-space: pre-wrap; padding: 5px;'>".$res['mesaj']."</div>";?>
        </div>
        <?if ($m_id!=$query2[0]){?>
        <center>
        <div class="table-responsive">
        <table class="table" style="width: auto;">
        <tbody>
        <?
        }
        }
        }
        $m_id++;
        }
?>
</tbody>
</table>
</div>
</center>
<?
}
}

else if ($token=="mesaje_trimise") //PAGINA CU LISTA MESAJELOR TRIMISE
{
if (!logat())
    redirect("index.php?token=eroare&id=nepermis");
?>
<center>
<h1 class="page-header">Mesaje trimise
<?if (query("SELECT * FROM trimise WHERE id_user='".get_id_by_user($_SESSION['user'])."'"))
echo '<a href="index.php?token=sterge_tot&id=outbox" onclick="return confirm(\'Sigur stergeti toate mesajele trimise?\');">
<span class="glyphicon glyphicon-trash"></span>
</a>';
?></h1>
<?
read_succes();
$u=$_SESSION['user'];
$id_user=get_id_by_user($u);
$m=query("SELECT COUNT(*) FROM trimise WHERE id_user=$id_user");
if ($m[0]==0)
    if (($detectare->isMobile() && !$detectare->isTablet())) echo '<div class="alert alert-danger" role="alert">Nu aveti niciun mesaj trimis.</div>';
    else echo '<div class="alert alert-danger" style="width: 70%;" role="alert">Nu aveti niciun mesaj trimis.</div>';
else 
{
?>
<div class="table-responsive">
<table class="table" style="width: auto;">
<thead>
<th style="text-align: center;">Destinatar</th>
<th style="text-align: center;">Subiect</th>
<?if (!($detectare->isMobile() && !$detectare->isTablet())) {?>
<th style="text-align: center;">Data trimiterii</th>
<?}?>
</thead>
<tbody>
<?
$id_user=get_id_by_user($_SESSION['user']);
$query="SELECT * FROM trimise WHERE id_user=$id_user ORDER BY data DESC ";
$query2=query("SELECT COUNT(*) FROM trimise WHERE id_user=$id_user ORDER BY data DESC");
$rez=@mysql_query($query);
$m_id=1;
while ($row=@mysql_fetch_array($rez))
        {
        if (!get_user_by_id($row['destinatar'])) $user=$row['destinatar'];
        else $user=get_user_by_id($row['destinatar']);
        if (($detectare->isMobile() && !$detectare->isTablet()))
            {if (strlen($user)>10)
                $user=substr($user, 0, 9)."...";}
        if (($detectare->isMobile() && !$detectare->isTablet()))
            {if (strlen($row['subiect'])>10)
                $row['subiect']=substr($row['subiect'], 0, 9)."...";}
        ?>
        <tr class='panel panel-default' style="background-color: #eee;">
        <?
        echo "<td style='text-align:center;'><a name='m".$m_id."'><p style='display: none;'>ancora mesaj</p></a>";
        if (!($detectare->isMobile() && !$detectare->isTablet()) && exist_user(get_user_by_id($row['destinatar']))) echo "<img src='".get_thumb(get_user_by_id($row['destinatar']))."' style='border-radius:5px; vertical-align:middle;' width='30' height='30'/>";
        if (!email_valid($row['destinatar'])) echo "<a href='index.php?token=profil&id=".$row['destinatar']."'>".$user."</a>";
        else echo $user;
        echo "</td>
        <td style='text-align:center;'>".$row['subiect'];
        if (attachment_link_outbox($row['id']))
        echo "<a href=".attachment_link_outbox($row['id'])."> <span class='glyphicon glyphicon-paperclip'></span></a>";
        echo "</td>";
        if (!($detectare->isMobile() && !$detectare->isTablet())) echo "
        <td style='text-align:center;'>".$row['data']."</td>";
        echo "
        <td>
        <a href='index.php?token=mesaje_trimise&id=".$row['id']."&m=".$m_id."'>
        <span class='glyphicon glyphicon-comment'></span>
        </a>
        </td>
        <td>
        <a onclick='return confirm(\"Sigur stergeti acest mesaj?\");' href='index.php?token=sterge_outbox&id=".$row['id']."'>
        <span class='glyphicon glyphicon-trash'></span>
        </a>
        </td>
        </tr>";
        if (isset($_GET['id']))
        {
        $id=mres($_GET['id']);
        $res=@query("SELECT * FROM trimise WHERE id='$id'");
        if (!res || $res['id_user']!=get_id_by_user($_SESSION['user']))
            redirect("index.php?token=eroare&id=nepermis");
        if (isset($_GET['m']))
            {
            $id_m=mres($_GET['m']);
            if ($id_m==1 || $id_m==2) $id_m="";
            else $id_m="#m".($id_m-2);
            redirect("index.php?token=mesaje_trimise&id=".$id.$id_m);
            }
        if ($id==$row['id'])
        {
        ?>
        </tbody>
        </table>
        </div>
        </center>
        <div id="d<?=$res['id'];?>" class="panel panel-info">
        <div class="panel-heading">Data trimiterii: <?echo $res['data'];
        echo "<a href='#'><span style='float: right;' class='glyphicon glyphicon-remove' id='buton_msj'  onclick=\"close_msg('d".$res['id']."');\"></span></a>
        <a style='float:right;' onclick='return confirm(\"Sigur stergeti acest mesaj?\");' href='index.php?token=sterge_outbox&id=".$id."'><span class='glyphicon glyphicon-trash'></span></a>";
        if (exist_user(get_user_by_id($res['destinatar'])))
            $user=get_user_by_id($res['destinatar']);
        else $user=$res['destinatar'];
        ?>
        <br />
        Destinatar: <?echo $user;?><br />
        Subiect: <?echo $res['subiect'];?>
        <?if (attachment_link_outbox($id))
            echo "<br />Atasament: <a href=".attachment_link_outbox($id).">".attachment_name_outbox($id)."<span class='glyphicon glyphicon-paperclip' style='padding-left: 2px;'></span></a>";
        ?>
        </div>
        <?
        echo "<div class='panel-body' style='white-space: pre-wrap; padding: 5px;'>".$res['mesaj']."</div>";?>
        </div>
        <?if ($m_id!=$query2[0]) {?>
        <center>
        <div class="table-responsive">
        <table class="table" style="width: auto;">
        <tbody>
        <?
        }
        }
        }
        $m_id++;
        }
?>
</tbody>
</table>
</div>
<?
}
}

else if ($token=="sterge_inbox") //PAGINA DE PROCESARE - STERGEREA UNUI MESAJ DIN INBOX
{
if (!logat() || $_SERVER['REQUEST_METHOD']!='GET' || !is_numeric($_GET['id']) || !isset($_GET['id']))
    redirect("index.php?token=eroare&id=nepermis");
$id=mres($_GET['id']);
$m=query("SELECT * FROM mesaje WHERE id=$id");
if (!$m || $m['id_user']!=get_id_by_user($_SESSION['user']))
    redirect("index.php?token=eroare?id=nepermis");
if ($m['atasament'])
    {unlink("uploads/".$m['nume_att']);
    @mysql_query("DELETE FROM fisiere WHERE nume='".$m['nume_att']."'");
    }
@mysql_query("DELETE FROM mesaje WHERE id='$id'");
opensession('succes', 'Mesajul a fost sters.');
redirect("index.php?token=mesaje_primite");
}

else if ($token=="sterge_outbox") //PAGINA DE PROCESARE - STERGEREA UNUI MESAJ DIN OUTBOX
{
if (!logat() || $_SERVER['REQUEST_METHOD']!='GET' || !isset($_GET['id']) || !is_numeric($_GET['id']))
    redirect("index.php?token=eroare&id=nepermis");
$id=mres($_GET['id']);
if (!query("SELECT * FROM trimise WHERE id=$id"))
    redirect("index.php?token=eroare&id=nepermis");
$q=query("SELECT * FROM trimise WHERE id=$id");
if (get_id_by_user($_SESSION['user'])!=$q['id_user'])
    redirect("index.php?token=eroare&id=nepermis");
@mysql_query("DELETE FROM trimise WHERE id=$id");
opensession('succes', 'Mesajul a fost sters.');
redirect("index.php?token=mesaje_trimise");
}

else if ($token=="citeste_inbox") //PAGINA DE CITIRE A UNUI MESAJ DIN INBOX - NEFOLOSITA
{
if (!logat() || !is_numeric($_GET['id']) || $_SERVER['REQUEST_METHOD']!='GET' || !isset($_GET['id']))
    redirect("index.php?token=eroare&id=nepermis");
$id=mres($_GET['id']);
$res=@query("SELECT * FROM mesaje WHERE id=$id");
if (!$res || $res['id_user']!=get_id_by_user($_SESSION['user']))
    redirect("index.php?token=eroare&id=nepermis");
$q=query("SELECT citit FROM mesaje WHERE id=$id");
if ($q[0]==0)
{@mysql_query("UPDATE mesaje SET citit=1 WHERE id=$id");
redirect("index.php?token=citeste_inbox&id=$id");}
?>
<div class="panel panel-info" style="margin-top: 15px;">
<div class="panel-heading">Data primirii: <?echo $res['data'];
if (blocked_emails()) $b='disabled="disabled"';
echo "<a style='float:right;' onclick='return confirm(\"Sigur stergeti acest mesaj?\");' href='index.php?token=sterge_inbox&id=".$id."'><span class='glyphicon glyphicon-trash'></span></a>";
if ($res['de_la']!=0) echo "<a style='float:right;' href='index.php?token=raspuns&id=".$id."'><span class='glyphicon glyphicon-share-alt'></span></a>";
if (exist_user(get_user_by_id($res['de_la'])))
    $user=get_user_by_id($res['de_la']);
else $user=$res['de_la'];
?>
<br />
De la: <?if ($res['de_la']==0) echo $site_title_short; else echo $user;?>
<br />
Subiect: <?echo $res['subiect'];
if (attachment_link($id))
        echo "<a style='float: right;' href=".attachment_link($id)."><span class='glyphicon glyphicon-paperclip'></span>".attachment_name($id)."</a>";
?>
</div>
<?echo "<pre style='white-space: no-wrap; margin: 0px;'>".$res['mesaj']."</pre>";?>
</div>
<?
}

else if ($token=="citeste_outbox") //PAGINA DE CITIRE A UNUI MESAJ DIN OUTBOX - NEFOLOSITA
{
if (!logat() || !is_numeric($_GET['id']) || $_SERVER['REQUEST_METHOD']!='GET' || !isset($_GET['id']))
    redirect("index.php?token=eroare&id=nepermis");
$id=mres($_GET['id']);
$res=@query("SELECT * FROM trimise WHERE id='$id'");
if (!res || $res['id_user']!=get_id_by_user($_SESSION['user']))
    redirect("index.php?token=eroare&id=nepermis");
?>
<div class="panel panel-info" style="margin-top: 15px;">
<div class="panel-heading">Data primirii: <?echo $res['data'];
echo "<a style='float:right;' onclick='return confirm(\"Sigur stergeti acest mesaj?\");' href='index.php?token=sterge_outbox&id=".$id."'><span class='glyphicon glyphicon-trash'></span></a>";
if (exist_user(get_user_by_id($res['destinatar'])))
    $user=get_user_by_id($res['destinatar']);
else $user=$res['destinatar'];
?>
<br />
Destinatar: <?echo $user;?><br />
Subiect: <?echo $res['subiect'];?><br />
<?if (attachment_link($id))
        echo "<a style='float: right;' href=".attachment_link_outbox($id)."><span class='glyphicon glyphicon-paperclip'></span>".attachment_name_outbox($id)."</a>";
?>
</div>
<?
echo "<pre style='margin: 0px; min-height: 350px;'>".$res['mesaj']."</pre>";?>
</div>
<?
}

else if ($token=="raspuns") //PAGINA DE RASPUNS LA UN MESAJ PRIVAT
{
if (!logat() || $_SERVER['REQUEST_METHOD']!='GET' || !is_numeric($_GET['id']) || !isset($_GET['id']))
    redirect("index.php?token=eroare&id=nepermis");
$id=mres($_GET['id']);
$q=query("SELECT de_la FROM mesaje WHERE id=$id");
if ($q[0]==0) redirect("index.php?token=eroare&id=nepermis");
$res=@query("SELECT * FROM mesaje WHERE id='$id'");
if (!$res || $res['id_user']!=get_id_by_user($_SESSION['user']))
    redirect("index.php?token=eroare&id=nepermis");
?>
<form class="form-set" role="form" action="index.php?token=procesare_raspuns&id=<? echo $id;?>" method="POST" enctype="multipart/form-data">
<label>Catre: </label><?echo "&nbsp;<b><i>".get_user_by_id($res['de_la'])."</i></b>";?>
<input class="form-control" type="text" name="subject" size="50" onclick="this.value='';" onfocus="this.select()" onblur="this.value=!this.value?'<?echo 'Raspuns: '.$res['subiect'];?>':this.value;" value="<?echo 'Raspuns: '.$res['subiect'];?>"/>
<label>Atasament (optional<?if (rank($_SESSION['user'])=='E') echo ", MAX. ".round(get_dim_max()-get_spatiu_user($_SESSION['user']), 2)."MB";?>)
</label>
<input class="form-control" type="file" name="fisier" <?if (blocked_uploads()) echo 'disabled="disabled"';?>/>
<textarea class="form-control" style="resize: none;" name="message" rows="10" cols="60" maxlength="100000" placeholder="Mesaj" required></textarea>
<?
read_error();
?>
<button class="btn btn-primary btn-block" style="width: auto;" type="submit" <?if (blocked_emails()) echo 'disabled="true"';?>>Trimitere</button>
</form>
<?
}

else if ($token=="procesare_raspuns") //PAGINA DE PROCESARE - RASPUNS LA UN MESAJ PRIVAT
{
if (!logat() || $_SERVER['REQUEST_METHOD']!='GET' && $_SERVER['REQUEST_METHOD']!='POST' || !isset($_GET['id']) || !is_numeric($_GET['id']))
    redirect("index.php?token=eroare&id=nepermis");
$id=$_GET['id'];
$res=query("SELECT * FROM mesaje WHERE id='$id'");
if (!$res['id_user'] || $res['id_user']!=get_id_by_user($_SESSION['user']))
    redirect("index.php?token=eroare&id=nepermis");
$subject=mres($_POST['subject']);
$message=mres($_POST['message']);
$touser=$res['de_la'];
$from=$_SESSION['user'];
$flag=1;
if ($subject=='')
    $subject="&lt;fara subiect&gt;";
if ($message=='')
    $flag=0;
if ($flag==0)
    {
    opensession('error', 'Completati toate campurile!');
    go_back();
    exit();
    }
$extensii=get_extensii();
    if ($_FILES['fisier']['name'])
    {
        if (blocked_uploads())
        {
            opensession('error', 'Incarcarile de fisiere sunt blocate!');
            go_back();
            exit();
        }
        if ($_FILES['fisier']['error'])
            {
            opensession('error', 'Incarcarea a returnat eroarea '.$_FILES['fisier']['error']);
            go_back();
            exit();
            }
        $nume=$_FILES['fisier']['name'];
        $info_fisier=@pathinfo($nume);
        $marime=$_FILES['fisier']['size']/1024;
        if (in_array(strtolower($info_fisier['extension']), $extensii))
            {
            opensession('error', 'Extensia nu este acceptata!');
            go_back();
            exit();
            }
        if (spatiu_depasit($_SESSION['user'], $marime) && rank($_SESSION['user'])=='E')
            {
            opensession('error', 'Fisierul este prea mare!');
            go_back();
            exit();
            }
        if (query("SELECT * FROM fisiere WHERE nume='$nume'"))
            {
            $v=explode(".", $nume);
            $nume=$v[0].time().".".$info_fisier['extension'];
            $redenumire=TRUE;
            }
        @move_uploaded_file($_FILES['fisier']['tmp_name'], 'uploads/'.$nume);
        $clasa=get_clasa_by_user($touser);
        $id_user=get_id_by_user($_SESSION['user']);
        if (!$clasa) $clasa=0;
        $cale=$site_link.'uploads/'.$nume;
        @mysql_query("INSERT INTO fisiere (nume, cale, marime, id_user, id_clasa, data) VALUES ('$nume', '$cale', $marime, $id_user, $clasa, NOW())");
        $rez=@query("SELECT id FROM fisiere WHERE nume='$nume'");
        $id=$rez[0];
        $cale=$site_link."index.php?token=descarca&id=".$id;
        increment_uploads($_SESSION['user'], $marime);
    }
$from=get_id_by_user($from);
@mysql_query("INSERT INTO mesaje (id_user, mesaj, de_la, subiect, data, atasament, nume_att, citit) VALUES ($touser, '$message', $from, '$subject', NOW(), '$cale', '$nume', 0)");
@mysql_query("INSERT INTO trimise (id_user, mesaj, destinatar, subiect, data, atasament, nume_att) VALUES ($from, '$message', $touser, '$subject', NOW(), '$cale', '$nume')");
$to=get_email_by_user($touser);
email($to, "Ati primit un PM pe ".$site_title_short, "Aceasta este o instiintare cum ca ati primit un raspuns la un mesaj privat trimis pe ".$site_title_short.", cu subiectul <<".$subject.">>.\n\nPentru a-l citi, vizitati ".$site_link.".\n\nCu respect,\nCosmin Crihan (admin).\n\nAcesta este un mesaj trimis automat. Nu raspundeti acestui e-mail.", "contact@".$site_link_short);
opensession('succes', 'Raspunsul fost trimis!');
if ($redenumire)
    $_SESSION['succes']=$_SESSION['succes']."<br/>Fisierul atasat a fost redenumit automat ca ".$nume."!";
increment_messages($_SESSION['user']);
redirect("index.php?token=email");
}

else if ($token=="sterge_tot") //PAGINA DE STERGERE MULTIPLA
{
if (!logat() || $_SERVER['REQUEST_METHOD']!='GET')
    redirect("index.php?token=eroare&id=nepermis");
$id=mres($_GET['id']);
$clasa=mres($_GET['clasa']);
if ($id=='inbox')
{
if (!query("SELECT * FROM mesaje WHERE id_user=".get_id_by_user($_SESSION['user'])))
    {
    opensession("error", "Nu exista niciun mesaj de sters!");
    redirect("index.php?token=mesaje_primite");
    }
@mysql_query("DELETE FROM mesaje WHERE id_user=".get_id_by_user($_SESSION['user']));
opensession("succes", "Toate mesajele au fost sterse!");
redirect("index.php?token=mesaje_primite");
}
else if ($id=='outbox')
{
if (!query("SELECT * FROM trimise WHERE id_user=".get_id_by_user($_SESSION['user'])))
    {
    opensession("error", "Nu exista niciun mesaj de sters!");
    redirect("index.php?token=mesaje_trimise");
    }
@mysql_query("DELETE FROM trimise WHERE id_user=".get_id_by_user($_SESSION['user']));
opensession("succes", "Toate mesajele au fost sterse!");
redirect("index.php?token=mesaje_trimise");
}
else if ($id=='fisiere' && $clasa)
{
if (rank($_SESSION['user'])=='E')
    {
    opensession("error", "Nu aveti dreptul sa stergeti toate fisierele!");
    redirect("index.php?token=fisiere");
    }
if (!query("SELECT * FROM fisiere WHERE id_clasa=".get_id_by_class($clasa)))
    {
    opensession("error", "Nu exista niciun fisier de sters!");
    redirect("index.php?token=fisiere");
    }
$r=@mysql_query("SELECT * FROM fisiere WHERE id_clasa=".get_id_by_class($clasa));
while ($row=@mysql_fetch_array($r))
    {
    @mysql_query("DELETE FROM fisiere WHERE id=".$row['id']);
    $cale="uploads/".$row['nume'];
    unlink($cale);
    }
opensession("succes", "Toate fisierele au fost sterse!");
redirect("index.php?token=fisiere");
}
else if ($id=='fisiere_toate')
{
if (rank($_SESSION['user'])=='E')
    redirect("error.php?id=nepermis");
if (!query("SELECT * FROM fisiere WHERE id_clasa=1"))
    {
    opensession("error", "Nu exista niciun fisier de sters!");
    redirect("index.php?token=toate");
    }
$r=@mysql_query("SELECT * FROM fisiere WHERE id_clasa=1");
while ($row=@mysql_fetch_array($r))
    {
    @mysql_query("DELETE FROM fisiere WHERE id=".$row['id']);
    $cale="uploads/".$row['nume'];
    unlink($cale);
    }
opensession("succes", "Toate fisierele au fost sterse!");
redirect("index.php?token=toate");
}
else redirect("index.php?token=eroare&id=nepermis");
}

else if ($token=="incarca") //PAGINA DE INCARCARE A UNUI FISIER
{
if (!logat())
    redirect("index.php?token=eroare&id=nepermis");
if (rank($_SESSION['user'])=='P' || rank($_SESSION['user'])=='A')
{
?>
<center>
<h1 class="page-header">Incarcare fisier</h1>
<form class="form-set" role="form" action="index.php?token=procesare_incarca" method="POST" enctype="multipart/form-data">
<label>Clasa pentru care incarcati fisierul: </label>
<select class="form-control" style="width: auto;" name="clasa">
<?
$rez=@mysql_query("SELECT * FROM clase ORDER BY clasa ASC");
while ($row=@mysql_fetch_array($rez))
    {
    if ((rank($_SESSION['user'])=='P' || rank($_SESSION['user'])=='A') && $row['clasa']=='ALL') echo "<option>".$row['clasa']."</option>";
    if ($row['clasa']!='ALL') echo "<option>".$row['clasa']."</option>";
    }
    ?>
</select>
<input class="form-control" type="file" name="fisier" <?if (blocked_uploads()) echo 'disabled="disabled"';?> required /><br />
<?
read_error();
?>
<button class="btn btn-primary btn-block" style="width: auto;" type="submit" <?if (blocked_uploads()) echo 'disabled="disabled"';?>>Incarcare</button>
</form>
</center>
<?
}
else
{
?>
<center>
<h1 class="page-header">Incarcare fisier</h1>
<div class="alert alert-warning">ATENTIE! Fisierul trebuie sa ocupe max. <?=round(get_dim_max()-get_spatiu_user($_SESSION['user']), 2);?> MB!</div>
<form class="form-set" role="form" action="index.php?token=procesare_incarca" method="POST" enctype="multipart/form-data">
<label>Clasa pentru care incarcati fisierul: 
<?
$u=$_SESSION['user'];
$rez=@query("SELECT id_clasa FROM utilizatori WHERE user='$u'");
$clasa=$rez[0];
$cls=get_class_by_id($clasa);
if ($cls!='ABS') echo $cls; else echo "ABSOLVENTI";
?>
</label>
<input type="hidden" name="clasa" value="<?=get_class_by_id($clasa);?>"/>
<input class="form-control" type="file" name="fisier" <?if (blocked_uploads()) echo 'disabled="disabled"';?> required />
<?
read_error();
?>
<button class="btn btn-primary btn-block" style="width: auto;" type="submit" <?if (blocked_uploads()) echo 'disabled="disabled"';?>>Incarcare</button>
</form>
</center>
<?
}
}

else if ($token=="procesare_incarca") //PAGINA DE PROCESARE - INCARCAREA UNUI FISIER
{
if (!logat() || !isset($_POST['clasa']))
    redirect("index.php?token=eroare&id=nepermis");
$clasa=mres($_POST['clasa']);
$extensii=get_extensii();
$user=$_SESSION['user'];
$id_clasa=get_id_by_class($clasa);
if (blocked_uploads())
{
    opensession('error', 'Incarcarile de fisiere sunt blocate!');
    redirect("index.php?token=incarca");
}
if (!query("SELECT * FROM clase WHERE id=$id_clasa"))
{
    opensession('error', 'Clasa nu exista!');
    redirect("index.php?token=incarca");
}
if (!$_FILES['fisier']['name'])
{
    opensession('error', 'Nu ati incarcat niciun fisier!');
    redirect("index.php?token=incarca");
}
if ($_FILES['fisier']['error'])
{
    opensession('error', 'Incarcarea a returnat eroarea '.$_FILES['fisier']['error']);
    redirect("index.php?token=incarca");
}
$nume=$_FILES['fisier']['name'];
$info_fisier=@pathinfo($nume);
$marime=$_FILES['fisier']['size']/1024;
if (in_array(strtolower($info_fisier['extension']), $extensii))
{
    opensession('error', 'Extensie neacceptata!');
    redirect("index.php?token=incarca");
}
if (spatiu_depasit($_SESSION['user'], $marime) && rank($_SESSION['user'])=='E')
{
    opensession('error', 'Fisierul este prea mare!');
    redirect("index.php?token=incarca");
}
if (query("SELECT * FROM fisiere WHERE nume='$nume'"))
{
    $v=explode(".", $nume);
    $nume=$v[0].time().".".$info_fisier['extension'];
    $redenumire=TRUE;
}
$id_user=get_id_by_user($user);
move_uploaded_file($_FILES['fisier']['tmp_name'], 'uploads/'.$nume);
$cale=$site_link."uploads/".$nume;
@mysql_query("INSERT INTO fisiere (nume, cale, marime, id_user, id_clasa, data) VALUES ('$nume', '$cale', $marime, $id_user, $id_clasa, NOW())");
opensession('succes', 'Fisierul a fost incarcat cu succes!');
if ($redenumire)
    $_SESSION['succes']=$_SESSION['succes']."<br />Acesta a fost redenumit automat ca ".$nume."!";
if ($clasa=='ALL') 
{
    $rez=@query("SELECT id FROM fisiere WHERE nume='$nume'");
    $id=$rez[0];
    opensession('link', 'Pentru a-l descarca, accesati urmatorul link: <br/>'.$site_link.'index.php?token=descarca&id='.$id);
}
increment_uploads($_SESSION['user'], $marime);
if ($clasa=='ALL') redirect("index.php?token=toate");
if (rank($_SESSION['user'])!='E') $_SESSION['clasa']=get_class_by_id($id_clasa);
redirect("index.php?token=fisiere");
}

else if ($token=="fisiere") //PAGINA CU LISTA FISIERELOR INCARCATE
{
if (!logat())
    redirect("index.php?token=eroare&id=nepermis");
if (rank($_SESSION['user'])=='P' || rank($_SESSION['user'])=='A')
{
$ses=mres($_GET['ses']);
$clasa=mres($_POST['clasa']);
if (!$_SESSION['clasa'] && !$ses) opensession("clasa", "$clasa");
if ($ses=='none') $_SESSION['clasa']='';
else $clasa_s=$_SESSION['clasa'];
?>
<center>
<h1 class="page-header">Fisiere incarcate</h1>
<?
$m=query("SELECT COUNT(*) FROM fisiere");
if ($m[0]==0)
    if (($detectare->isMobile() && !$detectare->isTablet())) echo '<div class="alert alert-danger" role="alert">Nu exista niciun fisier incarcat.</div>';
    else echo '<div class="alert alert-danger" style="width: 70%;" role="alert">Nu exista niciun fisier incarcat.</div>';
else if ((!isset($_POST['clasa']) && !$_SESSION['clasa']) || $ses=='none')
{
read_error();
read_succes();
?>
<form class="form-set" role="form" action="index.php?token=fisiere" method="POST">
<label>Alegeti clasa pentru care sa fie afisate fisierele:</label>
<select class="form-control" style="width: auto;" name="clasa">
<?
$rez=@mysql_query("SELECT * FROM clase ORDER BY clasa ASC");
while ($row=@mysql_fetch_array($rez))
    if ($row['clasa']!='ALL') echo "<option>".$row['clasa']."</option>";
?>
</select>
<button class="btn btn-primary btn-block" style="width: auto;" type="submit">Afisare</button>
</form>
</center>
<?}
else
{
$id_clasa=get_id_by_class($clasa);
$id_clasa_s=get_id_by_class($clasa_s);
if (query("SELECT * FROM fisiere WHERE id_clasa=$clasa") || query("SELECT * FROM fisiere WHERE id_clasa=$id_clasa_s"))
{
?>
<h3>Fisierele <?if ($clasa=='ABS' || $clasa_s=='ABS')
      {
      echo "elevilor absolventi";   
      }
else {?>
clasei <?if ($clasa) echo $clasa; else echo $clasa_s;
}
if ($clasa) $cls=$clasa; else $cls=$clasa_s;
$id_cls=get_id_by_class($cls);
if (query("SELECT * FROM fisiere WHERE id_clasa=$id_cls"))
echo '<a href="index.php?token=sterge_tot&id=fisiere&clasa='.$cls.'" onclick="return confirm(\'Sigur stergeti toate fisierele?\');">
<span class="glyphicon glyphicon-trash"></span>
</a></h3>';?>
<?
read_error();
read_succes();?>
<div class="table-responsive">
<table class="table" style="width: auto;">
<thead>
<th style="text-align: center;">Nume</th>
<?if (!($detectare->isMobile() && !$detectare->isTablet())){?>
<th style="text-align: center;">Marime</th>
<th style="text-align: center;">Numar descarcari</th>
<?}?>
</thead>
<tbody>
<?
if (!$_SESSION['clasa'])
    $query="SELECT * FROM fisiere WHERE id_clasa=$id_clasa ORDER BY id DESC";
else $query="SELECT * FROM fisiere WHERE id_clasa=$id_clasa_s ORDER BY id DESC";
$rez=@mysql_query($query);
while ($row=@mysql_fetch_array($rez))
        {
        if ($row['marime']>1024) $marime=round($row['marime']/1024, 2)." MB";
        else $marime=round($row['marime'], 2)." KB";
        if (($detectare->isMobile() && !$detectare->isTablet()))
            {if (strlen($row['nume'])>18)
                $row['nume']=substr($row['nume'], 0, 17)."...";}
        else if (strlen($row['nume'])>50)
                $row['nume']=substr($row['nume'], 0, 49)."...";
        if (rank(get_user_by_id($row['id_user']))!='E')
            if (rank(get_user_by_id($row['id_user']))=='A')$ubr="<font color='red'>(admin)</font>";
            else if (rank(get_user_by_id($row['id_user']))=='P')$ubr="<font color='magenta'>(prof)</font>";
        else $ubr="";
        if ($detectare->isMobile() && !$detectare->isTablet) $brake="<br />";
        else $brake="";
        echo "<tr>
        <td style='text-align:center;'>".$row['nume']."<br />
        <font color='blue'><b>incarcat de $brake<i>".get_user_by_id($row['id_user'])."</i></font>".$ubr."</b>
        <br />
        (".$row['data'].")
        </td>";
        if (!($detectare->isMobile() && !$detectare->isTablet())) echo "
        <td style='text-align:center;'><br/>".$marime."</td>
        <td style='text-align:center;'><br/>".get_nr_dwn($row['id'])."</td>";
        echo "
        <td>
        <br/>
        <a href=index.php?token=descarca&id=".$row['id'].">
        <span class='glyphicon glyphicon-save'></span>
        </a>
        </td>
        <td>
        <br/>
        <a href='index.php?token=email&id=".$row['id']."'>
        <span class='glyphicon glyphicon-forward'></span>
        </a>
        </td>
        <td>
        <br/>
        <a onclick='return confirm(\"Sigur stergeti acest fisier?\");' href='index.php?token=sterge_fisier&id=".$row['id']."'>
        <span class='glyphicon glyphicon-trash'></span>
        </a>
        </td>
        </tr>";
        $ubr="";
        }
?>
</tbody>
</table>
</div>
<a href="index.php?token=fisiere&ses=none">
<button class="btn btn-primary btn-block" style="width: auto;">Alegeti alta clasa</button>
</a>
<?}
else{
    opensession("error", "Nu exista niciun fisier incarcat pentru aceasta clasa!");
    redirect("index.php?token=fisiere&ses=none");
    }
}
}
else
{
$u=$_SESSION['user'];
$c=@query("SELECT id_clasa FROM utilizatori WHERE user='$u'");
$clasa=get_class_by_id($c[0]);
?>
<center>
<h1 class="page-header">Fisiere incarcate</h1>
<h3>Fisierele <?if ($clasa=='ABS' || $clasa_s=='ABS')
      {
      echo "elevilor absolventi";   
      }
else {?>
clasei <?if ($clasa) echo $clasa; else echo $clasa_s;
}
if ($clasa) $cls=$clasa; else $cls=$clasa_s;
$id_cls=get_id_by_class($cls);
if (query("SELECT * FROM fisiere WHERE id_clasa=$id_cls"))
echo '<a href="index.php?token=sterge_tot&id=fisiere&clasa='.$cls.'" onclick="return confirm(\'Sigur stergeti toate fisierele?\');"><span class="glyphicon glyphicon-trash"></span></a>';
?></h3>
<?
read_error();
read_succes();
$m=query("SELECT COUNT(*) FROM fisiere");
if ($m[0]==0)
    if (($detectare->isMobile() && !$detectare->isTablet())) echo '<div class="alert alert-danger" role="alert">Nu exista niciun fisier incarcat.</div>';
    else echo '<div class="alert alert-danger" style="width: 70%;" role="alert">Nu exista niciun fisier incarcat.</div>';
else 
{
$id_clasa=get_id_by_class($clasa);
$id_clasa_s=get_id_by_class($clasa_s);
if (query("SELECT * FROM fisiere WHERE id_clasa=$id_clasa") || query("SELECT * FROM fisiere WHERE id_clasa=$clasa_s"))
{
?>
<div class="table-responsive">
<table class="table" style="width: auto;">
<thead>
<th style="text-align: center;">Nume</th>
<?if (!($detectare->isMobile() && !$detectare->isTablet())){?>
<th style="text-align: center;">Marime</th>
<th style="text-align: center;">Numar descarcari</th>
<?}?>
</thead>
<tbody>
<?
if (!$_SESSION['clasa'])
    $query="SELECT * FROM fisiere WHERE id_clasa=$id_clasa ORDER BY id DESC";
else {$clasa_s=$_SESSION['clasa']; $query="SELECT * FROM fisiere WHERE id_clasa=$id_clasa_s ORDER BY id DESC";}
$rez=@mysql_query($query);
while ($row=@mysql_fetch_array($rez))
        {
        if ($row['marime']>1024) $marime=round($row['marime']/1024, 2)." MB";
        else $marime=round($row['marime'], 2)." KB";
        if (($detectare->isMobile() && !$detectare->isTablet()))
            {if (strlen($row['nume'])>18)
                $row['nume']=substr($row['nume'], 0, 17)."...";}
        else if (strlen($row['nume'])>50)
                $row['nume']=substr($row['nume'], 0, 49)."...";
        if (rank(get_user_by_id($row['id_user']))!='E')
            if (rank(get_user_by_id($row['id_user']))=='A')$ubr="<font color='red'>(admin)</font>";
            else if (rank(get_user_by_id($row['id_user']))=='P')$ubr="<font color='magenta'>(prof)</font>";
        else $ubr="";
        if ($detectare->isMobile() && !$detectare->isTablet) $brake="<br />";
        else $brake="";
        echo "<tr>
        <td style='text-align:center;'>".$row['nume']."<br />
        <font color='blue'>
        <b>incarcat de $brake<i>".get_user_by_id($row['id_user'])."</i></font>".$ubr."</b>
        <br />
        (".$row['data'].")
        </td>";
        if (!($detectare->isMobile() && !$detectare->isTablet()))
        echo "
        <td style='text-align:center;'><br/>".$marime."</td>
        <td style='text-align:center;'><br/>".get_nr_dwn($row['id'])."</td>";
        echo "
        <td>
        <a href=index.php?token=descarca&id=".$row['id'].">
        <br/><span class='glyphicon glyphicon-save'></span>
        </a>
        </td>
        <td>
        <a href='index.php?token=email&id=".$row['id']."'>
        <br/><span class='glyphicon glyphicon-share-alt'></span>
        </a>
        </td>
        <td>
        <a onclick='return confirm(\"Sigur stergeti acest fisier?\");' href='index.php?token=sterge_fisier&id=".$row['id']."'>
        <br/><span class='glyphicon glyphicon-trash'></span>
        </a>
        </td>
        </tr>";
        $ubr="";
        }
?>
</tbody>
</table>
</div>
</center>
<?
}
else if (($detectare->isMobile() && !$detectare->isTablet())) echo '<div class="alert alert-danger" role="alert">Nu exista niciun fisier incarcat pentru aceasta clasa.</div>';
else echo '<div class="alert alert-danger" style="width: 70%;" role="alert">Nu exista niciun fisier incarcat pentru aceasta clasa.</div>';
}
}
}

else if ($token=="descarca") //PAGINA DE PROCESARE A CERERII DE DESCARCARE A UNUI FISIER
{
ob_clean();
$id=mres($_GET['id']);
if (!is_numeric($id) || $_SERVER['REQUEST_METHOD']!='GET' || !isset($_GET['id']))
    redirect("index.php?token=eroare&id=nepermis");
if (logat() && $_SESSION['acc']==1)
    {
    $rez=@mysql_query("SELECT * FROM fisiere WHERE id=$id");
    $res=@mysql_fetch_array($rez);
    if ($res)
    {
    $fisier=$res['nume'];
    set_time_limit(0);
    $nume_fisier=$fisier;
    update_downloaded($_SESSION['user'], $id);
    $fisier_tinta="uploads/{$nume_fisier}";
    header('Content-Description: File Transfer');
    header("Content-Disposition: attachment; filename=\"$nume_fisier\"");
    header('Content-Type: application/octet-stream');
    header('Content-Transfer-Encoding: binary');
    header('Content-Length: '.filesize($fisier_tinta));
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Pragma: public');
    header('Expires: 0');
    readfile($fisier_tinta);
    $_SESSION['acc']='';
    }
    else 
    {
    opensession("error", "Fisierul nu exista!");
    redirect("index.php?token=eroare");
    }
    }
else if (logat() && (rank($_SESSION['user'])=='P' || rank($_SESSION['user'])=='A'))
    {
    $rez=@mysql_query("SELECT * FROM fisiere WHERE id=$id");
    $res=@mysql_fetch_array($rez);
    if ($res)
    {
    $user=$_SESSION['user'];
    $fisier=$res['nume'];
    set_time_limit(0);
    $nume_fisier=$fisier;
    update_downloaded($_SESSION['user'], $id);
    $fisier_tinta="uploads/{$nume_fisier}";
    header('Content-Description: File Transfer');
    header("Content-Disposition: attachment; filename=\"$nume_fisier\"");
    header('Content-Type: application/octet-stream');
    header('Content-Transfer-Encoding: binary');
    header('Content-Length: '.filesize($fisier_tinta));
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Pragma: public');
    header('Expires: 0');
    readfile($fisier_tinta);
    }
    else 
    {
    opensession("error", "Fisierul nu exista!");
    redirect("index.php?token=eroare");
    }
    }
else if (logat() && rank($_SESSION['user'])=='E')
    {
    $rez=@mysql_query("SELECT * FROM fisiere WHERE id=$id");
    $res=@mysql_fetch_array($rez);
    if ($res)
    {
    $clasa_f=$res['id_clasa'];
    $user=$_SESSION['user'];
    $rez2=@mysql_query("SELECT * FROM utilizatori WHERE user='$user'");
    $res2=@mysql_fetch_array($rez2);
    $clasa_e=$res2['id_clasa'];
    if ($clasa_e==$clasa_f || $clasa_f==1 || rank($_SESSION['user'])=='P')
    {
    $fisier=$res['nume'];
    set_time_limit(0);
    $nume_fisier=$fisier;
    update_downloaded($_SESSION['user'], $id);
    $fisier_tinta="uploads/{$nume_fisier}";
    header('Content-Description: File Transfer');
    header("Content-Disposition: attachment; filename=\"$nume_fisier\"");
    header('Content-Type: application/octet-stream');
    header('Content-Transfer-Encoding: binary');
    header('Content-Length: '.filesize($fisier_tinta));
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Pragma: public');
    header('Expires: 0');
    readfile($fisier_tinta);
    }
    else 
    {
    opensession("error", "Nu aveti permisiunea de a descarca acest fisier!");
    redirect("index.php?token=fisiere");
    }
    }
    else{
        opensession("error", "Fisierul nu exista!");
        redirect("index.php?token=eroare");
    }
}
else if (!logat() && !$_SESSION['acc'])
    {
    $rez=@mysql_query("SELECT * FROM fisiere WHERE id=$id");
    $res=@mysql_fetch_array($rez);
    if ($res)
    {
    $clasa_f=$res['id_clasa'];
    if ($clasa_f==1)
    {
    $fisier=$res['nume'];
    set_time_limit(0);
    $nume_fisier=$fisier;
    $fisier_tinta="uploads/{$nume_fisier}";
    header('Content-Description: File Transfer');
    header("Content-Disposition: attachment; filename=\"$nume_fisier\"");
    header('Content-Type: application/octet-stream');
    header('Content-Transfer-Encoding: binary');
    header('Content-Length: '.filesize($fisier_tinta));
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Pragma: public');
    header('Expires: 0');
    readfile($fisier_tinta);
    }
    else 
    {
    opensession("error", "Nu aveti permisiunea de a descarca acest fisier!");
    redirect("index.php?token=eroare");
    }
    }
    else{
        opensession("error", "Fisierul nu exista!");
        redirect("index.php?token=eroare");
    }
    }
else if (!logat() && $_SESSION['acc'])
    {
    $rez=@mysql_query("SELECT * FROM fisiere WHERE id=$id");
    $res=@mysql_fetch_array($rez);
    if ($res)
    {
    $fisier=$res['nume'];
    set_time_limit(0);
    $nume_fisier=$fisier;
    $fisier_tinta="uploads/{$nume_fisier}";
    header('Content-Description: File Transfer');
    header("Content-Disposition: attachment; filename=\"$nume_fisier\"");
    header('Content-Type: application/octet-stream');
    header('Content-Transfer-Encoding: binary');
    header('Content-Length: '.filesize($fisier_tinta));
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Pragma: public');
    header('Expires: 0');
    readfile($fisier_tinta);
    $_SESSION['acc']='';
    }
    else 
    {
    opensession("error", "Fisierul nu exista!");
    redirect("index.php?token=eroare");
    }
    }
}

else if ($token=="sterge_fisier") //PAGINA DE PROCESARE - STERGEREA UNUI FISIER
{
if (!logat() || $_SERVER['REQUEST_METHOD']!='GET' || !isset($_GET['id']) || !is_numeric($_GET['id']))
    redirect("index.php?token=eroare&id=nepermis");
$id=mres($_GET['id']);
$res=query("SELECT * FROM fisiere WHERE id=$id");
if (!$res)
    {
    opensession('error', 'Fisierul nu exista!');
    redirect("index.php?token=eroare");
    }
if (rank($_SESSION['user'])=='E' && $res['id_user']!=get_id_by_user($_SESSION['user']))
    {
    opensession('error', 'Nu aveti dreptul sa stergeti acest fisier!<br/>');
    redirect("index.php?token=eroare");
    }
$cale="uploads/".$res['nume'];
unlink($cale);
if (file_class($id)=='ALL')
    $r="index.php?token=toate";
else $r="index.php?token=fisiere";
decrement_uploads(get_user_by_id($res['id_user']), $res['marime']);
@mysql_query("DELETE FROM fisiere WHERE id=$id");
opensession('succes', 'Fisierul a fost sters.');
redirect($r);
}

else if ($token=="toate") //PAGINA CU LISTA FISIERELOR FARA CATEGORIE
{
if (!logat() || rank($_SESSION['user'])!='P' && rank($_SESSION['user'])!='A')
    redirect("index.php?token=eroare&id=nepermis");
?>
<center>
<h1 class="page-header">Fisiere fara categorie
<a href="index.php?token=sterge_tot&id=fisiere_toate" onclick="return confirm('Sigur stergeti toate fisierele?');"><span class="glyphicon glyphicon-trash"></span></a>
</h1>
<?
read_error();
read_succes();
$m=query("SELECT COUNT(*) FROM fisiere");
if ($m[0]==0)
    if (($detectare->isMobile() && !$detectare->isTablet())) echo '<div class="alert alert-danger" role="alert">Nu exista niciun fisier incarcat.</div>';
    else echo '<div class="alert alert-danger" style="width: 70%;" role="alert">Nu exista niciun fisier incarcat.</div>';
else if (!query("SELECT * FROM fisiere WHERE id_clasa=1"))
    if (($detectare->isMobile() && !$detectare->isTablet())) echo '<div class="alert alert-danger" role="alert">Nu exista niciun fisier incarcat pentru tipul "fisiere fara categorie".</div>';
    else echo '<div class="alert alert-danger" style="width: 70%;" role="alert">Nu exista niciun fisier incarcat pentru tipul "fisiere fara categorie".</div>';
else{
?>
<div class="alert alert-info" <?if (!($detectare->isMobile() && !$detectare->isTablet())){?> style="width: 70%;"<?}?>>Aceasta lista contine fisiere ce au fost incarcate ca atasament la e-mail-uri sau mesaje private, respectiv ce au fost incarcate pentru tipul "ALL" (fara categorie).</div>
<div class="table-responsive">
<table class="table" style="width: auto;">
<thead>
<th style="text-align: center;">Nume</th>
<?if (!($detectare->isMobile() && !$detectare->isTablet())){?>
<th style="text-align: center;">Marime</th>
<th style="text-align: center;">Numar descarcari</th>
<?}?>
</thead>
<tbody>
<?
$query="SELECT * FROM fisiere WHERE id_clasa=1 ORDER BY id DESC";
$rez=@mysql_query($query);
while ($row=@mysql_fetch_array($rez))
    {
    if ($row['marime']>1024) $marime=round($row['marime']/1024, 2)." MB";
    else $marime=round($row['marime'], 2)." KB";
    if (($detectare->isMobile() && !$detectare->isTablet()))
            {if (strlen($row['nume'])>18)
                $row['nume']=substr($row['nume'], 0, 17)."...";}
        else if (strlen($row['nume'])>50)
                $row['nume']=substr($row['nume'], 0, 49)."...";
    echo "<tr>
    <td style='text-align:center;'>".$row['nume']."<br />
    (".$row['data'].")";
    if (!($detectare->isMobile() && !$detectare->isTablet()))
    echo "
    <td style='text-align:center;'>".$marime."</td>
    <td style='text-align:center;'>".get_nr_dwn($row['id'])."</td>";
    echo "
    <td>
    <a href=index.php?token=descarca&id=".$row['id'].">
    <span class='glyphicon glyphicon-save'></span>
    </a>
    </td>
    <td>
    <a href='index.php?token=email&id=".$row['id']."'>
    <span class='glyphicon glyphicon-share-alt'></span>
    </a>
    </td>
    <td>
    <a href='index.php?token=sterge_fisier&id=".$row['id']."'>
    <span class='glyphicon glyphicon-trash'></span>
    </a>
    </td>
    </tr>";
    }
?>
</tbody>
</table>
</div>
</center>
<?
}
}

else if ($token=="materii") //PAGINA CU LISTA FISIERELOR INCARCATE PENTRU FIECARE MATERIE
{
if (!logat())
    redirect("index.php?token=eroare&id=nepermis");
?>
<center>
<h1 class="page-header">Fisiere pe materii</h1>
<?
read_succes();
read_error();
if (!query("SELECT * FROM fisiere_categorii"))
    if (($detectare->isMobile() && !$detectare->isTablet())) echo '<div class="alert alert-danger" role="alert">Nu exista niciun fisier incarcat de acest tip.</div>';
    else echo '<div class="alert alert-danger" style="width: 70%;" role="alert">Nu exista niciun fisier incarcat de acest tip.</div>';
else{
$q=@mysql_query("SELECT * FROM materii");
while($r=@mysql_fetch_array($q))
    {
    echo "<div class='panel panel-primary'>
    <div class='panel-heading'>".$r['nume']."</div>
    <div class='panel-body'>";
    if (!query("SELECT * FROM fisiere_categorii WHERE id_materie='".get_id_by_materie($r['nume'])."'"))
        if (($detectare->isMobile() && !$detectare->isTablet())) echo '<div class="alert alert-danger" role="alert">Nu exista niciun fisier incarcat pentru aceasta materie.</div>';
        else echo '<div class="alert alert-danger" style="width: 70%;" role="alert">Nu exista niciun fisier incarcat pentru aceasta materie.</div>';
    else{
        ?>
        <div class="table-responsive">
        <table class='table' style='width: auto;'">
        <thead>
        <th style="text-align: center;">Nume</th>
        <?if (!($detectare->isMobile() && !$detectare->isTablet())){?>
        <th style="text-align: center;">Marime</th>
        <th style="text-align: center;">Numar descarcari</th>
        <?}?>
        </thead>
        <tbody>
        <?
        $q2=@mysql_query("SELECT * FROM fisiere_categorii WHERE id_materie='".get_id_by_materie($r['nume'])."' ORDER BY nume ASC");
            while ($row=mysql_fetch_array($q2))
                {
                if ($row['marime']>1024) $marime=round($row['marime']/1024, 2)." MB";
                else $marime=round($row['marime'], 2)." KB";
                if (($detectare->isMobile() && !$detectare->isTablet()))
                    {if (strlen($row['nume'])>18)
                        $row['nume']=substr($row['nume'], 0, 17)."...";}
                else if (strlen($row['nume'])>50)
                        $row['nume']=substr($row['nume'], 0, 49)."...";
                if (rank(get_user_by_id($row['id_user']))!='E')
                    if (rank(get_user_by_id($row['id_user']))=='A')$ubr="<font color='red'>(admin)</font>";
                    else if (rank(get_user_by_id($row['id_user']))=='P')$ubr="<font color='magenta'>(prof)</font>";
                else $ubr="";
                if ($detectare->isMobile() && !$detectare->isTablet) $brake="<br />";
                else $brake="";
                echo "<tr>
                <td style='text-align:center;'>".$row['nume']."<br />
                <font color='blue'><b>incarcat de $brake<i>".get_user_by_id($row['id_user'])."</i></font>".$ubr."</b>
                <br />
                (".$row['data'].")";
                if (!($detectare->isMobile() && !$detectare->isTablet()))
                echo "
                <td style='text-align:center;'><br/>".$marime."</td>
                <td style='text-align:center;'><br/>".get_nr_dwn_materii($row['id'])."</td>";
                echo "
                <td>
                <a href=index.php?token=descarca_materii&id=".$row['id'].">
                <br/><span class='glyphicon glyphicon-save'></span>
                </a>
                </td>
                <td>
                <a href='index.php?token=sterge_fisier_materii&id=".$row['id']."' onclick=\"return confirm('Sigur stergeti acest fisier?');\"'>
                <br/><span class='glyphicon glyphicon-trash'></span>
                </td>
                </tr>";
                $ubr="";
                }
        ?>
        </tbody>
        </table>
        </div>
        <?
        }
    echo"</div></div>";
    }
    }
?>
</center>
<?
}

else if ($token=="incarca_materii") //PAGINA DE INCARCARE A UNUI FISIER PENTRU O ANUMITA MATERIE
{
if (!logat())
    redirect("index.php?token=eroare&id=nepermis");
?>
<center>
<h1 class="page-header">Incarcare fisier</h1>
<?if (rank($_SESSION['user'])=='E') {?>
<div class="alert alert-warning">ATENTIE! Fisierul trebuie sa ocupe max. <?=round(get_dim_max()-get_spatiu_user($_SESSION['user']), 2);?> MB!</div>
<?}?>
<form class="form-set" role="form" action="index.php?token=procesare_incarca_materii" method="POST" enctype="multipart/form-data">
<label>Alegeti materia: </label>
<select class="form-control" style="width: auto;" name="materie">
<?
$q=@mysql_query("SELECT * FROM materii ORDER BY nume ASC");
while ($r=@mysql_fetch_array($q))
    echo "<option>".$r['nume']."</option>";
?>
</select>
<input class="form-control" type="file" name="fisier" <?if (blocked_uploads()) echo 'disabled="disabled"';?> required />
<?
read_error();
?>
<button class="btn btn-primary btn-block" style="width: auto;" type="submit">Incarcare</button>
</form>
</center>
<?
}

else if ($token=="procesare_incarca_materii") //PAGINA DE PROCESARE - INCARCAREA UNUI FISIER PENTRU O ANUMITA MATERIE
{
if (!logat() || $_SERVER['REQUEST_METHOD']!='POST')
    redirect("index.php?token=eroare&id=nepermis");
$materie=mres($_POST['materie']);
$id_materie=get_id_by_materie($materie);
$extensii=get_extensii();
$user=$_SESSION['user'];
if (blocked_uploads())
{
    opensession('error', 'Incarcarile de fisiere sunt blocate!');
    redirect("index.php?token=incarca_materii");
}
if (!query("SELECT * FROM materii WHERE nume='$materie'"))
{
    opensession('error', 'Materia nu exista!');
    redirect("index.php?token=incarca_materii");
}
if (!$_FILES['fisier']['name'])
{
    opensession('error', 'Nu ati incarcat niciun fisier!');
    redirect("index.php?token=incarca_materii");
}
if ($_FILES['fisier']['error'])
{
    opensession('error', 'Incarcarea a returnat eroarea '.$_FILES['fisier']['error']);
    redirect("index.php?token=incarca_materii");
}
$nume=$_FILES['fisier']['name'];
$info_fisier=@pathinfo($nume);
$marime=$_FILES['fisier']['size']/1024;
if (in_array(strtolower($info_fisier['extension']), $extensii))
{
    opensession('error', 'Extensie neacceptata!');
    redirect("index.php?token=incarca_materii");
}
if (spatiu_depasit($_SESSION['user'], $marime) && rank($_SESSION['user'])=='E')
{
    opensession('error', 'Fisierul este prea mare!');
    redirect("index.php?token=incarca_materii");
}
if (query("SELECT * FROM fisiere_categorii WHERE nume='$nume'"))
{
    $v=explode(".", $nume);
    $nume=$v[0].time().".".$info_fisier['extension'];
    $redenumire=TRUE;
}
$id_user=get_id_by_user($user);
$cale=$site_link."fisiere_categorii/".$nume;
move_uploaded_file($_FILES['fisier']['tmp_name'], 'fisiere_categorii/'.$nume);
@mysql_query("INSERT INTO fisiere_categorii (nume, cale, marime, id_materie, id_user, data) VALUES ('$nume', '$cale', $marime, $id_materie, $id_user, NOW())");
opensession('succes', 'Fisierul a fost incarcat cu succes!');
if ($redenumire)
    $_SESSION['succes']=$_SESSION['succes']."<br /> Acesta a fost redenumit automat ca ".$nume."!";
increment_uploads($_SESSION['user'], $marime);
redirect("index.php?token=materii");
}

else if ($token=="sterge_fisier_materii") //PAGINA DE PROCESARE - STERGEREA UNUI FISIER DE LA O ANUMITA MATERIE
{
if (!logat() || $_SERVER['REQUEST_METHOD']!='GET' || !isset($_GET['id']) || !is_numeric($_GET['id']))
    redirect("index.php?token=eroare&id=nepermis");
$id=mres($_GET['id']);
$res=query("SELECT * FROM fisiere_categorii WHERE id=$id");
if (!$res)
    {
    opensession('error', 'Fisierul nu exista!');
    redirect("index.php?token=eroare");
    }
if (rank($_SESSION['user'])=='E' && get_user_by_id($res['id_user'])!=$_SESSION['user'])
    {
    opensession('error', 'Nu aveti dreptul sa stergeti acest fisier!<br/>');
    redirect("index.php?token=materii");
    }
$cale="fisiere_categorii/".$res['nume'];
unlink($cale);
decrement_uploads(get_user_by_id($res['id_user']), $res['marime']);
@mysql_query("DELETE FROM fisiere_categorii WHERE id=$id");
opensession('succes', 'Fisierul a fost sters.');
redirect("index.php?token=materii"); 
}

else if ($token=="descarca_materii") //PAGINA DE PROCESARE - DESCARCAREA UNUI FISIER DE LA O ANUMITA MATERIE
{
ob_clean();
$id=mres($_GET['id']);
if (!logat() || !is_numeric($id) || $_SERVER['REQUEST_METHOD']!='GET' || !isset($_GET['id']))
    redirect("index.php?token=eroare&id=nepermis");
$rez=@mysql_query("SELECT * FROM fisiere_categorii WHERE id=$id");
$res=@mysql_fetch_array($rez);
if ($res)
{
$fisier=$res['nume'];
set_time_limit(0);
$nume_fisier=$fisier;
update_downloaded_m($_SESSION['user'], $id);
$fisier_tinta="fisiere_categorii/{$nume_fisier}";
header('Content-Description: File Transfer');
header("Content-Disposition: attachment; filename=\"$nume_fisier\"");
header('Content-Type: application/octet-stream');
header('Content-Transfer-Encoding: binary');
header('Content-Length: '.filesize($fisier_tinta));
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Pragma: public');
header('Expires: 0');
readfile($fisier_tinta);
}
else 
{
opensession("error", "Fisierul nu exista!");
redirect("index.php?token=eroare");
}
}

else if ($token=="succes") //AFISAREA UNUI MESAJ DE CONFIRMARE
{
if ($_SESSION['succes'])
    {
    echo '<center>
    <h1 class="page-header">Yupi :)</h1>
    <div class="alert alert-success" role="alert"';
    if (!($detectare->isMobile() && !$detectare->isTablet())) echo 'style="width: 70%;"';
    echo '>
    <h4>Succes!</h4>
    '.$_SESSION['succes'].'</div>
    </center>';
    $_SESSION['succes']='';
    }
else redirect("");
}

else if ($token=="eroare") //AFISAREA UNUI MESAJ DE EROARE
{
$id=mres($_GET['id']);
if ($id=="nepermis")
    $_SESSION['error']='Nepermis!';
if ($_SESSION['error'])
    {
    echo '
    <center>
    <h1 class="page-header">Ups... ceva nu a mers bine :(</h1>
    <div class="alert alert-danger" role="alert" ';
    if (!($detectare->isMobile() && !$detectare->isTablet())) echo 'style="width: 70%;"';
    echo '>
    <h4 style="text-align: center;">Eroare!</h4>'.$_SESSION['error'].'</div>
    </center>';
    $_SESSION['error']='';
    }
else redirect ("");
}

else {
    opensession("error", "Pagina accesata nu exista!");
    redirect("index.php?token=eroare");
}

?>

<!-- SFARSITUL CONTINUTULUI -->

</div>

<!-- SUBSOLUL -->

<footer>
    <style type="text/css">
    .navbar {min-height: 20px; height: auto; background-color: #eee;}
    </style>
    <?if (!($detectare->isMobile() && !$detectare->isTablet()))
    {?>
    <nav class="navbar navbar-default navbar-fixed-bottom" role="navigation">
        <div class="container">
            <center>Copyright 2014 &copy; Cosmin Crihan - Toate drepturile rezervate.</center>
        </div>
    </nav>
    <?
    }
    ?>
</footer>

<!-- SFARSIT SUBSOL -->

</body>
</html>
